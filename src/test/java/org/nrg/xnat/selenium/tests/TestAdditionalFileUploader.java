package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.DataUnit;
import org.nrg.selenium.enums.ResourceSettingsCategory;
import org.nrg.selenium.util.ResourceValidation;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.selenium.xnat.page_model.session.report.SessionReportPageScanTable;
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage;
import org.nrg.selenium.xnat.resource_settings.ProjectResourceSettingsConfig;
import org.nrg.selenium.xnat.resource_settings.ProjectResourceSettingsUpload;
import org.nrg.testing.FileIOUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.scans.MRScan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.experiments.sessions.PETSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class TestAdditionalFileUploader extends BaseSeleniumTest {

    private final File textFileA = getDataFile("seleniumDataA.txt");
    private final int textFileABytes = 36;
    private static final Project AFU_PROJECT = new Project("AFU_" + RandomHelper.randomID(8));
    private static final TestData MR_DICOM = TestData.SIF_SESSION;
    private static final TestData PET_DICOM = TestData.SIMPLE_PET;
    private static final String PROP_ENABLE = "allowHtmlResourceRendering";

    @BeforeClass
    public void setupProject() {
        mainAdminInterface().postSiteConfigProperty(PROP_ENABLE, true);
        mainInterface().createProject(AFU_PROJECT.addSubject(new Subject().addExperiment(new MRSession())));
         // QA-450: Add an MR to make sure dropdown populates
        mainQueryBase().delete(formatXapiUrl("/access/cache/flush")).then().assertThat().statusCode(200);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        mainAdminInterface().postSiteConfigProperty(PROP_ENABLE, false);
    }

    @Test
    @JiraKey(simpleKey = "QA-326", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-322")
    })
    public void testProjectFileUpload() {
        final String title = "Project Level Files";
        final String description = "test description";
        final String resourceFolder = "prjFolder";

        final ProjectReportPage projectPage = initializeCustomUploadTest(
                new ProjectResourceSettingsConfig().category(ResourceSettingsCategory.PROJECT).title(title).description(description).resourceFolder(resourceFolder).overwrite(true)
        );
        navigate().refresh();
        projectPage.performInitialValidation();
        projectPage.loadCustomResourceUploader().
                uploadToProjectResourceSettings(new ProjectResourceSettingsUpload().title(title).description(description).file(textFileA)).
                loadManageFiles().
                capture().
                getResource(resourceFolder).
                assertFolderSizes(new ResourceValidation(textFileABytes, DataUnit.BYTES, 1)).
                getFile(textFileA.getName()).
                assertFileSize(textFileABytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-327", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-323")
    })
    public void testSubjectFileUpload() {
        final String title = "Subject Level Files";
        final String description = "test description";
        final String resourceFolder = "subjectFolder";
        final String subfolder = "subfolder";
        final String fileName = "seleniumDataD.txt";
        final File originalFile = Paths.get(Settings.DATA_LOCATION, "repeatData", "repeatData1", fileName).toFile();
        final int originalFileBytes = 26;
        final File overwriteFile = Paths.get(Settings.DATA_LOCATION, "repeatData", "repeatData2", fileName).toFile();
        final int overwriteFileBytes = 10;
        final ProjectResourceSettingsUpload uploadDefinition = new ProjectResourceSettingsUpload().title(title).description(description).file(originalFile);

        final SubjectReportPage subjectReportPage = initializeCustomUploadTest(
                new ProjectResourceSettingsConfig().category(ResourceSettingsCategory.SUBJECT).title(title).description(description).resourceFolder(resourceFolder).subfolder(subfolder).overwrite(true)
        ).createSubject(testSpecificSubject.project(AFU_PROJECT));

        subjectReportPage.loadCustomResourceUploader().
                uploadToProjectResourceSettings(uploadDefinition).
                loadManageFiles().
                capture().
                getResource(resourceFolder).
                getSubfolder(subfolder).
                getFile(fileName).
                assertFileSize(originalFileBytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(originalFile)).
                capture().
                navigate().
                back();

        subjectReportPage.loadCustomResourceUploader().
                uploadToProjectResourceSettings(uploadDefinition.file(overwriteFile).overwriteAllowed(true)).
                loadManageFiles().
                capture().
                getResource(resourceFolder).
                assertFolderSizes(new ResourceValidation(overwriteFileBytes, DataUnit.BYTES, 1)).
                getSubfolder(subfolder).
                getFile(fileName).
                assertFileSize(overwriteFileBytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(overwriteFile)).
                capture().
                navigate().
                back();

        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-328", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-324")
    })
    public void testMRFileUpload() {
        final String title = "MR Level Files";
        final String description = "test description";
        final String resourceFolder = "mrFolder";
        final File textFileR = getDataFile("seleniumDataR.txt");
        final int textFileRBytes = 16;

        initializeCustomUploadTest(
                new ProjectResourceSettingsConfig().category(ResourceSettingsCategory.IMAGE_SESSION).title(title).description(description).resourceFolder(resourceFolder).dataType(DataType.MR_SESSION.getSingularName())
        ).createSubject(testSpecificSubject.project(AFU_PROJECT)).
                loadSessionCreationPage().
                create(new MRSession(AFU_PROJECT, testSpecificSubject, testSpecificSubject.getLabel() + "_MR")).
                loadCustomResourceUploader().
                uploadToProjectResourceSettings(new ProjectResourceSettingsUpload().title(title).description(description).file(textFileR)).
                loadManageFiles().
                capture().
                getResource(resourceFolder).
                assertFolderSizes(new ResourceValidation(textFileRBytes, DataUnit.BYTES, 1)).
                getFile(textFileR.getName()).
                assertFileSize(textFileRBytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileR)).
                capture();

        home().
                loadSessionCreationPage().
                create(new PETSession(AFU_PROJECT, testSpecificSubject, testSpecificSubject.getLabel() + "_PET")).
                assertCustomResourceUploadStatus(false);
        logLogout();
    }

    @Test
    @TestRequires(data = TestData.SIMPLE_PET)
    @JiraKey(simpleKey = "QA-332", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-331")
    })
    public void testImageFileUpload() {
        final String title = "Image Level Files";
        final String description = "description test";
        final String resourceFolder = "imageFolder";
        final File textFileB = getDataFile("seleniumDataB.txt");
        final int textFileBBytes = 89;

        initializeCustomUploadTest(
                new ProjectResourceSettingsConfig().category(ResourceSettingsCategory.IMAGE_SESSION).title(title).description(description).resourceFolder(resourceFolder)
        ).createSubject(testSpecificSubject.project(AFU_PROJECT)).
                loadSessionCreationPage().
                create(new MRSession(AFU_PROJECT, testSpecificSubject, testSpecificSubject.getLabel() + "_MR")).
                assertCustomResourceUploadStatus(true).
                loadCompressedUploader().
                uploadToArchive(AFU_PROJECT, PET_DICOM).
                loadCustomResourceUploader().
                uploadToProjectResourceSettings(new ProjectResourceSettingsUpload().title(title).description(description).file(textFileB)).
                loadManageFiles().
                capture().
                getResource(resourceFolder).
                assertFolderSizes(new ResourceValidation(textFileBBytes, DataUnit.BYTES, 1)).
                getFile(textFileB.getName()).
                assertFileSize(textFileBBytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileB)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @TestRequires(data = TestData.SIF_SESSION)
    @JiraKey(simpleKey = "QA-329", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-325")
    })
    public void testScanFileUpload() {
        final String title = "Scan Level Files";
        final String description = "test description";
        final String resourceFolder = "scanFolder";
        final List<String> configuredScanTypes = Arrays.asList("3 Plane Localizer", "PET Start");
        final Scan scanForUpload = new MRScan().id("11").type("PET Start");

        final SessionReportPageScanTable scanTable = initializeCustomUploadTest(
                new ProjectResourceSettingsConfig().category(ResourceSettingsCategory.SCAN).title(title).description(description).resourceFolder(resourceFolder).scanTypes(configuredScanTypes)
        ).loadCompressedUploader().
                uploadToArchive(AFU_PROJECT, MR_DICOM).
                getScanTable();
        for (Scan scan : scanTable.readScans()) {
            assertEquals(
                    configuredScanTypes.contains(scan.getType()),
                    scanTable.isCustomResourceUploadOptionPresent(scan)
            );
        }
        scanTable.capture().
                loadCustomResourceUploaderForScan(scanForUpload).
                uploadToProjectResourceSettings(new ProjectResourceSettingsUpload().title(title).description(description).file(textFileA)).
                loadManageFiles().
                capture().
                getScanResource(resourceFolder, scanForUpload).
                assertFolderSizes(new ResourceValidation(textFileABytes, DataUnit.BYTES, 1)).
                getFile(textFileA.getName()).
                assertFileSize(textFileABytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-333")
    public void testAFUDeletion() {
        final String title = "Project Level Files";
        final String description = "test description";
        final String resourceFolder = "prjFolder";
        final ProjectResourceSettingsConfig config = new ProjectResourceSettingsConfig().category(ResourceSettingsCategory.PROJECT).title(title).description(description).resourceFolder(resourceFolder);

        final ProjectReportPage projectReportPage = loginPage.seleniumLogin().
                createProject(testSpecificProject).
                loadManageTab().
                setupProjectResourceSettings(config).
                getProjectPage();
        projectReportPage.navigate().refresh();
        projectReportPage.assertCustomResourceUploadStatus(true);
        projectReportPage.loadManageTab().
                deleteProjectResourceConfiguration(config).
                navigate().
                refresh();
        projectReportPage.assertCustomResourceUploadStatus(false);
        logLogout();
    }

    private ProjectReportPage initializeCustomUploadTest(ProjectResourceSettingsConfig config) {
        return loginPage.seleniumLogin().
                searchForProject(AFU_PROJECT).
                loadManageTab().
                setupProjectResourceSettings(config).
                getProjectPage();
    }

}
