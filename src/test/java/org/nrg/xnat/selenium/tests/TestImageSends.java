package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.*;
import org.nrg.selenium.enums.DataUnit;
import org.nrg.selenium.enums.PrearchiveStatus;
import org.nrg.selenium.util.ResourceValidation;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.testing.annotations.Basic;
import org.nrg.testing.annotations.ExpectedFailure;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.Test;
import java.util.*;

import static org.dcm4che3.data.Tag.*;

public class TestImageSends extends BaseSeleniumTest {

    @Test
    @TestRequires(data = TestData.JPEGLOSSLESS_2000)
    @JiraKey(simpleKey = "QA-282", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-119")
    })
    @ExpectedFailure(jiraIssue = "XNAT-6581")
    public void testUploadCompressedJpegLossless2000() {
        // JIRA ticket: XT-175, XT-90

        loginPage.seleniumLogin().
                createProject(testSpecificProject).
                loadCompressedUploader().
                uploadToPrearchive(testSpecificProject, TestData.JPEGLOSSLESS_2000).
                searchForProject(testSpecificProject).
                loadProjectPrearchive().
                findOnlySessionInProject(testSpecificProject).
                capture().
                checkAll().
                capture().
                reviewAndArchive().
                assertScanSize("3", new ResourceValidation(288, DataUnit.MEGABYTES, 3)).
                captureScans().
                archiveSession().
                assertPageRepresents(new MRSession("ANONYMIZED_SESSION")).
                assertAutoRunCompletion().
                assertSnapshotGeneration().
                logLogout();
    }

    @Test
    @TestRequires(data = TestData.SAMPLE_1, dicomScp = true)
    @Basic
    @JiraKey(simpleKey = "QA-120")
    public void testUploadAutoarchive() {
        // JIRA ticket: XT-101
        final MRSession seleniumSession = new MRSession(testSpecificProject, new Subject().label("seleniumSubject"), "seleniumSession");
        final Map<Integer, String> routingHeaders = new HashMap<>();
        routingHeaders.put(StudyDescription, seleniumSession.getPrimaryProject().getId());
        routingHeaders.put(PatientName, seleniumSession.getSubject().getLabel());
        routingHeaders.put(PatientID, seleniumSession.getLabel());

        final ProjectReportPage projectReportPage = loginPage.seleniumLogin().createProject(testSpecificProject);
        new XnatCStore().data(TestData.SAMPLE_1).overwrittenHeaders(routingHeaders).sendDICOM();
        captureScreenshotlessStep();
        projectReportPage.loadProjectPrearchive().
                findOnlySessionInProject(testSpecificProject, PrearchiveStatus.RECEIVING).
                capture();
        waitForSessionRebuilder();

        home().
                captureRecentSession(seleniumSession, 90).
                clickRecentSession(seleniumSession).
                capture().
                assertAutoRunCompletion().
                assertSnapshotGeneration().
                logLogout();
    }

}
