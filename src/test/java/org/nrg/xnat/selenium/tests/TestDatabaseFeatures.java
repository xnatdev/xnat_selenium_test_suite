package org.nrg.xnat.selenium.tests;

import org.apache.log4j.Logger;
import org.nrg.jira.components.zephyr.TestStatus;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.annotations.DisableWebDriver;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.testing.TestNgUtils;
import org.nrg.testing.annotations.DisallowXnatVersion;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.database.XnatDatabase;
import org.nrg.testing.xnat.database.XnatDatabaseCommand;
import org.nrg.testing.xnat.ssh.SSHConnection;
import org.nrg.xnat.rest.Credentials;
import org.nrg.xnat.rest.XnatAliasToken;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.sql.*;

import static org.testng.AssertJUnit.assertEquals;

@TestRequires(db = true, ssh = true)
public class TestDatabaseFeatures extends BaseSeleniumTest {
    // This class is not for every test that touches the database, but rather for tests where the majority/entirety of the test is DB interaction

    private boolean tcStartNeeded = false;
    private final SSHConnection sshConnection = new SSHConnection();
    private static final Logger LOGGER = Logger.getLogger(TestDatabaseFeatures.class);

    @Test
    @DisableWebDriver
    @JiraKey(simpleKey = "QA-321")
    public void testPrearchiveTableRecovery() {
        final int numPrearchiveColumns = 19;

        sshConnection.stopTomcat();
        tcStartNeeded = true;
        captureScreenshotlessStep();

        assertEquals(numPrearchiveColumns, getInformationSchemaColumnCount());
        captureScreenshotlessStep();

        new XnatDatabaseCommand("ALTER table xdat_search.prearchive DROP column lastmod, DROP column prevent_anon").execute();
        captureScreenshotlessStep();

        assertEquals(numPrearchiveColumns - 2, getInformationSchemaColumnCount());
        captureScreenshotlessStep();

        sshConnection.startTomcat();
        tcStartNeeded = false;
        captureScreenshotlessStep();

        assertEquals(numPrearchiveColumns, getInformationSchemaColumnCount());
        captureScreenshotlessStep();
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-368")
    public void testAliasTokenSecretColumnConversion() {
        sshConnection.stopTomcat();
        tcStartNeeded = true;
        captureScreenshotlessStep();

        new XnatDatabaseCommand("DELETE FROM xhbm_alias_token").execute();
        new XnatDatabaseCommand("ALTER TABLE xhbm_alias_token DROP COLUMN secret").execute();
        new XnatDatabaseCommand("ALTER TABLE xhbm_alias_token ADD COLUMN secret bigint").execute();
        captureScreenshotlessStep();

        assertEquals("int8", XnatDatabase.getTableColumnType("xhbm_alias_token", "secret"));
        captureScreenshotlessStep();

        sshConnection.startTomcat();
        tcStartNeeded = false;
        captureScreenshotlessStep();

        final IndexPage indexPage = loginPage.seleniumLogin();

        assertEquals("varchar", XnatDatabase.getTableColumnType("xhbm_alias_token", "secret"));
        captureScreenshotlessStep();

        restDriver.interfaceFor(mainUser).regenerateUserSession();
        final XnatAliasToken token = mainInterface().generateAliasToken();
        final String tokenString = String.format("Alias token: %s:%s", token.getAlias(), token.getSecret());
        captureStep(stepCounter, TestStatus.PASS, tokenString, false);
        LOGGER.info(tokenString);

        Credentials.build(token).get(formatRestUrl("user", Settings.MAIN_USERNAME, "sessions")).then().assertThat().statusCode(200);
        captureScreenshotlessStep();

        indexPage.logLogout();
    }

    @AfterMethod
    public void restoreTomcat(ITestResult result) {
        if (tcStartNeeded) {
            LOGGER.warn(TestNgUtils.getTestName(result) + " failed after stopping tomcat. Attempting to start it back up to avoid subsequent failures...");
            sshConnection.startTomcat();
        }
    }

    private int getInformationSchemaColumnCount() {
        try {
            final ResultSet resultSet = new XnatDatabaseCommand("SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = 'xdat_search' AND table_name = 'prearchive'").execute().getResultSet();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException sqle) {
            throw new AssertionError("Failed to execute or parse response for DB command. Exception: ", sqle);
        }
    }

}
