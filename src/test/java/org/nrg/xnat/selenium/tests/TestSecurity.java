package org.nrg.xnat.selenium.tests;

import com.google.common.base.Joiner;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.nrg.jira.components.zephyr.TestStatus;
import org.nrg.selenium.*;
import org.nrg.selenium.xnat.page_model.*;
import org.nrg.selenium.xnat.page_model.admin.AdminUiPage;
import org.nrg.selenium.xnat.page_model.general_xnat.GeneralXnatErrorPage;
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage;
import org.nrg.selenium.xnat.page_model.ui_element.popup.AdminUiPageSessionExpiringDialog;
import org.nrg.selenium.xnat.page_model.ui_element.popup.IndexPageSessionExpiringDialog;
import org.nrg.selenium.xnat.page_model.ui_element.popup.SessionExpiringDialog;
import org.nrg.selenium.xnat.page_model.users.AdminUsersPage;
import org.nrg.selenium.xnat.page_model.users.RegistrationPage;
import org.nrg.selenium.xnat.page_model.users.UserSelfServicePage;
import org.nrg.testing.TimeUtils;
import org.nrg.testing.annotations.*;
import org.nrg.testing.email.EmailQuery;
import org.nrg.testing.email.XnatEmailCatalog;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.Users;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.SiteConfigProps;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.rest.Credentials;
import org.nrg.xnat.rest.XnatAliasToken;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_7_2;
import org.nrg.xnat.versions.Xnat_1_7_5;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.AssertJUnit.*;

public class TestSecurity extends BaseSeleniumTest {

    private final String curentPasswordMissing = "Current Password: field cannot be empty";
    private final String customMessage = "You call that a password?";
    private final Logger logger = Logger.getLogger(TestSecurity.class);

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-272")
    public void testChangePassword() {
        final String updateUrl = formatXnatUrl("/app/template/XDATScreen_UpdateUser.vm");
        String currentPass = RandomHelper.randomString(14);
        final User newUser = Users.genericAccount().firstName("Such").lastName("Test").password(currentPass);

        final NavigablePageObject<?> adminPageObject = loginPage.seleniumAdminLogin().loadAdminUsersPage().createUser(newUser);
        for (int i = 0; i < 2; i++) {
            String newPass = RandomHelper.randomFullString(18);
            currentPass = newUser.getPassword();

            (i == 1 ? loginPage.seleniumAdminLogin() : adminPageObject).loadAdminUi().toggleLoginRequired().logLogout().waitForLogout();
            navigate().to(updateUrl);
            final UserSelfServicePage selfServicePage = loginPage.assertRedirectionToLoginPage().capture().login(newUser).loadUserSelfServicePage(newUser).
                    fillNewPasswordField(newPass).fillNewPasswordConfirmationField(newPass).uploadScreenshot().clickChangePasswordButton();
            selfServicePage.readFormValidationModal().assertErrorsEqual(Collections.singletonList(curentPasswordMissing)).capture().clickOK();
            selfServicePage.logLogout().loadLoginPage().assertFailedLogin(newUser.password(newPass)).login(newUser.password(currentPass)).loadUserSelfServicePage(newUser);

            newPass = RandomHelper.randomFullString(18);
            selfServicePage.changePassword(newPass).logLogout().loadLoginPage().assertFailedLogin(newUser.password(currentPass)).login(newUser.password(newPass)).logLogout();
        }
    }

    @Test
    @Basic
    @JiraKey(simpleKey = "QA-431", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-150")
    })
    public void testUserCreation() {
        // Makes sure XNAT-2984 is fixed
        final String username = "test" + TimeUtils.getTimestamp("MMddHHmmss");
        final User user = new User(username).password(username).firstName(RandomHelper.randomLetters(10) + " '-").lastName(RandomHelper.randomLetters(10) + " '-").email(Settings.EMAIL);

        loginPage.seleniumAdminLogin().loadAdminUsersPage().createUser(user).logLogout().login(user).logLogout();
    }

    @Test
    @RequireXnatVersion(allowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-151")
    public void testPARs() {
        // JIRA ticket: XNAT-2576
        final List<Integer> randomIds = RandomHelper.randomIntegers(8, 1, 50, false);
        final User parUser = new User(RandomHelper.randomID(12)).password(RandomHelper.randomFullString(16)).firstName("PAR").lastName("TEST").email(Settings.permuteSeleniumEmail());

        loginPage.seleniumAdminLogin().loadAdminUi().setAutoEnable(false).logLogout();
        commentStep("par_ids: " + Joiner.on(", ").join(randomIds));
        for (int parID : randomIds) {
            navigate().to(formatXnatUrl("/app/action/AcceptProjectAccess/par/", String.valueOf(parID)));
            loginPage.loadPage(RegistrationPage.class).fillCommonFields(parUser).clickRegisterButton().readWarning().assertTextEquals("Note:\nError Storing User.").uploadScreenshot();
        }
        captureScreenshotlessStep();

        final AdminUsersPage usersPage = loginPage.loadLoginPage().seleniumAdminLogin().loadAdminUsersPage().assertUserDoesNotExist(parUser.getUsername());
        captureScreenshotlessStep();
        usersPage.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-152")
    public void testSQL_Safety() {
        final String invalidStrings = "Found invalid value in submitted strings:";
        final String invalidAliases = "Found invalid value in submitted alias strings";
        final String sqlBomb = ")); update xhbm_xdat_user_auth set enabled=false;";
        final String escaper = RandomHelper.randomID(10).toLowerCase() + "\\";
        final String projectId = RandomHelper.randomID(12).toLowerCase();
        final String editId = RandomHelper.randomID(12).toLowerCase();
        final List<String> aliases = Arrays.asList(escaper, sqlBomb);

        loginPage.seleniumLogin().loadProjectCreationPage().createProjectWithExpectedFailure(new Project(projectId).title(sqlBomb).runningTitle(escaper)).assertTextContains(invalidStrings);
        captureScreenshotlessStep();
        home().loadProjectCreationPage().createProjectWithExpectedFailure(new Project(projectId).aliases(aliases)).assertTextContains(invalidAliases);
        captureScreenshotlessStep();

        mainQueryBase().put(formatRestUrl("projects", String.format("%s?name=%s&secondary_ID=%s", projectId, sqlBomb, escaper))).then().assertThat().statusCode(500);
        captureScreenshotlessStep();

        mainQueryBase().put(formatRestUrl("projects", String.format("%s?xnat:projectData/aliases/alias[0]/alias=%s&xnat:projectData/aliases/alias[1]/alias=%s", projectId, escaper, sqlBomb))).then().assertThat().statusCode(500);
        captureScreenshotlessStep();

        final ProjectEditPage editPage = home().createProject(new Project(editId)).loadEditPage().fillProjectTitle(sqlBomb).fillRunningTitle(escaper);
        editPage.submitForm();
        editPage.readAlert().assertTextContains(invalidStrings).capture();
        navigate().back();

        editPage.fillProjectTitle(editId).fillRunningTitle(editId).fillAliases(aliases).submitForm();
        editPage.readAlert().assertTextContains(invalidAliases).capture();

        editPage.logLogout().seleniumLogin().assertLogin(mainUser).logLogout(); // make sure we didn't nuke the user table ;)
    }

    @Test
    @JiraKey(simpleKey = "QA-432", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-153")
    })
    public void testPasswordPhish() {
        // JIRA ticket: Ensures XXX-14 is fixed

        final String password = RandomHelper.randomString(14);
        final User phishUser = Users.genericAccount().firstName("Phish").lastName("Test").password(password);
        final String phishPassword = "test123";

        final IndexPage indexPage = loginPage.seleniumAdminLogin().loadAdminUsersPage().createUser(phishUser).logLogout().loadLoginPage().login(phishUser);
        navigate().to(formatXnatUrl("/app/action/XDATChangePassword?xdat:user.primary_password=" + phishPassword));

        loadPage(GeneralXnatErrorPage.class).capture();
        navigate().back();
        indexPage.logLogout().loadLoginPage().assertFailedLogin(phishUser.password(phishPassword)).login(phishUser.password(password)).assertLogin(phishUser).logLogout();
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-309")
    public void testSessionTimeout() {
        final String originalTimeout = mainAdminInterface().readSiteConfigPreference(SiteConfigProps.SESSION_TIMEOUT);
        try {
            final IndexPage indexPage = loginPage.seleniumAdminLogin()
                    .loadAdminUi()
                    .setSessionTimeout("2 minutes")
                    .setLoginRequired(true)
                    .logLogout()
                    .seleniumLogin();
            check2MinuteTimeout(mainUser, indexPage, IndexPageSessionExpiringDialog.class);
            final AdminUiPage adminUiPage = loadPage(XnatLoginPage.class)
                    .seleniumAdminLogin()
                    .loadAdminUi();
            check2MinuteTimeout(mainAdminUser, adminUiPage, AdminUiPageSessionExpiringDialog.class);
            loadPage(XnatLoginPage.class)
                    .seleniumAdminLogin()
                    .loadAdminUi()
                    .restoreDefaultSessionTimeout()
                    .logLogout();
        } finally {
            mainAdminInterface().postSiteConfigProperty(SiteConfigProps.SESSION_TIMEOUT, originalTimeout);
        }
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-366")
    public void testAliasTokenTimeout() {
        final int cronFreq = 10;
        final String cronFreqString = TimeUtils.cronEveryNSeconds(cronFreq);
        final int aliasTimeout = 30;
        final String aliasTimeoutInterval = aliasTimeout + " seconds";
        final int remainingDuration = 5;
        final int leeway = 5;

        final AdminUiPage adminUiPage = loginPage.seleniumLogin().createProject(testSpecificProject).logLogout().seleniumAdminLogin().loadAdminUi().setAliasTokenTimeout(aliasTimeoutInterval, cronFreqString);
        final XnatAliasToken token = mainInterface().generateAliasToken();
        final String tokenString = String.format("Alias token: %s:%s", token.getAlias(), token.getSecret());
        captureStep(stepCounter, TestStatus.PASS, tokenString, false);
        logger.info(tokenString);

        TimeUtils.sleep(1000 * (aliasTimeout - remainingDuration));
        Credentials.build(token).get(mainInterface().projectUrl(testSpecificProject)).then().assertThat().statusCode(200);
        captureScreenshotlessStep();
        logger.info("After waiting a little bit, alias token still valid...");
        TimeUtils.sleep(1000 * (remainingDuration + cronFreq + leeway)); // leeway gives the expiration task time to run
        Credentials.build(token).get(mainInterface().projectUrl(testSpecificProject)).then().assertThat().statusCode(Matchers.isOneOf(401, 403));
        captureScreenshotlessStep();
        adminUiPage.restoreDefaultAliasTokenTimeouts().logLogout();
    }

    private void assertPasswordComplexityWarnings() {
        new SyntheticPageObject(getXnatDriver()).readFormValidationModal().
                assertErrorsEqual(Arrays.asList(String.format("New Password: %s", customMessage), String.format("Confirm Password: %s", customMessage))).
                capture().clickOK();
    }

    @Test
    @TestRequires(email = true)
    @DisallowXnatVersion(disallowedVersions = {Xnat_1_6dev.class, Xnat_1_7_2.class}) // test written/locked to 1.7.3(+?) behavior
    @JiraKey(simpleKey = "QA-361")
    public void testPasswordComplexity() {
        final String regex = "^[mno]{10}\\d+$";
        final String badPassRegister = "moomoomooo";
        final String goodPassRegister = "nomnomnomm12345";
        final String badPassChange = "ononononono999";
        final String goodPassChange = "moooooooom000";
        final User user = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10)).password(badPassRegister);

        final RegistrationPage registrationPage = loginPage.seleniumAdminLogin().loadAdminUi().setAutoEnable(true).setPasswordComplexity(regex, customMessage).logLogout().loadLoginPage().clickRegisterLink().capture().
                fillCommonFields(user).uploadScreenshot().clickRegisterButton();
        commentStep("password = " + badPassRegister);

        assertPasswordComplexityWarnings();
        registrationPage.fillPassword(goodPassRegister).fillPasswordCheck(goodPassRegister);
        commentStep("password = " + goodPassRegister);
        registrationPage.clickRegisterButton().assertRegistrationRequiresEmailVerification().capture();
        user.password(goodPassRegister);
        new EmailQuery()
                .fromCatalog(XnatEmailCatalog.verificationEmail(user))
                .queryUntilSingleResult()
                .validateEach(message -> {
                    navigate().to(message.readVerificationEmailLink());
                });

        if (XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_7_5.class)) {
            new SyntheticPageObject(getXnatDriver()).loadPage(XnatLoginPage.class, false, true).assertVerificationMessageFor(user).login(user);
        } else {
            testController.getStepCounter().add(2);
        }

        final UserSelfServicePage userSelfServicePage = loadPage(IndexPage.class).loadUserSelfServicePage(user).fillCurrentPasswordField(user.getPassword()).fillNewPasswordField(badPassChange).fillNewPasswordConfirmationField(badPassChange);
        commentStep("password = " + badPassChange);
        userSelfServicePage.uploadScreenshot().clickChangePasswordButton();
        assertPasswordComplexityWarnings();
        userSelfServicePage.logLogout().login(user).loadUserSelfServicePage(user);
        commentStep("password = " + goodPassChange);
        userSelfServicePage.changePassword(goodPassChange).logLogout().login(user).logLogout().seleniumAdminLogin().loadAdminUi().restoreDefaultPasswordComplexity().logLogout();
    }

    private <X extends NavigablePageObject<X>> void check2MinuteTimeout(User user, X page, Class<? extends SessionExpiringDialog<X>> dialogClass) {
        page.assertSessionTimer(120, 5);
        TimeUtils.sleep(1000*110);
        assertEquals(user.getUsername(), loadPage(dialogClass).withUnderlyingPage(page).refreshExpiringSession(10, 120, 5).readCurrentLoggedInUsername());
        TimeUtils.sleep(1000*90);
        loadPage(dialogClass).assertRemainingSeconds(30, 5);
        captureScreenshotlessStep();
        TimeUtils.sleep(1000*35);
        page.assertSessionTimeout();
    }

}