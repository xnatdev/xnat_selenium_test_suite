package org.nrg.xnat.selenium.tests;

import org.apache.commons.lang3.StringUtils;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.download.DataDownloadPage;
import org.nrg.testing.DicomUtils;
import org.nrg.testing.FileIOUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Extensible;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.*;
import org.nrg.xnat.pogo.experiments.assessors.QC;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.experiments.sessions.PETSession;
import org.nrg.xnat.pogo.extensions.SimpleResourceFileExtension;
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension;
import org.nrg.xnat.pogo.resources.Resource;
import org.nrg.xnat.pogo.resources.ResourceFile;
import org.nrg.xnat.pogo.resources.SessionAssessorResource;
import org.nrg.xnat.pogo.resources.SubjectAssessorResource;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.util.ListUtils;
import org.nrg.xnat.versions.Xnat_1_7_4;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.nrg.testing.enums.TestData.*;
import static org.testng.AssertJUnit.*;

@TestRequires(admin = true,
    data = {
            MIXED_FRAME_STUDY, 
            DICOM_WEB_MRPR,
            DICOM_WEB_MISSINGNO_MR,
            DICOM_WEB_PETCT1,
            DICOM_WEB_MR1
    }
)
public class TestZipDownloader extends BaseSeleniumTest {

    private static final String SCANS = "scans";
    private static final String RESOURCES = "resources";
    private static final String ASSESSORS = "assessors";
    private static final List<String> DICOM_RESOURCE_NAMES = Arrays.asList("DICOM", "secondary");
    
    private final BinaryResourceValidator binaryResourceValidator = new BinaryResourceValidator();
    private final DicomResourceValidator dicomResourceValidator = new DicomResourceValidator();
    private final Map<Scan, Map<String, Attributes>> scanInstanceMap = new HashMap<>();
    
    private final Project publicProject = new Project().accessibility(Accessibility.PUBLIC);
    private final Subject publicSubject1 = new Subject(publicProject);
    private final MRSession publicSubject1Mr1 = new MRSession(publicProject, publicSubject1);
    private final MRSession publicSubject1Mr2 = new MRSession(publicProject, publicSubject1);
    private final Subject publicSubject2 = new Subject(publicProject);
    private final MRSession publicSubject2Mr1 = new MRSession(publicProject, publicSubject2);
    private final QC publicSubject2Mr1QC = new QC(publicProject, publicSubject2, publicSubject2Mr1);
    private final Project privateProject = new Project().accessibility(Accessibility.PRIVATE).addMember(mainUser);
    private final Subject privateSubject1 = new Subject(privateProject);
    private final PETSession privateSubject1Pet1 = new PETSession(privateProject, privateSubject1);
    private final Subject privateSubject2 = new Subject(privateProject);
    private final MRSession privateSubject2Mr1 = new MRSession(privateProject, privateSubject2);
    private final List<Project> allProjects = Arrays.asList(publicProject, privateProject);

    @BeforeClass
    public void addRequiredData() {
        mainAdminInterface().disableSiteAnonScript();

        final Map<ImagingSession, TestData> sessionTestDataMap = new HashMap<>();
        sessionTestDataMap.put(publicSubject1Mr1, MIXED_FRAME_STUDY);
        sessionTestDataMap.put(publicSubject1Mr2, DICOM_WEB_MRPR);
        sessionTestDataMap.put(publicSubject2Mr1, DICOM_WEB_MISSINGNO_MR);
        sessionTestDataMap.put(privateSubject1Pet1, DICOM_WEB_PETCT1);
        sessionTestDataMap.put(privateSubject2Mr1, DICOM_WEB_MR1);

        for (Map.Entry<ImagingSession, TestData> sessionStudyEntry : sessionTestDataMap.entrySet()) {
            registerStudy(sessionStudyEntry.getKey(), sessionStudyEntry.getValue());
        }

        final Resource sessionResource = new SubjectAssessorResource(publicProject, publicSubject2, publicSubject2Mr1, "RESOURCES");
        new ResourceFile(sessionResource, "resourcefile.txt").extension(new SimpleResourceFileExtension(getDataFile("seleniumDataA.txt")));
        new ResourceFile(sessionResource, "resourcefile2.txt").extension(new SimpleResourceFileExtension(getDataFile("seleniumDataB.txt")));

        final Resource assessorResource = new SessionAssessorResource(publicProject, publicSubject2, publicSubject2Mr1, publicSubject2Mr1QC, "DATA");
        new ResourceFile(assessorResource, "zippy.zip").extension(new SimpleResourceFileExtension(getDataFile("seleniumDataZ1.zip")));

        for (Project project : allProjects) {
            mainAdminInterface().createProject(project);
            for (Subject subject : project.getSubjects()) {
                for (ImagingSession session : subject.getSessions()) {
                    mainAdminInterface().waitForAutoRun(session, 120);
                    for (Resource resource : session.getResources()) {
                        restDriver.interfaceFor(mainAdminUser).readResourceFiles(resource); // easy way to get all of the MD5s that XNAT populated
                    }
                    session.assessors(mainAdminInterface().readSessionAssessors(project, subject, session)); // easy way to get all of the MD5s that XNAT populated
                    mainAdminInterface().readScans(project, subject, session);
                }
            }
        }
        
        for (Map.Entry<ImagingSession, TestData> sessionStudyEntry : sessionTestDataMap.entrySet()) {
            addInstances(sessionStudyEntry.getKey(), sessionStudyEntry.getValue());
        }
    }

    @Test
    @JiraKey(simpleKey = "QA-507")
    public void testBasicSessionDownload() {
        new DownloadTest().user(mainUser).entryPoint(new SingleSessionEntryPoint(publicSubject1Mr1)).session(publicSubject1Mr1).execute();
    }

    @Test
    @TestRequires(openXnat = true)
    @JiraKey(simpleKey = "QA-508")
    public void testBasicSessionGuestDownload() {
        new DownloadTest().entryPoint(new SingleSessionEntryPoint(publicSubject1Mr1)).session(publicSubject1Mr1).execute();
    }

    @Test
    @JiraKey(simpleKey = "QA-509")
    public void testAllProjectSessionsDownload() {
        new DownloadTest().user(mainUser).includeProject().includeSubject().entryPoint(new ProjectDownloadAction(privateProject)).sessions(Arrays.asList(privateSubject1Pet1, privateSubject2Mr1)).execute();
    }

    @Test
    @JiraKey(simpleKey = "QA-510")
    public void testDicomOnlyMRSessionsDownload() {
        new DownloadTest().user(mainUser).entryPoint(new SiteDataListingEntryPoint(DataType.MR_SESSION)).sessions(Arrays.asList(publicSubject1Mr1, publicSubject1Mr2, publicSubject2Mr1, privateSubject2Mr1)).scanFormats(Collections.singletonList("DICOM")).execute();
    }

    @Test
    @JiraKey(simpleKey = "QA-511")
    public void testFilteredScanTypesAndFormatsDownload() {
        new DownloadTest().user(mainUser).entryPoint(new SiteDataListingEntryPoint(DataType.MR_SESSION)).sessions(Arrays.asList(publicSubject1Mr1, publicSubject1Mr2, publicSubject2Mr1, privateSubject2Mr1)).scanFormats(Collections.singletonList("SNAPSHOTS")).scanTypes(Collections.singletonList("PDW_TSE")).execute();
    }

    @Test
    @JiraKey(simpleKey = "QA-512")
    public void testSessionFilteredDownload() {
        new DownloadTest().user(mainUser).entryPoint(new SiteDataListingEntryPoint(DataType.MR_SESSION)).session(publicSubject1Mr2).execute();
    }

    @Test
    @JiraKey(simpleKey = "QA-513")
    public void testProjectSearchDownload() {
        new DownloadTest().user(mainUser).includeSubject().entryPoint(new ProjectStoredSearchEntryPoint(publicProject, DataType.MR_SESSION.getPluralName())).sessions(Arrays.asList(publicSubject1Mr1, publicSubject1Mr2, publicSubject2Mr1)).execute();
    }

    private class DownloadTest {
        private User user;
        private EntryPoint entryPoint;
        private final List<ImagingSession> sessions = new ArrayList<>();
        private final List<String> scanFormats = new ArrayList<>(); // i.e. scan resource
        private final List<String> scanTypes = new ArrayList<>();
        private final List<String> sessionResources = new ArrayList<>();
        private final List<DataType> assessors = new ArrayList<>();
        private boolean allFormats = true;
        private boolean allTypes = true;
        private boolean allSessionResources = true;
        private boolean allAssessors = true;
        private boolean includeProject = false;
        private boolean includeSubject = false;
        private boolean simplifyArchiveStructure = true;

        private final Map<Project, Set<Subject>> projectSubjectMap = new HashMap<>();
        private final Map<Subject, Set<ImagingSession>> subjectSessionMap = new HashMap<>();
        private final Map<ImagingSession, Set<Scan>> expectedScans = new HashMap<>();
        private final Map<Scan, Collection<Resource>> expectedScanResources = new HashMap<>();
        private final Map<ImagingSession, Collection<Resource>> expectedSessionResources = new HashMap<>();
        private final Map<ImagingSession, Collection<SessionAssessor>> expectedSessionAssessors = new HashMap<>();

        DownloadTest user(User user) {
            this.user = user;
            return this;
        }

        DownloadTest entryPoint(EntryPoint entryPoint) {
            this.entryPoint = entryPoint;
            return this;
        }

        DownloadTest session(ImagingSession session) {
            return sessions(Collections.singletonList(session));
        }

        DownloadTest sessions(List<ImagingSession> sessions) {
            ListUtils.copyInto(sessions, this.sessions);
            return this;
        }

        DownloadTest scanFormats(List<String> scanFormats) {
            ListUtils.copyInto(scanFormats, this.scanFormats);
            allFormats = false;
            return this;
        }

        DownloadTest scanTypes(List<String> scanTypes) {
            ListUtils.copyInto(scanTypes, this.scanTypes);
            allTypes = false;
            return this;
        }

        DownloadTest sessionResources(List<String> resources) {
            ListUtils.copyInto(resources, sessionResources);
            allSessionResources = false;
            return this;
        }

        DownloadTest assessors(List<DataType> assessors) {
            ListUtils.copyInto(assessors, this.assessors);
            allAssessors = false;
            return this;
        }

        DownloadTest includeProject() {
            includeProject = true;
            return this;
        }

        DownloadTest includeSubject() {
            includeSubject = true;
            return this;
        }

        DownloadTest disabledSimplifiedArchiveStructure() {
            simplifyArchiveStructure = false;
            return this;
        }

        void execute() {
            validateDownload(performWebappPart());
        }

        private Path performWebappPart() {
            final IndexPage indexPage = (user == null) ? home() : loginPage.login(user);
            final DataDownloadPage downloadPage = entryPoint.loadZipDownloader(indexPage).
                    capture().
                    uncheckAllSessions().
                    checkSessions(sessions);
            if (!allFormats) {
                downloadPage.uncheckAllFormats().checkFormats(scanFormats);
            }
            if (!allTypes) {
                downloadPage.uncheckAllScanTypes().checkScanTypes(scanTypes);
            }
            if (!allSessionResources) {
                downloadPage.uncheckAllResources().checkResources(sessionResources);
            }
            if (!allAssessors) {
                downloadPage.uncheckAllAssessors().checkAssessors(assessors);
            }
            final File downloadedZip = downloadPage.markProjectInclusionSetting(includeProject).
                    markSubjectInclusionSetting(includeSubject).
                    markSimplifiedDownloadStructureSetting(simplifyArchiveStructure).
                    selectZipDownloadOption().
                    capture().
                    downloadAsFile();

            if (user != null) {
                logLogout();
            }
            final String downloadId = StringUtils.split(downloadedZip.getName(), '.')[0];
            final Path unzippedOuterFolder = Paths.get(Settings.TEMP_SUBDIR, downloadId);
            FileIOUtils.unzip(unzippedOuterFolder.toFile(), downloadedZip);
            return XnatTestingVersionManager.testedVersionFollows(Xnat_1_7_4.class) ? unzippedOuterFolder : unzippedOuterFolder.resolve(downloadId);
        }

        private void validateDownload(Path unzippedDownload) {
            constructExpectations();
            if (includeProject) {
                final Collection<Project> projects = projectSubjectMap.keySet();
                assertDirectoryContainsExactly(unzippedDownload, projects);
                if (includeSubject) {
                    for (Project project : projects) {
                        final Set<Subject> subjects = projectSubjectMap.get(project);
                        final Path projectDir = unzippedDownload.resolve(getDirectoryRepresentation(project));
                        assertDirectoryContainsExactly(projectDir, subjects);
                        for (Subject subject : subjects) {
                            final Set<ImagingSession> sessions = subjectSessionMap.get(subject);
                            final Path subjectDir = projectDir.resolve(getDirectoryRepresentation(subject));
                            assertDirectoryContainsExactly(subjectDir, sessions);
                        }
                    }
                } else {
                    for (Project project : projectSubjectMap.keySet()) {
                        final Path projectDir = unzippedDownload.resolve(getDirectoryRepresentation(project));
                        assertDirectoryContainsExactly(projectDir, flattenSessions(project));
                    }
                }
            } else if (includeSubject) {
                Collection<Subject> subjects = flattenSubjects();
                assertDirectoryContainsExactly(unzippedDownload, subjects);
                for (Subject subject : subjects) {
                    final Path subjectDir = unzippedDownload.resolve(getDirectoryRepresentation(subject));
                    assertDirectoryContainsExactly(subjectDir, subjectSessionMap.get(subject));
                }
            } else {
                assertDirectoryContainsExactly(unzippedDownload, flattenSessions());
            }
            // We have now checked that the download contains only the sessions specified, in the correct places. Next is to check that each of the sessions is correct.

            for (Map.Entry<ImagingSession, Set<Scan>> sessionScanEntry : expectedScans.entrySet()) {
                final ImagingSession session = sessionScanEntry.getKey();
                final Path sessionDirectory = pathForSessionDirectory(unzippedDownload, session);
                final Set<Scan> scans = expectedScans.getOrDefault(session, new HashSet<>());
                final Collection<Resource> resources = expectedSessionResources.getOrDefault(session, new HashSet<>());
                final Collection<SessionAssessor> assessors = expectedSessionAssessors.getOrDefault(session, new HashSet<>());
                if (simplifyArchiveStructure) {
                    final Set<Extensible<?>> expectedObjectsInSessionDir = new HashSet<>();
                    expectedObjectsInSessionDir.addAll(scans);
                    expectedObjectsInSessionDir.addAll(resources);
                    expectedObjectsInSessionDir.addAll(assessors);
                    assertDirectoryContainsExactly(sessionDirectory, expectedObjectsInSessionDir);
                } else {
                    final Set<String> subdirectories = new HashSet<>();
                    if (!scans.isEmpty()) {
                        subdirectories.add(SCANS);
                    }
                    if (!resources.isEmpty()) {
                        subdirectories.add(RESOURCES);
                    }
                    if (!assessors.isEmpty()) {
                        subdirectories.add(ASSESSORS);
                    }
                    assertDirectoryContainsExactlySubdirectories(sessionDirectory, subdirectories);
                    if (!scans.isEmpty()) {
                        assertDirectoryContainsExactly(sessionDirectory.resolve(SCANS), scans);
                    }
                    if (!resources.isEmpty()) {
                        assertDirectoryContainsExactly(sessionDirectory.resolve(RESOURCES), resources);
                    }
                    if (!assessors.isEmpty()) {
                        assertDirectoryContainsExactly(sessionDirectory.resolve(ASSESSORS), assessors);
                    }
                }
                // We have now checked that the session folder (one level deeper if archive structure is unsimplified) contains only the relevant scans, resources, and assessors (regardless of archive structure flag)
                for (Scan scan : scans) {
                    final Path scanResourcesPath = pathToScanResources(sessionDirectory, scan);
                    final Collection<Resource> scanResources = expectedScanResources.get(scan);
                    assertDirectoryContainsExactly(scanResourcesPath, scanResources);
                    final List<Resource> dicomResources = new ArrayList<>();
                    for (Resource resource : scanResources) {
                        if (DICOM_RESOURCE_NAMES.contains(resource.getFolder())) {
                            dicomResources.add(resource);
                        } else {
                            binaryResourceValidator.validate(scanResourcesPath, resource);
                        }
                    }
                    if (!dicomResources.isEmpty()) {
                        dicomResourceValidator.validate(scanResourcesPath, dicomResources);
                    }

                }
                for (Resource resource : resources) {
                    binaryResourceValidator.validate(pathToSessionResources(sessionDirectory), resource);
                }
                for (SessionAssessor assessor : assessors) {
                    final Path assessorResourcesPath = pathToAssessorResources(sessionDirectory, assessor);
                    final List<Resource> assessorResources = assessor.getResources();
                    assertDirectoryContainsExactly(assessorResourcesPath, assessorResources);
                    for (Resource resource : assessorResources) {
                        binaryResourceValidator.validate(assessorResourcesPath, resource);
                    }
                }
            }
            captureScreenshotlessStep();
        }

        private void constructExpectations() {
            for (ImagingSession session : sessions) {
                boolean sessionDownloadable = false; // at least one resource for the session (in a scan, session resource, or assessor) needs to not be filtered out for it to be downloaded
                final List<Scan> filteredScans = allTypes ? session.getScans() : session.filterScansByType(scanTypes);
                for (Scan scan : filteredScans) {
                    final Collection<Resource> filteredResources;
                    if (allFormats) {
                        filteredResources = scan.getScanResources();
                    } else {
                        filteredResources = new HashSet<>();
                        for (Resource resource : scan.getScanResources()) {
                            if (scanFormats.contains(resource.getFolder())) filteredResources.add(resource);
                        }
                    }
                    if (!filteredResources.isEmpty()) {
                        expectedScanResources.put(scan, filteredResources);
                        final Set<Scan> scans = expectedScans.computeIfAbsent(session, k -> new HashSet<>());
                        scans.add(scan);
                        sessionDownloadable = true;
                    }
                }
                final Collection<Resource> filteredSessionResources;
                if (allSessionResources) {
                    filteredSessionResources = session.getResources();
                } else {
                    filteredSessionResources = new HashSet<>();
                    for (Resource resource : session.getResources()) {
                        if (sessionResources.contains(resource.getFolder())) filteredSessionResources.add(resource);
                    }
                }
                if (!filteredSessionResources.isEmpty()) {
                    expectedSessionResources.put(session, filteredSessionResources);
                    sessionDownloadable = true;
                }
                final List<SessionAssessor> filteredAssessors = allAssessors ? session.getAssessors() : session.filterAssessorsByDataType(assessors);
                final Set<SessionAssessor> actualAssessors = new HashSet<>();
                for (SessionAssessor assessor : filteredAssessors) {
                    if (!assessor.getResources().isEmpty()) {
                        actualAssessors.add(assessor);
                    }
                }
                if (!actualAssessors.isEmpty()) {
                    expectedSessionAssessors.put(session, actualAssessors);
                    sessionDownloadable = true;
                }

                if (sessionDownloadable) {
                    final Subject subject = session.getSubject();
                    final Project project = subject.getProject();
                    final Set<Subject> subjectsInProject = projectSubjectMap.computeIfAbsent(project, k -> new HashSet<>());
                    subjectsInProject.add(subject);
                    final Set<ImagingSession> sessionsForSubject = subjectSessionMap.computeIfAbsent(subject, k -> new HashSet<>());
                    sessionsForSubject.add(session);
                }
            }
        }

        private Path pathToScanResources(Path sessionPath, Scan scan) {
            return simplifyArchiveStructure ? sessionPath.resolve(getDirectoryRepresentation(scan)) : sessionPath.resolve(SCANS).resolve(getDirectoryRepresentation(scan)).resolve(RESOURCES);
        }

        private Path pathToSessionResources(Path sessionPath) {
            return simplifyArchiveStructure ? sessionPath : sessionPath.resolve(RESOURCES);
        }

        private Path pathToAssessorResources(Path sessionPath, SessionAssessor assessor) {
            return simplifyArchiveStructure ? sessionPath.resolve(getDirectoryRepresentation(assessor)) : sessionPath.resolve(ASSESSORS).resolve(getDirectoryRepresentation(assessor)).resolve(RESOURCES);
        }

        @SuppressWarnings("ConstantConditions")
        List<File> listDirectories(File baseDirectory) {
            final File[] directories = baseDirectory.listFiles();
            for (File file : directories) {
                assertTrue(file.isDirectory());
            }
            return Arrays.asList(directories);
        }

        private Set<String> nameSet(List<File> files) {
            final Set<String> names = new HashSet<>();
            for (File file : files) {
                names.add(file.getName());
            }
            return names;
        }

        private void assertDirectoryContainsExactly(Path directory, Collection<? extends Extensible<?>> subdirectories) {
            final Set<String> expectedSubdirNames = new HashSet<>();
            for (Extensible<?> extensible : subdirectories) {
                expectedSubdirNames.add(getDirectoryRepresentation(extensible));
            }
            assertDirectoryContainsExactlySubdirectories(directory, expectedSubdirNames);
        }

        private void assertDirectoryContainsExactlySubdirectories(Path directory, Collection<String> subdirectoryNames) {
            final List<File> actualSubdirs = listDirectories(directory.toFile());
            final Set<String> actualSubdirNames = nameSet(actualSubdirs);
            assertEquals(subdirectoryNames.size(), actualSubdirs.size());
            for (String subdirName : subdirectoryNames) {
                assertTrue(actualSubdirNames.contains(subdirName));
            }
        }

        private Path pathForSessionDirectory(Path rootDownload, ImagingSession session) {
            Path tempPath = rootDownload;
            if (includeProject) tempPath = tempPath.resolve(getDirectoryRepresentation(session.getPrimaryProject()));
            if (includeSubject) tempPath = tempPath.resolve(getDirectoryRepresentation(session.getSubject()));
            tempPath = tempPath.resolve(getDirectoryRepresentation(session));
            return tempPath;
        }

        private String getDirectoryRepresentation(Extensible<?> extensible) {
            if (extensible instanceof Project) {
                return ((Project) extensible).getId();
            } else if (extensible instanceof Subject) {
                return ((Subject) extensible).getLabel();
            } else if (extensible instanceof Experiment) {
                return ((Experiment) extensible).getLabel();
            } else if (extensible instanceof Scan) {
                return ((Scan) extensible).getId();
            } else if (extensible instanceof Resource) {
                return ((Resource) extensible).getFolder();
            }
            throw new UnsupportedOperationException("Can't call this method on that type of Extensible object.");
        }

        private Set<ImagingSession> flattenSessions(Project project) {
            final Set<ImagingSession> sessions = new HashSet<>();
            for (Subject subject : projectSubjectMap.get(project)) {
                sessions.addAll(subjectSessionMap.get(subject));
            }
            return sessions;
        }

        private Set<ImagingSession> flattenSessions() {
            final Set<ImagingSession> allSessions = new HashSet<>();
            for (Project project : projectSubjectMap.keySet()) {
                allSessions.addAll(flattenSessions(project));
            }
            return allSessions;
        }

        private Set<Subject> flattenSubjects() {
            final Set<Subject> subjects = new HashSet<>();
            for (Set<Subject> subjectSet : projectSubjectMap.values()) {
                subjects.addAll(subjectSet);
            }
            return subjects;
        }
    }

    private void registerStudy(ImagingSession session, TestData data) {
        new SessionImportExtension(session, data.toFile());
    }

    private void addInstances(ImagingSession session, TestData testData) {
        final Map<String, Scan> seriesUidMap = new HashMap<>();
        for (Scan scan : session.getScans()) {
            seriesUidMap.put(scan.getUid(), scan);
            scanInstanceMap.put(scan, new HashMap<>());
        }

        for (File sopInstance : FileIOUtils.listFilesRecursively(testData.toDirectory())) {
            final Attributes datasetWithFMI = DicomUtils.readDicom(sopInstance);
            final String seriesUid = datasetWithFMI.getString(Tag.SeriesInstanceUID);
            final Scan scan = seriesUidMap.get(seriesUid);
            if (scan == null) {
                fail("Could not find a scan with series instance UID = " + seriesUid);
            } else {
                scanInstanceMap.get(scan).put(datasetWithFMI.getString(Tag.SOPInstanceUID), datasetWithFMI);
            }
        }
    }

    private abstract static class EntryPoint {
        /**
         * Should navigate from the splash page to the zip downloader in the correct context
         */
        abstract DataDownloadPage loadZipDownloader(IndexPage indexPage);
    }

    private static class ProjectStoredSearchEntryPoint extends EntryPoint {
        private final Project project;
        private final String storedSearch;

        ProjectStoredSearchEntryPoint(Project project, String storedSearch) {
            this.project = project;
            this.storedSearch = storedSearch;
        }

        @Override
        DataDownloadPage loadZipDownloader(IndexPage indexPage) {
            return indexPage.searchForProject(project).
                    loadProjectDataTable().
                    switchToTab(storedSearch).
                    optionsDownload();
        }
    }

    private static class ProjectDownloadAction extends EntryPoint {
        private final Project project;

        ProjectDownloadAction(Project project) {
            this.project = project;
        }

        @Override
        DataDownloadPage loadZipDownloader(IndexPage indexPage) {
            return indexPage.searchForProject(project).clickDownloadImages();
        }

    }

    private static class SiteStoredSearchEntryPoint extends EntryPoint {
        private final String storedSearch;

        SiteStoredSearchEntryPoint(String storedSearch) {
            this.storedSearch = storedSearch;
        }

        @Override
        DataDownloadPage loadZipDownloader(IndexPage indexPage) {
            return indexPage.loadStoredSearch(storedSearch).
                    getDataTable().
                    optionsDownload();
        }
    }

    private static class SiteDataListingEntryPoint extends EntryPoint {
        private final DataType dataType;

        SiteDataListingEntryPoint(DataType dataType) {
            this.dataType = dataType;
        }

        @Override
        DataDownloadPage loadZipDownloader(IndexPage indexPage) {
            return indexPage.loadDataTypeListing(dataType).
                    getDataTable().
                    optionsDownload();
        }
    }

    private static class SingleSessionEntryPoint extends EntryPoint {
        private final ImagingSession session;

        SingleSessionEntryPoint(ImagingSession session) {
            this.session = session;
        }

        @Override
        DataDownloadPage loadZipDownloader(IndexPage indexPage) {
            return indexPage.searchForSession(session.getLabel()).clickDownloadImages();
        }
    }

    private class BinaryResourceValidator {
        void validate(Path allResourcesPath, Resource resource) {
            final Path resourcePath = allResourcesPath.resolve(resource.getFolder());
            for (ResourceFile resourceFile : resource.getResourceFiles()) {
                final File localCopy = resourcePath.resolve(resourceFile.getName()).toFile();
                assertTrue(localCopy.exists());
                assertEquals(resourceFile.getMd5(), FileIOUtils.calculateMD5(localCopy));
            }
        }
    }

    private class DicomResourceValidator {
        void validate(Path allResourcesPath, List<Resource> resources) {
            final Set<String> allProcessedSOPInstanceUIDs = new HashSet<>();
            final Map<String, Attributes> scanInstances = scanInstanceMap.get(resources.get(0).getScan());
            for (Resource resource : resources) {
                final File resourceDir = allResourcesPath.resolve(resource.getFolder()).toFile();
                File[] filesInDir = resourceDir.listFiles();
                assertNotNull(filesInDir);
                assertEquals(resource.getResourceFiles().size(), filesInDir.length);
                for (File file : filesInDir) {
                    assertTrue(file.isFile());
                    final Attributes sopInstanceInFile = DicomUtils.readDicom(file);
                    final String sopInstanceUID = sopInstanceInFile.getString(Tag.SOPInstanceUID);
                    assertFalse(allProcessedSOPInstanceUIDs.contains(sopInstanceUID));
                    allProcessedSOPInstanceUIDs.add(sopInstanceUID);
                    assertEquals(scanInstances.get(sopInstanceUID), sopInstanceInFile);
                }
            }
        }
    }

}
