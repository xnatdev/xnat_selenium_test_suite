package org.nrg.xnat.selenium.tests;

import io.restassured.response.Response;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.dcm4che3.data.Tag;
import org.nrg.jira.components.zephyr.TestStatus;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.DataUnit;
import org.nrg.selenium.enums.PrearchiveStatus;
import org.nrg.selenium.util.ResourceValidation;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.XnatLoginPage;
import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive;
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.enums.MergeBehavior;
import org.nrg.xnat.enums.PetMrProcessingSetting;
import org.nrg.xnat.importer.XnatImportRequest;
import org.nrg.xnat.importer.importers.DefaultImporterRequest;
import org.nrg.xnat.importer.importers.DicomZipRequest;
import org.nrg.xnat.importer.params.ManualRoutingRequest;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.experiments.sessions.PETMRSession;
import org.nrg.xnat.pogo.experiments.sessions.PETSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_8_10;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.testng.AssertJUnit.*;

@TestRequires(data = TestData.PETMR_DATA, dicomScp = true)
public class TestPetMrData extends BaseSeleniumTest {

    private final String commonNamingString = RandomHelper.randomString(5);
    private final Project petmrProject = new Project("PETMR_" + commonNamingString);
    private final Project petProject = new Project("PET_" + commonNamingString);
    private final Project separateProject = new Project("SEPARATE_" + commonNamingString);
    private final Project defaultProject = new Project("DEFAULT_" + commonNamingString);
    private final List<Project> petmrTestProjects = Arrays.asList(petmrProject, petProject, separateProject);
    private final TestData petmrTestData = TestData.PETMR_DATA;
    private final Map<DataType, String> xmlDisplayNameMap = new HashMap<>();

    @BeforeClass
    public void initPetMrTests() {
        setupProjects();
        xmlDisplayNameMap.put(DataType.PET_MR_SESSION, "xnat:PETMRSession");
        xmlDisplayNameMap.put(DataType.PET_SESSION, "xnat:PETSession");
        xmlDisplayNameMap.put(DataType.MR_SESSION, "xnat:MRSession");
    }

    @AfterMethod(alwaysRun = true)
    public void removeSessions() {
        for (Project project : Arrays.asList(petmrProject, petProject, separateProject, defaultProject)) {
            try {
                mainInterface().deleteAllProjectData(project);
            } catch (Exception | Error e) {
                final Response response = restDriver.interfaceFor(mainUser).jsonQuery().queryParam("columns", "ID,subject_ID").get(formatRestUrl("projects", project.getId(), "experiments"));
                Logger.getLogger(TestPetMrData.class).info(String.format("Repeated call to TestPetMrData.removeSessions():\nStatus code: %d\nResponse body: %s", response.statusCode(), response.body().asString()));
                throw e;
            }
        }
    }

    private void setupProjects() {
        final Map<Project, PetMrProcessingSetting> settingMap = new HashMap<>();
        settingMap.put(petmrProject, PetMrProcessingSetting.AS_PET_MR);
        settingMap.put(petProject, PetMrProcessingSetting.AS_PET);
        settingMap.put(separateProject, PetMrProcessingSetting.SPLIT);
        settingMap.put(defaultProject, PetMrProcessingSetting.DEFAULT_TO_SITE);
        constructXnatDriver();

        final IndexPage indexPage = loadPage(XnatLoginPage.class).seleniumLogin();
        for (Map.Entry<Project, PetMrProcessingSetting> entry : settingMap.entrySet()) {
            indexPage.createProject(entry.getKey()).loadManageTab().setPetMrSetting(entry.getValue());
        }
        indexPage.logout().quit();
    }

    @Test
    @JiraKey(simpleKey = "QA-115")
    public void testPETMR_Rebuild() {
        final String labelBase = "petmr_test";
        final PetMrSessionCollection petMrSessionCollection = new PetMrSessionCollection(
                XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_8_10.class) ? "petmrSub" : labelBase
        );
        final String sessionName = labelBase + "_PETMR";

        final XnatImportRequest<?> importerRequest;
        if (XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_8_10.class)) {
            importerRequest = new DefaultImporterRequest().
                    subject(labelBase).
                    exptLabel(sessionName).
                    overwrite(MergeBehavior.APPEND).
                    destPrearchive().
                    file(petmrTestData.toFile());
        } else {
            importerRequest = new DicomZipRequest()
                    .subject(labelBase)
                    .exptLabel(sessionName)
                    .overwrite(MergeBehavior.APPEND)
                    .file(petmrTestData.toFile());
        }

        final IndexPage indexPage = loginPage.seleniumLogin();

        for (Project project : petmrTestProjects) {
            indexPage.loadSubjectCreationPage().create(new Subject(project, labelBase));
            stepCounter.decrement();
            ((ManualRoutingRequest<?>) importerRequest).project(project);
            mainInterface().callImporter(importerRequest);
        }

        captureScreenshotlessStep();
        final XnatPrearchive prearchive = indexPage.loadPrearchive();
        prearchive.findSessionsInProjects(3, petmrTestProjects).assertProjectsInSubset(petmrTestProjects).checkAll().capture().rebuild();
        prearchive.findSessionsInProjects(4, petmrTestProjects, PrearchiveStatus.READY).capture().checkAll().capture().archive().capture();

        home().captureRecentSessions(petMrSessionCollection.toList());
        assertFullPETMRData(indexPage, petMrSessionCollection);
        clearProjectArchive(petmrTestProjects);

        indexPage.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-116")
    public void testPETMR_Autoarchive() {
        final String subjectLabel = "PETMR_test";
        final String sessionNamingBase = RandomHelper.randomLetters(10);
        final String sessionName = makePetMrLabel(sessionNamingBase);
        final String petName = makePetLabel(sessionNamingBase);
        final String separatedMr = makeMrLabel(sessionNamingBase);
        final PetMrSessionCollection petMrSessionCollection = new PetMrSessionCollection(sessionNamingBase);

        final IndexPage indexPage = loginPage.seleniumLogin();

        for (Project project : petmrTestProjects) {
            final Map<Integer, String> dicomHeaders = new HashMap<>();
            dicomHeaders.put(Tag.PatientName, subjectLabel);
            dicomHeaders.put(Tag.PatientID, sessionName);
            new XnatCStore().data(petmrTestData).overwrittenHeaders(dicomHeaders).sendDICOMToProject(project);
        }
        captureStep(
                stepCounter,
                TestStatus.PASS,
                String.format("PETMR = %s, PET = %s, separated PET = %s, separated MR = %s", sessionName, petName, petName, separatedMr),
                false
        );

        indexPage.waitForSessionRebuilder().captureRecentSessions(petMrSessionCollection.toList());
        assertFullPETMRData(indexPage, petMrSessionCollection);

        clearProjectArchive(petmrTestProjects);

        indexPage.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-280", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-117")
    })
    public void testSitePETMR_Rebuild() {
        final String subjectLabel = "PETMR_test";
        final String sessionNameBase = RandomHelper.randomLetters(10);
        Map<Integer, String> dicomHeaders = new HashMap<>();
        dicomHeaders.put(Tag.PatientName, subjectLabel);
        dicomHeaders.put(Tag.PatientID, makePetMrLabel(sessionNameBase));

        for (PetMrProcessingSetting setting : Arrays.asList(PetMrProcessingSetting.AS_PET_MR, PetMrProcessingSetting.AS_PET, PetMrProcessingSetting.SPLIT)) {
            final int numSessionsAfterBuild = (setting == PetMrProcessingSetting.SPLIT) ? 2 : 1;
            final IndexPage indexPage = loadPage(XnatLoginPage.class).seleniumAdminLogin().loadAdminUi().setSitePetMrSetting(setting).logLogout().seleniumLogin();
            new XnatCStore().data(petmrTestData).overwrittenHeaders(dicomHeaders).sendDICOMToProject(defaultProject);
            captureScreenshotlessStep();

            indexPage.loadPrearchive().findOnlySessionInProject(defaultProject).capture().rebuild().
                    findSessionsInProject(numSessionsAfterBuild, defaultProject, PrearchiveStatus.READY).capture().checkAll().
                    capture().archive().capture().home();

            assertSitePETMR(indexPage, setting, sessionNameBase);

            clearProjectArchive(Collections.singletonList(defaultProject));
            indexPage.logLogout();
        }
    }

    @Test
    @JiraKey(simpleKey = "QA-281", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-118")
    })
    public void testSitePETMR_Autoarchive() {
        // JIRA ticket: XT-198 (close enough)
        mainAdminInterface().setSessionXmlRebuilderTimes(1, 10000);

        final String subjectId = "PETMR_test";
        final String sessionNameBase = RandomHelper.randomLetters(10);
        Map<Integer, String> dicomHeaders = new HashMap<>();
        dicomHeaders.put(Tag.PatientName, subjectId);
        dicomHeaders.put(Tag.PatientID, makePetMrLabel(sessionNameBase));

        for (PetMrProcessingSetting setting : Arrays.asList(PetMrProcessingSetting.AS_PET_MR, PetMrProcessingSetting.AS_PET, PetMrProcessingSetting.SPLIT)) {
            final IndexPage indexPage = loadPage(XnatLoginPage.class).seleniumAdminLogin().
                    loadAdminUi().
                    setSitePetMrSetting(setting).
                    logLogout().
                    seleniumLogin();
            new XnatCStore().data(petmrTestData).overwrittenHeaders(dicomHeaders).sendDICOMToProject(defaultProject);
            captureScreenshotlessStep();

            indexPage.waitForSessionRebuilder(1);
            assertSitePETMR(indexPage, setting, sessionNameBase);
            clearProjectArchive(Collections.singletonList(defaultProject));
            indexPage.logLogout();
        }
    }

    private void validateSessionExpectations(ImagingSessionReportPage sessionReportPage, DataType expectedDataType, ExpectedScanList expectedScanList) {
        assertTrue(sessionReportPage.getSessionTitle().startsWith(expectedDataType.getSingularName()));
        assertEquals(
                expectedDataType,
                mainInterface().readExperiment(sessionReportPage.readAccessionNumber(), ImagingSession.class).getDataType()
        );
        sessionReportPage.assertScansEqualTo(expectedScanList.getAssociatedScans());
        sessionReportPage.assertSummedScanResourcesRepresent(new ResourceValidation(expectedScanList.sizeInMB, DataUnit.MEGABYTES, expectedScanList.numScanFiles));
        captureScreenshotlessStep();
    }

    private void assertSitePETMR(IndexPage indexPage, PetMrProcessingSetting setting, String sessionNameBase) {
        switch (setting) {
            case AS_PET_MR :
                final ImagingSession petmr = new PETMRSession(makePetMrLabel(sessionNameBase)).project(defaultProject);
                indexPage.captureRecentSession(petmr).clickRecentSession(petmr);
                validateSessionExpectations(loadPage(ImagingSessionReportPage.class), DataType.PET_MR_SESSION, ExpectedScanList.PET_AND_MR);
                break;
            case AS_PET :
                final ImagingSession pet = new PETSession(makePetLabel(sessionNameBase)).project(defaultProject);
                indexPage.captureRecentSession(pet).clickRecentSession(pet);
                validateSessionExpectations(loadPage(ImagingSessionReportPage.class), DataType.PET_SESSION, ExpectedScanList.PET_AND_MR);
                break;
            case SPLIT :
                final ImagingSession separatedPet = new PETSession(makePetLabel(sessionNameBase)).project(defaultProject);
                final ImagingSession separatedMr = new MRSession(makeMrLabel(sessionNameBase)).project(defaultProject);
                indexPage.captureRecentSessions(Arrays.asList(separatedPet, separatedMr)).clickRecentSession(separatedPet);
                final ImagingSessionReportPage sessionPage = loadPage(ImagingSessionReportPage.class);
                validateSessionExpectations(sessionPage, DataType.PET_SESSION, ExpectedScanList.PET_ONLY);
                sessionPage.assertAutoRunCompletion().assertSnapshotGeneration().home();
                indexPage.clickRecentSession(separatedMr);
                validateSessionExpectations(loadPage(ImagingSessionReportPage.class), DataType.MR_SESSION, ExpectedScanList.MR_ONLY);
        }
        loadPage(ImagingSessionReportPage.class).assertAutoRunCompletion().assertSnapshotGeneration();
    }

    private void assertFullPETMRData(IndexPage indexPage, PetMrSessionCollection petMrSessionCollection) {
        final ImagingSessionReportPage genericSessionPageObject = indexPage.clickRecentSession(petMrSessionCollection.petMr);
        validateSessionExpectations(genericSessionPageObject, DataType.PET_MR_SESSION, ExpectedScanList.PET_AND_MR);

        home().clickRecentSession(petMrSessionCollection.pet);
        validateSessionExpectations(genericSessionPageObject, DataType.PET_SESSION, ExpectedScanList.PET_AND_MR);

        home().clickRecentSession(petMrSessionCollection.separatedPet);
        validateSessionExpectations(genericSessionPageObject, DataType.PET_SESSION, ExpectedScanList.PET_ONLY);

        home().clickRecentSession(petMrSessionCollection.separatedMr);
        validateSessionExpectations(genericSessionPageObject, DataType.MR_SESSION, ExpectedScanList.MR_ONLY);
    }

    private void clearProjectArchive(List<Project> testProjects) {
        for (Project project : testProjects) {
            home().searchForProject(project).loadDeleteComponent().clearProjectArchive();
            stepCounter.decrement();
        }
        stepCounter.increment();
    }

    private String makePetMrLabel(String sessionBase) {
        return sessionBase + "_PETMR";
    }

    private String makePetLabel(String sessionBase) {
        return sessionBase + "_PET";
    }

    private String makeMrLabel(String sessionBase) {
        return sessionBase + "_MR";
    }

    private class PetMrSessionCollection {
        private PETMRSession petMr;
        private PETSession pet;
        private PETSession separatedPet;
        private MRSession separatedMr;

        PetMrSessionCollection(String sessionBase) {
            petMr = new PETMRSession(petmrProject, makePetMrLabel(sessionBase));
            pet = new PETSession(petProject, makePetLabel(sessionBase));
            separatedPet = new PETSession(separateProject, makePetLabel(sessionBase));
            separatedMr = new MRSession(separateProject, makeMrLabel(sessionBase));
        }

        List<ImagingSession> toList() {
            return Arrays.asList(petMr, pet, separatedPet, separatedMr);
        }
    }

    private enum ExpectedScanList {
        MR_ONLY (new int[]{1, 2, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30}, 11.5, 71),
        PET_ONLY (new int[]{3, 4, 5, 32, 33, 35, 36, 38, 39, 41, 42}, 3.9, 35),
        PET_AND_MR (ArrayUtils.addAll(MR_ONLY.associatedScanIds, PET_ONLY.associatedScanIds), MR_ONLY.sizeInMB + PET_ONLY.sizeInMB, MR_ONLY.numScanFiles + PET_ONLY.numScanFiles);

        ExpectedScanList(int[] scanIds, double expectedSize, int numFiles) {
            associatedScanIds = scanIds;
            sizeInMB = expectedSize;
            numScanFiles = numFiles;
        }

        List<Scan> getAssociatedScans() {
            return Arrays.stream(associatedScanIds).
                    mapToObj(id -> new Scan().id(String.valueOf(id))).
                    collect(Collectors.toList());
        }

        private final int[] associatedScanIds;
        private final double sizeInMB;
        private final int numScanFiles;
    }

}
