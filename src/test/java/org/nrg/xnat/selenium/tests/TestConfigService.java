package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.xnat.page_model.project.report.ProjectManageTab;
import org.nrg.xnat.pogo.AnonScript;
import org.nrg.xnat.pogo.Project;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestConfigService extends BaseSeleniumTest {

    private final Project project = new Project();

    @BeforeClass
    public void addTestProject() {
        mainInterface().createProject(project);
    }

    @AfterClass(alwaysRun = true)
    public void cleanupTestProject() {
        restDriver.deleteProjectSilently(mainUser, project);
    }

    @Test // make sure XNAT-6874 doesn't come back. This is also technically not using the default config service API, but it's closely related
    public void testGuiAnonCrud() {
        final String anonContents = "-(0010,0020)\n";
        final ProjectManageTab manageTab = loginPage.seleniumLogin().
                searchForProject(project).
                loadManageTab().
                setAnonScript(true, new AnonScript().contents(anonContents)).
                disableAnonScript();
        manageTab.getDriver().navigate().refresh();
        assertFalse(manageTab.readAnonStatus());
        assertEquals(anonContents, manageTab.readAnonContents());
        manageTab.setAnonScript(true, null);
        manageTab.getDriver().navigate().refresh();
        assertTrue(manageTab.readAnonStatus());
        assertEquals(anonContents, manageTab.readAnonContents());
        manageTab.getProjectPage().logLogout();
    }

}
