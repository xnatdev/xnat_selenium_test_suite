package org.nrg.xnat.selenium.tests;

import com.google.common.collect.Sets;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.PrearchiveStatus;
import org.nrg.selenium.enums.SeriesImportFilterType;
import org.nrg.selenium.xnat.page_model.prearchive.PrearchiveSessionSubset;
import org.nrg.selenium.xnat.page_model.project.report.ProjectManageTab;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@TestRequires(data = TestData.SIF_SESSION, dicomScp = true)
public class TestSeriesImportFilters extends BaseSeleniumTest {

    private final TestData SIF_DATA = TestData.SIF_SESSION;
    private final Project SIF_PROJECT = new Project("SIF_" + RandomHelper.randomID(6));
    private final String SITE_SIF = "siteSeriesImportFilter.txt";
    private final String PROJECT_SIF = "projectSeriesImportFilter.txt";
    private final Set<Integer> SCANS_MATCHING_PROJECT_SIF = Sets.newHashSet(1, 2, 6, 7, 12);
    private final Set<Integer> SCANS_MATCHING_SITE_SIF = Sets.newHashSet(2, 8, 9, 10, 11, 13);
    private final Set<Integer> SCANS_MATCHING_NEITHER_SIF = Collections.singleton(14);

    @BeforeClass
    public void initSifTests() {
        setupProjects();
    }

    private void setupProjects() {
        mainInterface().createProject(SIF_PROJECT);
    }

    @Test
    @JiraKey(simpleKey = "QA-283", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-185")
    })
    public void testDicomReceiverBlacklist() {
        performDicomReceiverSIFTest(true, true);
    }

    @Test
    @JiraKey(simpleKey = "QA-284", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-186")
    })
    public void testDicomReceiverWhitelist() {
        performDicomReceiverSIFTest(false, true);
    }

    @Test
    @JiraKey(simpleKey = "QA-285", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-192")
    })
    public void testDicomReceiverBlacklistDisabled() {
        performDicomReceiverSIFTest(true, false);
    }

    @Test
    @JiraKey(simpleKey = "QA-286", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-193")
    })
    public void testDicomReceiverWhitelistDisabled() {
        performDicomReceiverSIFTest(false, false);
    }

    private void performDicomReceiverSIFTest(boolean isBlacklist, boolean isEnabled) {
        final MRSession session = new MRSession(SIF_PROJECT, "sifSession");
        final SeriesImportFilterType mode = (isBlacklist) ? SeriesImportFilterType.BLACKLIST : SeriesImportFilterType.WHITELIST;
        Set<Integer> scansRemaining;
        if (isEnabled) {
            scansRemaining = (isBlacklist) ? SCANS_MATCHING_NEITHER_SIF : Sets.intersection(SCANS_MATCHING_PROJECT_SIF, SCANS_MATCHING_SITE_SIF);
        } else {
            scansRemaining = Sets.union(Sets.union(SCANS_MATCHING_NEITHER_SIF, SCANS_MATCHING_SITE_SIF), SCANS_MATCHING_PROJECT_SIF);
        }
        final List<Scan> scanObjects = scansRemaining.stream().map(id -> new Scan().id(id.toString())).collect(Collectors.toList());

        final ProjectManageTab manageTab = loginPage.seleniumAdminLogin().loadAdminUi().setSiteSeriesImportFilter(isEnabled, mode, readDataFile(SITE_SIF)).logLogout().
                seleniumLogin().searchForProject(SIF_PROJECT).loadManageTab();
        manageTab.setProjectSeriesImportFilter(true, mode, readDataFile(PROJECT_SIF));
        if (!isEnabled) {
            stepCounter.decrement();
            manageTab.setProjectSeriesImportFilter(false, null, null);
            // we're setting it in two stages like this because we want to have it be "disabled" with a "reasonable" value in the disabled tests to make sure that
            // it really is disabled, and not just applying an empty blacklist
        }
        new XnatCStore().data(SIF_DATA).sendDICOMToProject(SIF_PROJECT);
        captureScreenshotlessStep();
        final PrearchiveSessionSubset checkedSession = manageTab.getProjectPage().loadProjectPrearchive().findOnlySessionInProject(SIF_PROJECT).checkAll().capture().rebuild().
                findOnlySessionInProject(SIF_PROJECT, PrearchiveStatus.READY).capture().checkAll();
        checkedSession.loadDetails().capture().assertScansEqualTo(scanObjects).returnToPrearchive();
        checkedSession.archive().capture().home().captureRecentSession(session).clickRecentSession(session).
                assertScansEqualTo(scanObjects).searchForProject(SIF_PROJECT).loadDeleteComponent().clearProjectArchive().
                logLogout().seleniumAdminLogin().loadAdminUi().setSiteSeriesImportFilter(false, null, null).logLogout();
    }

}
