package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.PrearchiveStatus;
import org.nrg.selenium.xnat.page_model.dicom.DicomDumpPage;
import org.nrg.selenium.xnat.page_model.prearchive.PrearchiveDetails;
import org.nrg.selenium.xnat.page_model.prearchive.PrearchiveSessionSubset;
import org.nrg.selenium.xnat.page_model.prearchive.XnatPrearchive;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.dicom.RootDicomObject;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.dcm4che3.data.Tag.*;

public class TestXSSAttacks extends BaseSeleniumTest {

    private static final TestData SAMPLE_1 = TestData.SAMPLE_1;

    @Test
    @TestRequires(data = TestData.SAMPLE_1, dicomScp = true)
    @JiraKey(simpleKey = "QA-379")
    public void testDicomXSS() {
        final String xssAttack = "<script>alert('XSS vulnerability confirmed.')</script>";
        final int xssTag = PerformingPhysicianName;
        final Project xssProject = new Project("XSS_" + RandomHelper.randomID(8));
        final Map<Integer, String> dicomHeaders = new HashMap<>();
        dicomHeaders.put(xssTag, xssAttack);
        final RootDicomObject expectedDicomObject = new RootDicomObject();
        expectedDicomObject.putValueEqualCheck(xssTag, xssAttack);
        final MRSession session = new MRSession(xssProject, "Sample_ID");

        final ProjectReportPage projectReportPage = loginPage.seleniumLogin().createProject(xssProject);
        new XnatCStore().data(SAMPLE_1).overwrittenHeaders(dicomHeaders).sendDICOMToProject(xssProject);
        captureScreenshotlessStep();
        final XnatPrearchive prearchive = projectReportPage.loadProjectPrearchive();
        final PrearchiveSessionSubset prearcSessionObject = prearchive.findOnlySessionInProject(xssProject).
                checkAll().
                capture().
                rebuild().
                findOnlySessionInProject(xssProject, PrearchiveStatus.READY).
                capture().
                checkAll();
        final DicomDumpPage<PrearchiveDetails> dicomDump = prearcSessionObject.loadDetails().
                capture().
                readDicomDump().
                validate(expectedDicomObject);
        captureScreenshotlessStep();
        dicomDump.returnToUnderlyingPage().returnToPrearchive();
        prearcSessionObject.archive().
                capture().
                home().
                captureRecentSession(session).
                clickRecentSession(session).
                viewFirstScan().
                readDicomDump().
                validate(expectedDicomObject).
                captureScreenshotlessStep();
        home().logLogout();
    }

}
