package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.*;
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.testing.TimeUtils;
import org.nrg.testing.annotations.*;
import org.nrg.testing.email.EmailQuery;
import org.nrg.testing.email.XnatEmailCatalog;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.pogo.users.UserGroup;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_7_4;
import org.nrg.xnat.versions.Xnat_1_8_3;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.nrg.xnat.pogo.users.UserGroups.*;
import static org.testng.AssertJUnit.*;

public class TestProjectData extends BaseSeleniumTest {

    private final int USER_LENGTH = 12;
    private String projectTitle;
    private String projectRunningTitle;
    private final User USER_A = new User(RandomHelper.randomLetters(USER_LENGTH));
    private final User USER_B = new User(RandomHelper.randomLetters(USER_LENGTH));
    private final User USER_C = new User(RandomHelper.randomLetters(USER_LENGTH));

    @BeforeClass
    public void setUpAccounts() {
        constructXnatDriver();
        USER_A.email(Settings.permuteSeleniumEmail()).password(USER_A.getUsername()).lastName(USER_A.getUsername()).firstName(USER_A.getUsername()).verified(true).enabled(true);
        mainAdminInterface().createUser(USER_A);
        USER_B.email(Settings.permuteSeleniumEmail()).password(USER_B.getUsername()).lastName(USER_B.getUsername()).firstName(USER_B.getUsername()).verified(true).enabled(true);
        mainAdminInterface().createUser(USER_B);
        USER_C.email(Settings.permuteSeleniumEmail()).password(USER_C.getUsername()).lastName(USER_C.getUsername()).firstName(USER_C.getUsername()).verified(true).enabled(true);
        mainAdminInterface().createUser(USER_C);
    }

    @BeforeMethod
    public void resetTitles() {
        final int PROJECT_LENGTH = 11;
        projectTitle = RandomHelper.randomID(PROJECT_LENGTH).toLowerCase();
        projectRunningTitle = RandomHelper.randomID(PROJECT_LENGTH).toLowerCase();
        testSpecificProject.id(testSpecificProject.getId().toLowerCase());
    }

    @Test
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-268", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-134")
    })
    public void testUserListInvitationWithEmails() {
        final int expectedNumEmails = 3;
        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setUserListRestriction(false).
                logLogout().
                seleniumLogin().
                createProject(testSpecificProject).
                loadAccessTab().
                addUserToProject(USER_A, OWNER, true).
                addUserToProject(USER_B, MEMBER, true).
                addUserToProject(USER_C, COLLABORATOR, true).
                getProjectPage().
                logLogout();
        assertRoles(testSpecificProject);
        new EmailQuery()
                .fromCatalog(XnatEmailCatalog.projectAccessEmail(testSpecificProject, true))
                .queryUntilMatchingExactNumberOfResults(expectedNumEmails);

        captureScreenshotlessStep();
    }

    @Test
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-269", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-231")
    })
    public void testUserListInvitationWithoutEmails() {
        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setUserListRestriction(false).
                logLogout().
                seleniumLogin().
                createProject(testSpecificProject).
                loadAccessTab().
                addUserToProject(USER_A, OWNER, false).
                addUserToProject(USER_B, MEMBER, false).
                addUserToProject(USER_C, COLLABORATOR, false).
                getProjectPage().
                logLogout();
        assertRoles(testSpecificProject);
        TimeUtils.sleep(10000); // wait a few seconds before making sure no emails are received

        assertTrue(
                new EmailQuery()
                        .fromCatalog(XnatEmailCatalog.projectAccessEmail(testSpecificProject, true))
                        .issueQueryOnce()
                        .isEmpty()
        );

        captureScreenshotlessStep();
    }

    @Test
    @Basic
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-480")
    public void testInvitationByEmail() {
        final int expectedNumEmails = 3;
        loginPage.seleniumLogin().
                createProject(testSpecificProject).
                loadAccessTab().
                addUserByEmail(USER_A, OWNER, true).
                addUserByEmail(USER_B, MEMBER, true).
                addUserByEmail(USER_C, COLLABORATOR, true).
                getProjectPage().
                logLogout();

        final EmailQuery userAdditionEmailQuery = new EmailQuery()
                .fromCatalog(XnatEmailCatalog.projectAccessEmail(testSpecificProject, true));
        userAdditionEmailQuery.queryUntilMatchingExactNumberOfResults(expectedNumEmails);

        captureScreenshotlessStep();
        assertRoles(testSpecificProject);

        userAdditionEmailQuery.queryUntilMatchingExactNumberOfResults(expectedNumEmails);
        // Check to make sure still only 1 access granted email for each of the users

        captureScreenshotlessStep();
    }

    @Test
    @JiraKey(simpleKey = "QA-136")
    public void testSingleProjectAliases() {
        final String thirdAlias = RandomHelper.randomFullString(13);
        final String fourthAlias = RandomHelper.randomFullString(13);
        final String fifthAlias = RandomHelper.randomFullString(13);
        final List<String> aliases = Arrays.asList(RandomHelper.randomID(10), RandomHelper.randomID(10), projectTitle);

        final ProjectEditPage projectEditPage = loginPage.seleniumLogin().loadProjectCreationPage();
        projectEditPage.createProjectWithExpectedFailure(
                testSpecificProject.title(projectTitle).runningTitle(projectRunningTitle).aliases(aliases)
        ).assertTextEquals(String.format("Invalid Project Title: '%s' cannot be used as the Project Title and an alias.", projectTitle)).capture();

        projectEditPage.fillAlias(2, projectRunningTitle).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Running Title: '%s' cannot be used as the Running Title and an alias.", projectRunningTitle)).
                capture();

        projectEditPage.fillAlias(2, testSpecificProject.getId()).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Project Id: '%s' cannot be used as the Project Id and an alias.", testSpecificProject.getId())).
                capture();

        testSpecificProject.getAliases().remove(2);
        testSpecificProject.addAlias(thirdAlias);
        projectEditPage.fillAlias(2, thirdAlias).
                submitAndValidate(testSpecificProject).
                loadEditPage();

        testSpecificProject.addAlias(fourthAlias);
        projectEditPage.fillAlias(3, fourthAlias).
                uploadScreenshot().
                submitAndValidate(testSpecificProject).
                loadEditPage();

        testSpecificProject.addAlias(fifthAlias);
        projectEditPage.fillAlias(4, fifthAlias).
                uploadScreenshot().
                submitAndValidate(testSpecificProject).
                loadEditPage();

        testSpecificProject.getAliases().remove(3);
        projectEditPage.fillAlias(3, "").
                uploadScreenshot().
                submitAndValidate(testSpecificProject);

        projectEditPage.verifyNoSearchResultsFound(fourthAlias).
                searchForProject(testSpecificProject).
                assertPageRepresents(testSpecificProject);

        home().searchForProject(fifthAlias).
                assertPageRepresents(testSpecificProject).
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-137")
    public void testMultipleProjectAliases() {
        // JIRA ticket: XNAT-2934 (Bug)

        final String uniqueString = testSpecificProject + "0";
        final String alias = RandomHelper.randomID(11).toLowerCase();
        // LowerCase is until XNAT-3075 is fixed.

        final ProjectEditPage projectEditPage = loginPage.seleniumLogin().
                createProject(testSpecificProject.title(projectTitle).runningTitle(projectRunningTitle).addAlias(alias)).
                loadProjectCreationPage();
        projectEditPage.createProjectWithExpectedFailure(new Project(uniqueString).addAlias(projectTitle)).
                assertTextEquals(String.format("Invalid Alias: '%s' is already being used.", projectTitle)).
                capture();
        projectEditPage.fillAlias(0, projectRunningTitle).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Alias: '%s' is already being used.", projectRunningTitle)).
                capture();
        projectEditPage.fillAlias(0, testSpecificProject.getId()).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Alias: '%s' is already being used.", testSpecificProject.getId())).
                capture();
        projectEditPage.fillAlias(0, "").
                fillProjectTitle(alias).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Project Title: '%s' is already being used.", alias)).
                capture();
        projectEditPage.fillProjectTitle(uniqueString).
                fillRunningTitle(alias).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Running Title: '%s' is already being used.", alias)).
                capture();
        projectEditPage.fillRunningTitle(uniqueString).
                fillProjectID(alias).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Project Id: '%s' is already being used.", alias)).
                capture();
        projectEditPage.fillProjectID(uniqueString).
                fillAlias(0, alias).
                uploadScreenshot().
                submitWithExpectedFailure().
                assertTextEquals(String.format("Invalid Alias: '%s' is already being used.", alias)).
                capture();
        logLogout();
    }

    @Test
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-270", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-138")
    })
    public void testChangeProjectAccessGroup() {
        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setUserListRestriction(false).
                logLogout().
                login(USER_A).
                createProject(testSpecificProject.title(projectTitle).runningTitle(projectRunningTitle)).
                loadAccessTab().
                addUserToProject(USER_B, OWNER, false).
                addUserToProject(USER_C, MEMBER, false).
                changeProjectAccessRole(USER_B, MEMBER, true).
                changeProjectAccessRole(USER_B, OWNER, false).
                removeUserFromProject(USER_B).
                addUserToProject(USER_B, MEMBER, false).
                changeProjectAccessRole(USER_C, COLLABORATOR, false).
                removeUserFromProject(USER_C).
                addUserToProject(USER_C, OWNER, false).
                changeProjectAccessRole(USER_C, COLLABORATOR, false).
                getProjectPage().
                logLogout();
        assertRoles(testSpecificProject);

        new EmailQuery()
                .fromCatalog(XnatEmailCatalog.projectAccessEmail(testSpecificProject, true))
                .queryUntilSingleResult();
        captureScreenshotlessStep();
    }

    @Test
    @Basic
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-139")
    public void testAccessLevel() {
        final String description = RandomHelper.randomFullString(30);
        final String commentText = "test for request of access";

        final ProjectReportPage projectReportPage = loginPage.seleniumLogin().
                createProject(testSpecificProject.title(projectTitle).runningTitle(projectRunningTitle).description(description).accessibility(Accessibility.PRIVATE)).
                logLogout().
                login(USER_A).
                verifyNoSearchResultsFound(projectTitle).
                logLogout().
                seleniumLogin().
                searchForProject(projectTitle).
                loadAccessTab().
                setAccessibility(Accessibility.PROTECTED).
                capture().
                getProjectPage().
                logLogout().
                login(USER_A).
                searchForProject(projectTitle).
                assertProtectedProjectWarning(testSpecificProject).
                clickRequestAccessButton().
                assertDisplayedProject(testSpecificProject).
                capture().
                requestAccess(COLLABORATOR, commentText).
                logLogout().
                seleniumLogin().
                searchForProject(projectTitle).
                loadAccessTab().
                setAccessibility(Accessibility.PUBLIC).
                capture().
                getProjectPage().
                logLogout().
                login(USER_A).
                searchForProject(projectTitle);
        projectReportPage.loadProjectDataTable().assertCurrentTab(DataType.SUBJECT.getPluralName());
        projectReportPage.capture();

        new EmailQuery()
                .containing(accessRequestSearchText())
                .queryUntilSingleResult()
                .validateEach(message -> {
                    message.assertEmailContains("Login: " + USER_A.getUsername());
                    message.assertEmailContains("Comments: " + commentText);
                });

        captureScreenshotlessStep();
        logLogout();
    }

    private List<String> accessRequestSearchText() {
        if (XnatTestingVersionManager.testedVersionFollows(Xnat_1_8_3.class)) {
            return Arrays.asList(
                    String.format("We received a request to access the %s project from a user on XNAT as a", projectTitle),
                    "collaborator. Granting this kind of access"
            ); // gmail API breaks on parentheses in search
        }
        if (XnatTestingVersionManager.testedVersionFollows(Xnat_1_7_4.class)) {
            return Collections.singletonList(String.format("request to access the %s project from a user on XNAT as a collaborator", projectTitle));
        }
        return Collections.singletonList(String.format("The following user has requested collaborator access to the %s Project", projectTitle));
    }

    private void assertRoles(Project project) {
        final Map<User, UserGroup> groupMap = new HashMap<>();
        groupMap.put(USER_A, OWNER);
        groupMap.put(USER_B, MEMBER);
        groupMap.put(USER_C, COLLABORATOR);
        for (Map.Entry<User, UserGroup> userEntry : groupMap.entrySet()) {
            navigateToLoginPage().
                    login(userEntry.getKey()).
                    searchForProject(project).
                    assertPageRepresentsGroup(userEntry.getValue()).
                    logLogout();
        }
    }

}
