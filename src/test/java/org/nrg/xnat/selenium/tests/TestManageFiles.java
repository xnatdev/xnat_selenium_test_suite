package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.DataUnit;
import org.nrg.selenium.exceptions.DuplicateUploadedFilesException;
import org.nrg.selenium.util.ResourceValidation;
import org.nrg.selenium.xnat.manage_files.ManageFilesFileInformation;
import org.nrg.selenium.xnat.page_model.report.XnatReportPage;
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage;
import org.nrg.selenium.xnat.page_model.ui_element.manage_files.ManageFiles;
import org.nrg.selenium.xnat.page_model.ui_element.manage_files.ManageFilesFile;
import org.nrg.selenium.xnat.page_model.ui_element.manage_files.ManageFilesResource;
import org.nrg.selenium.xnat.page_model.ui_element.manage_files.SimpleTextRender;
import org.nrg.testing.FileIOUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.util.RandomHelper;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.fail;

public class TestManageFiles extends BaseSeleniumTest {

    private final Project project = new Project();
    private final Subject subject = new Subject(project, "Sample_Patient");
    private final ImagingSession session = new MRSession(project, subject, "Sample_ID");

    private final File textFileA = getDataFile("seleniumDataA.txt");
    private final int textFileABytes = 36;
    private final File textFileR = getDataFile("seleniumDataR.txt");
    private final int textFileRBytes = 16;
    private final File zipFile1 = getDataFile("seleniumDataZ1.zip");
    private final int zipFile1Bytes = 391;
    private final String zipFile1InnerText1Name = "seleniumDataB.txt";
    private final String zipFile1InnerText1Content = "lion tiger jaguar bobcat";
    private final int zipFile1InnerText1Bytes = 27;
    private final String zipFile1InnerText2Name = "seleniumDataC.txt";
    private final String zipFile1InnerText2Content = "blue orange purple yellow black white green red";
    private final int zipFile1InnerText2Bytes = 50;
    private final File duplicateNameFile1 = getDataFile("repeatData" + File.separator + "repeatData1" + File.separator + "seleniumDataD.txt");
    private final int duplicateNameFile1Bytes = 26;
    private final File duplicateNameFile2 = getDataFile("repeatData" + File.separator + "repeatData2" + File.separator + "seleniumDataD.txt");
    private final int duplicateNameFile2Bytes = 10;
    private final File compressedFolderTreeFile = getDataFile("seleniumTree.zip");
    private static final String PROP_ENABLE = "allowHtmlResourceRendering";

    @BeforeClass(groups = TestGroups.CLIENT_DEPLOYMENT)
    public void setUpProject() {
        constructXnatDriver();
        mainAdminInterface().postSiteConfigProperty(PROP_ENABLE, true);
        navigateToLoginPage().
                seleniumLogin().
                createProject(project).
                loadCompressedUploader().
                uploadToArchive(project, getDataFile("seleniumSampleA.zip")).
                quit();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        mainAdminInterface().postSiteConfigProperty(PROP_ENABLE, false);
    }

    @Test(groups = TestGroups.CLIENT_DEPLOYMENT)
    @JiraKey(simpleKey = "QA-121")
    public void testFileUpload() throws DuplicateUploadedFilesException {
        // JIRA ticket: part of XNAT-2715
        final ManageFilesResource resource = simpleResource("file_upload_session");

        initializeStandardManageFilesTest().
                createFolder(resource).
                uploadFile(textFileA).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(textFileABytes)).
                getFile(textFileA.getName()).
                assertFileSize(textFileABytes).
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        resource.getFileManager().
                getUnderlyingPage().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-122")
    public void testZippedUploadExtract() throws DuplicateUploadedFilesException {
        // tests uploading a zipped directory with extracting
        final ManageFilesResource resource = simpleResource("extraction_allowed");

        final ManageFiles fileManager = initializeStandardManageFilesTest().
                createFolder(resource).
                uploadFile(zipFile1, new ManageFilesFileInformation().extract(true)).
                getFileManager();
        resource.assertFolderSizes(new ResourceValidation(zipFile1InnerText1Bytes + zipFile1InnerText2Bytes, DataUnit.BYTES, 2));
        final ManageFilesFile innerFile1Object = resource.getFile(zipFile1InnerText1Name).assertFileSize(zipFile1InnerText1Bytes);
        final ManageFilesFile innerFile2Object = resource.getFile(zipFile1InnerText2Name).assertFileSize(zipFile1InnerText2Bytes);
        fileManager.capture();
        innerFile1Object.clickTextFile().
                assertTextEquals(zipFile1InnerText1Content).
                capture().
                navigate().
                back();
        fileManager.getUnderlyingPage().loadManageFiles();
        innerFile2Object.clickTextFile().
                assertTextEquals(zipFile1InnerText2Content).
                capture().
                navigate().
                back();
        fileManager.getUnderlyingPage().loadManageFiles();
        resource.assertChildFileNotPresent(zipFile1.getName());
        fileManager.capture();
        home().logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-123")
    public void testZippedUploadCancelExtract() throws DuplicateUploadedFilesException {
        // tests uploading a zipped directory without extracting
        final ManageFilesResource resource = simpleResource("extraction_cancelled");

        initializeStandardManageFilesTest().
                createFolder(resource).
                uploadFile(zipFile1).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(zipFile1Bytes)).
                getFile(zipFile1.getName()).
                assertFileSize(zipFile1Bytes).
                capture().
                getBaseResource().
                assertChildFileNotPresent(zipFile1InnerText1Name).
                assertChildFileNotPresent(zipFile1InnerText2Name).
                capture();

        // Verify the contents of the zip are correct using REST calls
        navigate().to(formatRestUrl("/archive/projects/", project.getId(), "/subjects/", subject.getLabel(), "/experiments/", session.getLabel(), "/resources/", resource.getFolderName(), "/files/seleniumDataZ1.zip/seleniumDataB.txt"));
        loadPage(SimpleTextRender.class).assertTextEquals("lion tiger jaguar bobcat");
        navigate().to(formatRestUrl("/archive/projects/", project.getId(), "/subjects/", subject.getLabel(), "/experiments/", session.getLabel(), "/resources/", resource.getFolderName(), "/files/seleniumDataZ1.zip/seleniumDataC.txt"));
        loadPage(SimpleTextRender.class).assertTextEquals("blue orange purple yellow black white green red");

        home().logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-124")
    public void testOverwriteRepeatDataUncheckedBox() throws DuplicateUploadedFilesException {
        // tests uploading data with the same name without overwrite

        final ResourceValidation validationSpec = singleFileInBytes(duplicateNameFile1Bytes);
        final ManageFilesResource resource = simpleResource("overwrite_failure");
        initializeStandardManageFilesTest().
                createFolder(resource).
                uploadFile(duplicateNameFile1).
                assertFileSize(duplicateNameFile1Bytes).
                getBaseResource().
                assertFolderSizes(validationSpec).
                capture();

        try {
            resource.uploadFile(duplicateNameFile2);
            fail("Upload should have been blocked as duplicate");
        } catch (DuplicateUploadedFilesException duplicateUploadedFilesException) {
            assertEquals(Collections.singletonList(duplicateNameFile1.getName()), duplicateUploadedFilesException.getReportedFileNames());
        }

        resource.clickExpander().
                assertFolderSizes(validationSpec).
                assertNumberOfChildren(1).
                getFile(duplicateNameFile1.getName()).
                assertFileSize(duplicateNameFile1Bytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(duplicateNameFile1)).
                capture().
                navigate().
                back();

        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-125")
    public void testOverwriteDistinctDataCheckedBox() throws DuplicateUploadedFilesException {
        // tests uploading data with different names with overwrite
        final ManageFilesResource resource = simpleResource("no_overwrite");

        initializeStandardManageFilesTest().
                createFolder(resource).
                uploadFile(duplicateNameFile1).
                assertFileSize(duplicateNameFile1Bytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(duplicateNameFile1Bytes)).
                capture().
                uploadFile(textFileA, new ManageFilesFileInformation().overwrite(true)).
                getBaseResource().
                assertFolderSizes(new ResourceValidation(duplicateNameFile1Bytes + textFileABytes, DataUnit.BYTES, 2));
        final ManageFilesFile duplicateNameFile = resource.getFile(duplicateNameFile1.getName()).assertFileSize(duplicateNameFile1Bytes);
        final ManageFilesFile fileA = resource.getFile(textFileA.getName()).assertFileSize(textFileABytes);
        duplicateNameFile.capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(duplicateNameFile1)).
                capture().
                navigate().
                back();
        resource.getFileManager().
                getUnderlyingPage().
                loadManageFiles();
        fileA.clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-126")
    public void testOverwriteRepeatDataCheckedBox() throws DuplicateUploadedFilesException {
        // tests uploading data with the same name with overwrite

        initializeStandardManageFilesTest().
                createFolder(simpleResource("overwrite_success")).
                uploadFile(duplicateNameFile1).
                assertFileSize(duplicateNameFile1Bytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(duplicateNameFile1Bytes)).
                capture().
                uploadFile(duplicateNameFile2, new ManageFilesFileInformation().overwrite(true)).
                assertFileSize(duplicateNameFile2Bytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(duplicateNameFile2Bytes)).
                capture().
                assertNumberOfChildren(1).
                getFile(duplicateNameFile2.getName()).
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(duplicateNameFile2)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-127")
    public void testFolderStructure() throws DuplicateUploadedFilesException {
        // tests for correct structure of unzipped directories
        final String branch1 = "seleniumBranch1";
        final String branch2 = "seleniumBranch2";
        final String branch3 = "seleniumBranch3";
        final String twig1 = "seleniumTwig1";
        final String twig2 = "seleniumTwig2";
        final String twig3 = "seleniumTwig3";
        final String leaf1Name = "seleniumLeaf1.txt";
        final String leaf2Name = "seleniumLeaf2.txt";
        final String leaf3Name = "seleniumLeaf3.txt";
        final String leaf4Name = "seleniumLeaf4.txt";
        final String leaf5Name = "seleniumLeaf5.txt";
        final int leaf1Bytes = 322;
        final int leaf2Bytes = 28;
        final int leaf3Bytes = 32;
        final int leaf4Bytes = 15;
        final int leaf5Bytes = 4;
        final String leaf1Content = "Arma virumque cano, Troiae qui primus ab oris\n" +
                "Italiam, fato profugus, Laviniaque venit\n" +
                "litora, multum ille et terris iactatus et alto\n" +
                "vi superum saevae memorem Iunonis ob iram;\n" +
                "multa quoque et bello passus, dum conderet urbem,\n" +
                "inferretque deos Latio, genus unde Latinum,\n" +
                "Albanique patres, atque altae moenia Romae.";
        final String leaf2Content = "1 + 2 + 3 + 4 + ... = -1/12?";
        final String leaf3Content = "1 + 2 + 3 + ... + n = n(n + 1)/2";
        final String leaf4Content = "a^2 + b^2 = c^2";
        final String leaf5Content = "TEXT";

        final ManageFilesResource resource = simpleResource("folder_structure");

        final XnatReportPage<?, ?> reportPage = initializeStandardManageFilesTest().
                createFolder(resource).
                uploadFile(compressedFolderTreeFile, new ManageFilesFileInformation().extract(true)).
                getBaseResource().
                assertFolderSizes(new ResourceValidation(leaf1Bytes + leaf2Bytes + leaf3Bytes + leaf4Bytes + leaf5Bytes, DataUnit.BYTES, 5)).
                capture().
                getFileManager().
                getUnderlyingPage();
        for (String leaf : Arrays.asList(leaf1Name, leaf2Name, leaf3Name, leaf4Name, leaf5Name)) {
            resource.assertChildFileNotPresent(leaf);
        }

        resource.getSubfolder(branch1).
                getSubfolder(twig1).
                getFile(leaf2Name).
                assertFileSize(leaf2Bytes).
                capture().
                clickTextFile().
                assertTextEquals(leaf2Content).
                capture().
                navigate().
                back();

        reportPage.loadManageFiles();
        resource.getSubfolder(branch1).
                getSubfolder(twig2).
                getFile(leaf3Name).
                assertFileSize(leaf3Bytes).
                capture().
                clickTextFile().
                assertTextEquals(leaf3Content).
                capture().
                navigate().
                back();

        reportPage.loadManageFiles();
        resource.getSubfolder(branch2).
                getFile(leaf1Name).
                assertFileSize(leaf1Bytes).
                capture().
                clickTextFile().
                assertTextEquals(leaf1Content).
                capture().
                navigate().
                back();

        reportPage.loadManageFiles();
        resource.getSubfolder(branch3).
                getSubfolder(twig3).
                getFile(leaf4Name).
                assertFileSize(leaf4Bytes).
                capture().
                clickTextFile().
                assertTextEquals(leaf4Content).
                capture().
                navigate().
                back();

        reportPage.loadManageFiles();
        resource.getSubfolder(branch3).
                getSubfolder(twig3).
                getFile(leaf5Name).
                assertFileSize(leaf5Bytes).
                capture().
                clickTextFile().
                assertTextEquals(leaf5Content).
                capture().
                navigate().
                back();

        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-128")
    public void testRenameUpload() throws DuplicateUploadedFilesException {
        // tests renaming of a file through upload
        final String newName = RandomHelper.randomString(12) + ".txt";

        initializeStandardManageFilesTest().
                createFolder(simpleResource("renamed_file_upload")).
                uploadFile(textFileA, new ManageFilesFileInformation().rename(newName)).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(textFileABytes)).
                getFile(newName).
                assertFileSize(textFileABytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-129")
    public void testRenameUploadAvoidDuplicates() throws DuplicateUploadedFilesException {
        // tests using rename feature to avoid file name collisions
        final String rename = RandomHelper.randomString(12) + ".txt";

        initializeStandardManageFilesTest().
                createFolder(simpleResource("no_duplicates_rename")).
                uploadFile(duplicateNameFile1).
                assertFileSize(duplicateNameFile1Bytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(duplicateNameFile1Bytes)).
                capture().
                uploadFile(duplicateNameFile2, new ManageFilesFileInformation().rename(rename).overwrite(true)).
                getBaseResource().
                assertFolderSizes(new ResourceValidation(duplicateNameFile1Bytes + duplicateNameFile2Bytes, DataUnit.BYTES, 2)).
                assertNumberOfChildren(2).
                getFile(duplicateNameFile1.getName()).
                assertFileSize(duplicateNameFile1Bytes).
                getBaseResource().
                getFile(rename).
                assertFileSize(duplicateNameFile2Bytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(duplicateNameFile2)).
                capture().
                navigate().
                back();

        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-130")
    public void testRenameUploadCauseDuplicatesOverwriteUnchecked() throws DuplicateUploadedFilesException {
        // tests using rename feature to cause file name collisions without overwrite

        final ManageFilesResource resource = initializeStandardManageFilesTest().
                createFolder(simpleResource("overwrite_rename_failure")).
                uploadFile(textFileA).
                assertFileSize(textFileABytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(textFileABytes)).
                capture();

        try {
            resource.uploadFile(textFileR, new ManageFilesFileInformation().rename(textFileA.getName()));
            fail("Upload should have been blocked as duplicate");
        } catch (DuplicateUploadedFilesException dufe) {
            assertEquals(Collections.singletonList(textFileA.getName()), dufe.getReportedFileNames());
        }

        resource.assertFolderSizes(singleFileInBytes(textFileABytes)).
                assertNumberOfChildren(1).
                getFile(textFileA.getName()).
                assertFileSize(textFileABytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-131")
    public void testRenameUploadCauseDuplicatesOverwriteChecked() throws DuplicateUploadedFilesException {
        // tests using rename feature to cause file name collisions with overwrite

        initializeStandardManageFilesTest().
                createFolder(simpleResource("overwrite_rename_success")).
                uploadFile(textFileA).
                assertFileSize(textFileABytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(textFileABytes)).
                capture().
                uploadFile(textFileR, new ManageFilesFileInformation().rename(textFileA.getName()).overwrite(true)).
                assertFileSize(textFileRBytes).
                getBaseResource().
                assertNumberOfChildren(1).
                assertFolderSizes(singleFileInBytes(textFileRBytes)).
                capture().
                getFile(textFileA.getName()).
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileR)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-307", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-306")
    })
    public void testZipExtensions() throws DuplicateUploadedFilesException {
        final String newExt = "nzte";
        final String badExt = "nztenv";
        final String validZipName = "SeleniumDataZG." + newExt;
        final String invalidZipName = "SeleniumDataZB." + badExt;
        final int zipBytes = 326;
        final String innerFile1Name = "pi.txt";
        final String innerFile1Contents = "3.14159";
        final String innerFile2Name = "pokemon.txt";
        final String innerFile2Contents = "Mew,Dragonair,Xatu,Latias,Latios,Kyogre";
        final String zipExtensions = "zip,jar,rar,ear,gar,mrb," + newExt;
        final String resourceName = "zipExtensions";

        final ImagingSessionReportPage sessionReportPage = loginPage.seleniumAdminLogin().
                loadAdminUi().
                setZipExtensions(zipExtensions).
                logLogout().
                seleniumLogin().
                searchForSession(mainInterface().getAccessionNumber(session));
        final String accessionNumber = sessionReportPage.readAccessionNumber();
        sessionReportPage.loadManageFiles().
                capture().
                createFolder(simpleResource(resourceName)).
                uploadFile(getDataFile(validZipName)).
                assertFileSize(zipBytes).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(zipBytes)).
                capture().
                uploadFile(getDataFile(invalidZipName)).
                assertFileSize(zipBytes).
                getBaseResource().
                assertFolderSizes(new ResourceValidation(2 * zipBytes, DataUnit.BYTES, 2)).
                getFile(validZipName).
                assertFileSize(zipBytes).
                capture();

        for (String innerFileName : Arrays.asList(innerFile1Name, innerFile2Name)) {
            navigate().to(formatZipResourceUrl(accessionNumber, resourceName, invalidZipName, innerFileName));
            readTomcatErrorPage().
                    assertDisplayedError("HTTP Status 404 – Not Found").
                    capture();
        }

        navigate().to(formatZipResourceUrl(accessionNumber, resourceName, validZipName, innerFile1Name));
        loadPage(SimpleTextRender.class).assertTextEquals(innerFile1Contents);
        navigate().to(formatZipResourceUrl(accessionNumber, resourceName, validZipName, innerFile2Name));
        loadPage(SimpleTextRender.class).assertTextEquals(innerFile2Contents);

        home().logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-132")
    public void testFileUploadSubject() throws DuplicateUploadedFilesException {
        // tests uploading a file to Subject resources

        loginPage.seleniumLogin().
                searchForProject(project).
                clickSubjectLink(subject.getLabel()).
                capture().
                loadManageFiles().
                capture().
                createFolder(simpleResource("file_upload_subject")).
                uploadFile(textFileA).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(textFileABytes)).
                getFile(textFileA.getName()).
                assertFileSize(textFileABytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-133")
    public void testFileUploadProject() throws DuplicateUploadedFilesException {
        // tests uploading a file to Project resources

        loginPage.seleniumLogin().
                searchForProject(project).
                capture().
                loadManageFiles().
                capture().
                createFolder(simpleResource("file_upload_project")).
                uploadFile(textFileA).
                getBaseResource().
                assertFolderSizes(singleFileInBytes(textFileABytes)).
                getFile(textFileA.getName()).
                assertFileSize(textFileABytes).
                capture().
                clickTextFile().
                assertTextEquals(FileIOUtils.readFile(textFileA)).
                capture().
                navigate().
                back();
        logLogout();
    }

    private ManageFiles initializeStandardManageFilesTest() {
        return loginPage.seleniumLogin().
                searchForProject(project).
                clickSubjectLink(subject.getLabel()).
                clickMrSessionLink().
                capture().
                loadManageFiles().
                capture();
    }

    private ManageFilesResource simpleResource(String resourceName) {
        final ManageFilesResource resource =  loadPage(ManageFilesResource.class);
        resource.setFolderName(resourceName);
        return resource;
    }

    private ResourceValidation singleFileInBytes(int numBytes) {
        return new ResourceValidation(numBytes, DataUnit.BYTES, 1);
    }

    private String formatZipResourceUrl(String accessionNumber, String resourceFolder, String zipName, String fileName) {
        return formatRestUrl("experiments", accessionNumber, "resources", resourceFolder, "files", zipName, fileName);
    }

}