package org.nrg.xnat.selenium.tests;

import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.jsoup.nodes.Document;
import org.nrg.selenium.*;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.ReportIssuePage;
import org.nrg.selenium.xnat.page_model.WorkflowReportPage;
import org.nrg.selenium.xnat.page_model.admin.JavaMelodyMonitoringServlet;
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage;
import org.nrg.selenium.xnat.page_model.site_search.AdvancedSearchResult;
import org.nrg.testing.TestNgUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.email.EmailQuery;
import org.nrg.testing.email.MessageTemplate;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_7_6;
import org.nrg.xnat.versions.Xnat_1_9;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.AssertJUnit.*;

public class TestAdminFeatures extends BaseSeleniumTest {

    private static final Logger LOGGER = Logger.getLogger(TestAdminFeatures.class); // TODO: replace with @Log4j

    @Test
    @JiraKey(simpleKey = "QA-105")
    public void testUserListRestriction() {
        // JIRA ticket: XNAT-2705

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setUserListRestriction(true).
                logLogout().
                seleniumLogin().
                createProject(testSpecificProject.description("Automated testing").addKeyword("automation").accessibility(Accessibility.PRIVATE)).
                loadAccessTab().
                assertUserListUnavailable().
                capture();
        navigate().to(formatRestUrl("/users"));
        readRestletErrorPage().
                assertDisplayedError("Access Denied: Only site managers can access site-level user resources.").
                capture();

        if (is16()) {
            skipStep();
        } else {
            for (String xapiFragment : Arrays.asList("users", "users/profiles", "users/profile/admin", "users/current")) {
                mainQueryBase().get(formatXapiUrl(xapiFragment)).then().assertThat().statusCode(403);
            }
            captureScreenshotlessStep();
        }

        home().
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                setUserListRestriction(false).
                logLogout().
                seleniumLogin().
                searchForProject(testSpecificProject).
                loadAccessTab().
                assertUserListIsAvailable().
                capture();
        navigate().to(formatRestUrl("/users?format=html"));
        readDefaultTablePage().
                captureLeadingRows(1);

        if (is16()) {
            skipStep();
        } else {
            mainQueryBase().get(formatXapiUrl("users")).then().assertThat().statusCode(200).and().body("", Matchers.hasItem(mainUser.getUsername()));
            captureScreenshotlessStep();
        }

        home().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-106")
    public void testProjectRestriction() {
        // JIRA ticket: QA-77, XNAT-3537. Ensures that project creation can be limited to admins.

        final List<String> subHeaders = loginPage.seleniumAdminLogin().
                loadAdminUi().
                setNonadminProjectsAllowed(false).
                logLogout().
                seleniumLogin().
                readTopNav().
                readSubnavHeaders("New");
        assertTrue(subHeaders.contains("Subject"));
        if (subHeaders.contains("Project")) {
            testController.failBecause("Failed because top nav New > Project option available for nonadmins", "XNAT-4261");
        }
        captureScreenshotlessStep();
        navigate().to(Settings.BASEURL + "/app/template/XDATScreen_add_xnat_projectData.vm"); // Load the project creation page

        if (XnatTestingVersionManager.testedVersionFollows(Xnat_1_7_6.class)) {
            readTomcatErrorPage().assertDisplayedError("HTTP Status 403 – Forbidden").capture();
            navigate().back();
            logLogout();
        } else {
            final ProjectEditPage projectEditPage = loginPage.loadPage(ProjectEditPage.class, false, true). // get a page object for it
                    fillProjectTitle(testSpecificProject.getTitle()).
                    fillRunningTitle(testSpecificProject.getRunningTitle()).
                    fillProjectID(testSpecificProject.getId());
            commentStep("IDs: " + testSpecificProject);
            projectEditPage.capture();
            projectEditPage.submitForm();
            projectEditPage.readAlert().
                    assertTextEquals("Invalid permissions for this operation").
                    capture();
            projectEditPage.logLogout();
        }

        mainQueryBase().put(formatRestUrl("/projects/", testSpecificProject.getId())).then().assertThat().statusCode(403);
        captureScreenshotlessStep();

        loginPage.seleniumAdminLogin().
                verifyNoSearchResultsFound(testSpecificProject.getId()).
                createProject(testSpecificProject).
                loadAdminUi().
                setNonadminProjectsAllowed(true).
                logLogout().
                seleniumLogin().createProject(new Project()).
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-107")
    public void testMonitoringServlet() {
        // JIRA tickets: XNAT-2578, XNAT-2570

        final String servletUrl = formatXnatUrl("/monitoring");
        loginPage.seleniumAdminLogin();
        navigate().to(servletUrl);
        loadPage(JavaMelodyMonitoringServlet.class).
                captureGraphs().
                verifyBasicCaching().
                home().
                logLogout().
                seleniumLogin();
        navigate().to(servletUrl);
        readTomcatErrorPage().
                assertDisplayedError("HTTP Status 403 – Forbidden").
                capture().
                home().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-265", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-108")
    })
    public void testWorkflowEntries() {
        // Makes sure that workflows are not easily searchable from the Data sidebar and that they are still being created and searchable by admins
        final String fullName = String.format("%s %s", mainUser.getFirstName(), mainUser.getLastName());
        // TODO: Move pipelines checks in a separate test suite, which will be depend on wherther we have or not pipelines engine installed
        //final String pipelineName = "Added Project";
        final String nameColumn = "Userfullname";
        //final String pipelineNameColumn = XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_9.class) ? "pipeline_name" : "Workflow Name";

        final List<String> dataOptions = loginPage.seleniumLogin().
                createProject(testSpecificProject).
                createSubject(testSpecificSubject.project(testSpecificProject)).
                logLogout().
                seleniumLogin().
                readTopNav().
                readSubnavSubtable("Browse", "Data");
        assertTrue(dataOptions.contains(DataType.SUBJECT.getPluralName()));
        assertFalse(dataOptions.contains("Workflows"));
        captureScreenshotlessStep();

        final AdvancedSearchResult workflowTable = home().
                logLogout().
                seleniumAdminLogin().
                loadLegacyAdminPage().
                viewAllWorkflows();
        workflowTable.capture().
                launchFilteringOnColumn(nameColumn).
                capture().
                addEqualsFilter(fullName).
                capture().
                submitFilters();

        for (String entry : workflowTable.getDataTable().readEntriesForColumn(nameColumn)) {
            assertEquals(fullName, entry);
        }

        // TODO: Move pipelines checks in a separate test suite, which will be depend on wherther we have or not pipelines engine installed
        //workflowTable.capture().
        //        launchFilteringOnColumn(pipelineNameColumn).
        //        capture().
        //        addEqualsFilter(pipelineName).
        //        capture().
        //        submitFilters();

        for (String entry : workflowTable.getDataTable().readEntriesForColumn(nameColumn)) {
            assertEquals(fullName, entry);
        }

        // TODO: Move pipelines checks in a separate test suite, which will be depend on wherther we have or not pipelines engine installed
        //for (String entry : workflowTable.getDataTable().readEntriesForColumn(pipelineNameColumn)) {
        //    assertEquals(pipelineName, entry);
        //}

        workflowTable.capture().
                sortDownOnColumn("Workflow ID").
                getDataTable().
                captureLeadingRows(1).
                clickDataCellLink(0, 0); // Click the first entry in the table: i.e. the workflow for the newly created project

        loadPage(WorkflowReportPage.class).
                // TODO: Move pipelines checks in a separate test suite, which will be depend on wherther we have or not pipelines engine installed
                //assertId(testSpecificProject.getId()).
                assertExternalId(testSpecificProject.getId()).
                capture().
                logLogout();
    }

    @Test
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-109")
    public void testHelp() {
        // Ensures that users can send issue reports to the admin

        final MessageTemplate email = new MessageTemplate("helpEmailTemplate.txt");
        final String currentTime = email.usedTime();
        final String helpTitle = "Automated Test of Issue Reports";
        final String helpDescription = email.read();

        final IndexPage indexPage = loginPage.seleniumLogin().
                loadPage(ReportIssuePage.class).
                capture().
                fillSummary(helpTitle).
                fillDescription(helpDescription).
                capture().
                attachFile(getDataFile("seleniumDataA.txt")).
                capture().
                submitReport();
        captureScreenshotlessStep();
        indexPage.logLogout();

        new EmailQuery()
                .containing(currentTime)
                .queryUntilSingleResult()
                .validateEach(message -> {
                    final Document document = message.getDocument();
                    document.outputSettings(new Document.OutputSettings().prettyPrint(false));

                    assertEquals(helpTitle, document.select("div[class=value]").first().text());
                    assertEquals(helpDescription.replaceAll("\n\n", " "), document.select("div[class=value]").last().text());
                    assertEquals(Settings.MAIN_USERNAME, document.select("td:contains(Login)").first().lastElementSibling().text());
                    assertEquals("3.14 2.72 8.32 9.21 2.22 9.21 1.90", message.getAttachments().get(0).getStringContents().trim());
                });

        captureScreenshotlessStep();
    }

    @AfterMethod(alwaysRun = true)
    public void fixCriticalSettings(ITestResult result) {
        if (TestNgUtils.checkTestFailingAndEquals(result, "testProjectRestriction")) {
            try {
                LOGGER.info("testProjectRestriction has failed. Attempting to do a REST call to set the non-admin project creation status to true to avoid affecting subsequent tests.");
                restDriver.interfaceFor(mainAdminUser).setNonadminProjectSetting(true);
            } catch (Error | Exception e) {
                LOGGER.warn("Could not correct non-admin project creation setting due to: ", e);
            }
        } else if (TestNgUtils.checkTestFailingAndEquals(result, "testUserListRestriction")) {
            try {
                LOGGER.info("testUserListRestriction has failed. Attempting to do a REST call to set the site user-list restriction to false to avoid affecting subsequent tests.");
                restDriver.interfaceFor(mainAdminUser).setSiteUserListRestriction(false);
            } catch (Error | Exception e) {
                LOGGER.warn("Could not correct non-admin user list restriction setting due to: ", e);
            }
        }
    }

}
