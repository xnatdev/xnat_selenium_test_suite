package org.nrg.xnat.selenium.tests;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.nrg.selenium.*;
import org.nrg.selenium.enums.ScriptingLanguageType;
import org.nrg.selenium.exceptions.*;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.WorkflowReportPage;
import org.nrg.selenium.xnat.page_model.automation.EventHandlerListComponent;
import org.nrg.selenium.xnat.page_model.automation.ScriptsTab;
import org.nrg.selenium.xnat.scripting.AutomationScript;
import org.nrg.selenium.xnat.scripting.EventHandler;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.resources.ProjectResource;
import org.nrg.xnat.pogo.resources.ResourceFile;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.nrg.selenium.enums.ScriptingLanguageType.*;
import static org.testng.AssertJUnit.*;

public class TestScripting extends BaseSeleniumTest {

    private final Project scriptProject = new Project("Script" + RandomHelper.randomID(6));
    private final String PROJECT_EVENT = "Added Subject";
    private final String SITE_EVENT = "Added MR Session";
    private final String SCRIPT_LOCATION = Paths.get(Settings.DATA_LOCATION, "automation_scripts").toString();

    @BeforeClass
    public void setUpScripting() {
        final Subject subject = new Subject(scriptProject);

        constructXnatDriver();

        navigateToLoginPage().seleniumLogin().
                createProject(scriptProject).
                createSubject(subject).
                createSession(new MRSession(scriptProject, subject)).
                logout();
    }

    @Test
    @JiraKey(simpleKey = "QA-392", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-141")
    })
    public void testAddScript() throws DuplicateScriptException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException {
        final ScriptsTab scriptsTab = loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab();

        for (List<? extends Serializable> tuple : Lists.cartesianProduct(
                Arrays.asList(true, false),
                Arrays.asList(JAVASCRIPT, GROOVY, PYTHON)
        )) {
            final AutomationScript testScript = new AutomationScript(RandomHelper.randomID(20)).
                    prespecify((Boolean) tuple.get(0)).
                    language((ScriptingLanguageType) tuple.get(1)).
                    description(RandomHelper.randomID(20)).
                    contents(RandomHelper.randomStringLines(18, 3));
            scriptsTab.addScript(testScript).validateScript(testScript);
        }
        scriptsTab.getAutomationPage().logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-393", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-142")
    })
    public void testScriptModifications() throws ScriptingLanguageNotSpecifiedException, MissingScriptIdException, MissingScriptContentException, DuplicateScriptException {
        final AutomationScript originalScript = scriptForModTest().language(GROOVY);
        final AutomationScript unappendedScript = scriptForModTest().appendSetting(false);
        final AutomationScript appendedScript = scriptForModTest().appendSetting(true);

        loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab().
                addScript(originalScript).
                editScript(originalScript, RandomHelper.randomID(12), RandomHelper.randomID(14), RandomHelper.randomStringLines(25, 5)).
                validateScript(originalScript).
                duplicateScript(originalScript, unappendedScript).
                validateScript(unappendedScript).
                duplicateScript(originalScript, appendedScript).
                validateScript(appendedScript).
                getAutomationPage().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-394", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-143")
    })
    public void testDeleteScript() throws ScriptingLanguageNotSpecifiedException, MissingScriptIdException, MissingScriptContentException, InvalidEventHandlerException, DuplicateScriptException {
        final AutomationScript script = new AutomationScript(RandomHelper.randomLetters(15)).description(RandomHelper.randomLetters(15)).contents(RandomHelper.randomStringLines(10, 3)).language(GROOVY);
        final EventHandler eventHandler = new EventHandler(PROJECT_EVENT).script(script);

        loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab().
                addScript(script).
                deleteScript(script).
                addScript(script.language(JAVASCRIPT)).
                getAutomationPage().
                searchForProject(scriptProject).
                loadManageTab().
                loadEventManagement().
                addEventHandler(eventHandler).
                loadAutomationPage().
                loadScriptsTab().
                deleteScript(script).
                getAutomationPage().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-395", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-144")
    })
    public void testMissingFields() throws MissingScriptContentException, ScriptingLanguageNotSpecifiedException, MissingScriptIdException, DuplicateScriptException {
        final String scriptId = RandomHelper.randomLetters(13);
        final String scriptContents = RandomHelper.randomStringLines(30, 2);
        final AutomationScript script = new AutomationScript(scriptId).language(GROOVY).contents(scriptContents);

        final ScriptsTab scriptsTab = loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab();

        try {
            scriptsTab.launchScriptCreation(new AutomationScript(null).prespecify(false).language(NONE));
            fail("Exception should have been thrown.");
        } catch (ScriptingLanguageNotSpecifiedException e) {
            captureScreenshotlessStep();
        }

        try {
            scriptsTab.addScript(new AutomationScript(null).language(GROOVY).contents(RandomHelper.randomStringLines(30, 2)));
            fail("Exception should have been thrown.");
        } catch (MissingScriptIdException e) {
            captureScreenshotlessStep();
        }

        try {
            scriptsTab.addScript(new AutomationScript(scriptId).language(GROOVY));
            fail("Exception should have been thrown.");
        } catch (MissingScriptContentException e) {
            captureScreenshotlessStep();
        }

        final EventHandlerListComponent eventHandlerListComponent = scriptsTab.assertScriptNotPresent(scriptId).
                captureScripts().
                addScript(script).
                getAutomationPage().
                logLogout().
                seleniumLogin().
                searchForProject(scriptProject).
                loadManageTab().
                loadEventManagement();
        final EventHandler missingEvent = new EventHandler(null).script(script);
        final EventHandler missingScript = new EventHandler(PROJECT_EVENT);

        for (EventHandler eventHandler : Arrays.asList(missingEvent, missingScript)) {
            try {
                eventHandlerListComponent.addEventHandler(eventHandler);
                fail("Exception should have been thrown.");
            } catch (InvalidEventHandlerException e) {
                captureScreenshotlessStep();
            }
        }

        eventHandlerListComponent.assertNoEventHandlers().
                capture().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-396", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-145")
    })
    public void testDuplicateFields() throws DuplicateScriptException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException {
        final String scriptId = RandomHelper.randomLetters(20);
        final AutomationScript originalScript = new AutomationScript(scriptId).language(GROOVY).description(RandomHelper.randomLetters(10)).contents(RandomHelper.randomStringLines(20, 2));
        final AutomationScript duplicateScript = new AutomationScript(scriptId).language(GROOVY).description(RandomHelper.randomLetters(10)).contents(RandomHelper.randomStringLines(20, 2));

        final ScriptsTab scriptsTab = loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab().
                addScript(originalScript);
        try {
            scriptsTab.addScript(duplicateScript);
            fail("Exception should have been thrown");
        } catch (DuplicateScriptException e) {
            captureScreenshotlessStep();
        }
        scriptsTab.validateScript(originalScript);

        scriptsTab.deleteScript(originalScript);
        logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-397", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-146")
    })
    public void testDeleteEventHandler() throws DuplicateScriptException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, InvalidEventHandlerException {
        final AutomationScript script = new AutomationScript(RandomHelper.randomLetters(20)).description(RandomHelper.randomLetters(16)).contents(RandomHelper.randomStringLines(20, 2)).language(GROOVY);
        final EventHandler eventHandler = new EventHandler(PROJECT_EVENT).script(script);

        loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab().
                addScript(script).
                getAutomationPage().
                logLogout().
                seleniumLogin().
                searchForProject(scriptProject).
                loadManageTab().
                loadEventManagement().
                addEventHandler(eventHandler).
                deleteEventHandler(eventHandler).
                assertNoEventHandlers().
                capture().
                logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-398", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-147")
    })
    public void testGroovyScript() throws DuplicateScriptException, InvalidEventHandlerException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, IOException {
        testStandardScript(GROOVY, true);
    }

    @Test
    @JiraKey(simpleKey = "QA-399", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-234")
    })
    public void testJS_Script() throws DuplicateScriptException, InvalidEventHandlerException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, IOException {
        testStandardScript(JAVASCRIPT, true);
    }

    @Test
    @JiraKey(simpleKey = "QA-400", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-235")
    })
    public void testPythonScript() throws DuplicateScriptException, InvalidEventHandlerException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, IOException {
        testStandardScript(PYTHON, true);
    }

    @Test
    @JiraKey(simpleKey = "QA-401", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-236")
    })
    public void testGroovySiteScript() throws DuplicateScriptException, InvalidEventHandlerException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, IOException {
        testStandardScript(GROOVY, false);
    }

    @Test
    @JiraKey(simpleKey = "QA-402", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-237")
    })
    public void testJS_SiteScript() throws DuplicateScriptException, InvalidEventHandlerException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, IOException {
        testStandardScript(JAVASCRIPT, false);
    }

    @Test
    @JiraKey(simpleKey = "QA-403", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-238")
    })
    public void testPythonSiteScript() throws DuplicateScriptException, InvalidEventHandlerException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, IOException {
        testStandardScript(PYTHON, false);
    }

    @Test
    @JiraKey(simpleKey = "QA-404", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-240")
    })
    public void testScriptQueueing() throws DuplicateScriptException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, InvalidEventHandlerException, IOException {
        final File goodScriptSource = Paths.get(SCRIPT_LOCATION, "bonusScript." + GROOVY.getFileExtension()).toFile();
        final AutomationScript badScript = new AutomationScript("bad_queue_" + RandomHelper.randomID(8)).language(GROOVY).description("Garbage script").contents("Ah ah ah, you didn't say the magic word.");
        final AutomationScript goodScript = new AutomationScript("good_queue_" + RandomHelper.randomID(8)).language(GROOVY).description("Valid script").source(goodScriptSource);
        final EventHandler badEventHandler = new EventHandler(PROJECT_EVENT).script(badScript);
        final EventHandler goodEventHandler = new EventHandler(PROJECT_EVENT).script(goodScript);
        final ProjectResource resourceFolder = new ProjectResource(scriptProject, "bonus_folder");

        final IndexPage indexPage = loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab().
                addScript(badScript).
                addScript(goodScript).
                getAutomationPage().
                logLogout().
                seleniumLogin();
        mainInterface().uploadResource(resourceFolder);
        captureScreenshotlessStep();

        indexPage.searchForProject(scriptProject).
                loadManageTab().
                loadEventManagement().
                addEventHandler(badEventHandler);

        for (int i = 0; i < 10; i++) {
            indexPage.createSubject(new Subject(scriptProject));
            stepCounter.decrement();
        }
        stepCounter.increment();

        indexPage.searchForProject(scriptProject).
                loadManageTab().
                loadEventManagement().
                deleteEventHandler(badEventHandler).
                addEventHandler(goodEventHandler).
                createSubject(testSpecificSubject.project(scriptProject));

        assertScriptExecution(goodScript, resourceFolder, goodEventHandler, testSpecificSubject.getLabel());
        logLogout();
    }

    private void testStandardScript(ScriptingLanguageType scriptLanguage, boolean isProjectSpecific) throws DuplicateScriptException, MissingScriptIdException, ScriptingLanguageNotSpecifiedException, MissingScriptContentException, InvalidEventHandlerException, IOException {
        final String scope = (isProjectSpecific) ? "proj" : "site";
        final ProjectResource resourceFolder = new ProjectResource(scriptProject, String.format("%s_%s_folder", scriptLanguage.getLanguageName(), scope));
        final File scriptSource = Paths.get(SCRIPT_LOCATION, String.format("%s%sScript.%s", scriptLanguage.getLanguageName(), (isProjectSpecific) ? "" : "Site", scriptLanguage.getFileExtension())).toFile();
        final String event = (isProjectSpecific) ? PROJECT_EVENT : SITE_EVENT;
        final String session = RandomHelper.randomID();
        final AutomationScript script = new AutomationScript(scope + RandomHelper.randomID(8)).language(scriptLanguage).description(RandomHelper.randomID(12)).source(scriptSource);
        final EventHandler eventHandler = new EventHandler(event).scope(isProjectSpecific).script(script);

        final IndexPage indexPage = loginPage.seleniumAdminLogin().
                loadAutomationPage().
                loadScriptsTab().
                addScript(script).
                getAutomationPage().
                logLogout().
                seleniumLogin();
        mainInterface().uploadResource(resourceFolder);
        captureScreenshotlessStep();

        if (isProjectSpecific) {
            indexPage.searchForProject(scriptProject).
                    loadManageTab().
                    loadEventManagement().
                    addEventHandler(eventHandler).
                    createSubject(testSpecificSubject.project(scriptProject));
        } else {
            logLogout().
                    seleniumAdminLogin().
                    loadAutomationPage().
                    loadSiteEventHandlersTab().
                    getEventHandlerListComponent().
                    addEventHandler(eventHandler).
                    logLogout().
                    seleniumLogin().
                    createSubject(testSpecificSubject.project(scriptProject)).
                    createSession(new MRSession(scriptProject, testSpecificSubject, session));
        }

        assertScriptExecution(script, resourceFolder, eventHandler, (isProjectSpecific) ? testSpecificSubject.getLabel() : session);

        logLogout();
    }

    private void assertPipeline(String workflow, String expectedPipeline) {
        assertEquals(expectedPipeline, mainInterface().readWorkflow(workflow).getPipelineName());
    }

    private void assertScriptExecution(AutomationScript script, ProjectResource resourceFolder, EventHandler eventHandler, String createdEntity) throws IOException {
        navigate().to(String.format("%s/app/action/DisplayItemAction/search_value/Executed script %s/search_element/wrk:workflowData/search_field/wrk:workflowData.pipeline_name", Settings.BASEURL, script.getScriptID()));
        loadPage(WorkflowReportPage.class).waitForWorkflowComplete().capture();

        mainQueryBase().post(formatRestUrl("/services/refresh/catalog?resource=/archive/projects/", scriptProject.getId(), "/resources/", resourceFolder + "&options=append")).then().statusCode(200);
        captureScreenshotlessStep();

        final List<String> outputLines = IOUtils.readLines(mainInterface().streamResourceFile(resourceFolder, new ResourceFile().name("testFile.txt")), StandardCharsets.UTF_8);

        assertEquals("Here goes nothing...", outputLines.get(0));
        assertEquals(String.format("User: %s", Settings.MAIN_USERNAME), outputLines.get(1));
        captureScreenshotlessStep();

        assertTrue(outputLines.get(2).matches("srcWorkflowId: [0-9]+"));
        final String sourceWorkflowId = outputLines.get(2).split(" ")[1];
        commentStep("srcWorkflowId: " + sourceWorkflowId);
        assertPipeline(sourceWorkflowId, eventHandler.getEventID());
        captureScreenshotlessStep();

        assertTrue(outputLines.get(3).matches("scriptWorkflowId: [0-9]+"));
        final String scriptWorkflowId = outputLines.get(3).split(" ")[1];
        commentStep("scriptWorkflowId: " + scriptWorkflowId);
        assertPipeline(scriptWorkflowId, "Executed script " + script.getScriptID());
        captureScreenshotlessStep();

        final String dataId = outputLines.get(4).split(" ")[1];
        commentStep("dataId: " + dataId);
        final IndexPage indexPage = home();
        if (eventHandler.isProjectSpecific()) {
            indexPage.searchForSubject(dataId).assertPageRepresents(new Subject().label(createdEntity)).capture();
        } else {
            indexPage.searchForSession(dataId).assertPageRepresents(new MRSession().label(createdEntity)).capture();
        }

        assertEquals(String.format("externalId: %s", scriptProject), outputLines.get(5));

        assertEquals("dataType: " + (eventHandler.isProjectSpecific() ? DataType.SUBJECT : DataType.MR_SESSION).getXsiType(), outputLines.get(6));

        assertEquals(String.format("workflow: %s", scriptWorkflowId), outputLines.get(7));
        captureScreenshotlessStep();

        if (eventHandler.isProjectSpecific()) {
            indexPage.searchForProject(scriptProject).
                    loadManageTab().
                    loadEventManagement().
                    deleteEventHandler(eventHandler);
        } else {
            logLogout().
                    seleniumAdminLogin().
                    loadAutomationPage().
                    loadSiteEventHandlersTab().
                    getEventHandlerListComponent().
                    deleteEventHandler(eventHandler);
        }
    }

    private AutomationScript scriptForModTest() {
        return new AutomationScript(RandomHelper.randomID()).description(RandomHelper.randomID(15)).contents(RandomHelper.randomStringLines(25, 5));
    }

}
