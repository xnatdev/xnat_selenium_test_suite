package org.nrg.xnat.selenium.tests;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.PasswordExpirationType;
import org.nrg.selenium.xnat.page_model.admin.AdminUiPage;
import org.nrg.testing.TimeUtils;
import org.nrg.testing.annotations.DisallowXnatVersion;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.Users;
import org.nrg.testing.xnat.database.XnatDatabaseCommand;
import org.nrg.testing.xnat.ssh.SSHConnection;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.rest.Credentials;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestLoginSettings extends BaseSeleniumTest {

    private boolean maxSessionResetNeeded = false;
    private static StopWatch cacheTimer;
    private static final Logger LOGGER = Logger.getLogger(TestLoginSettings.class);

    @BeforeClass
    public void startTimer() {
        cacheTimer = TimeUtils.launchStopWatch();
    }

    @Test
    @TestRequires(db = true, ssh = true)
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-374")
    public void testMaxSessions() {
        final User user = Users.genericAccount();
        final int maxSessions = 10;

        final AdminUiPage adminUiPage = loginPage.seleniumAdminLogin().
                loadAdminUsersPage().
                createUser(user).
                loadAdminUi().
                setMaxSessions(maxSessions);
        maxSessionResetNeeded = true;
        adminUiPage.logLogout();

        mainAdminInterface().expireAllActiveSessions(); // Safety feature to avoid getting locked out on this account
        captureScreenshotlessStep();
        new SSHConnection().restartTomcat();
        captureScreenshotlessStep();

        validateSuccessfulConnections(user, maxSessions);

        Credentials.build(user).get(mainInterface().userSessionsRestUrl(user)).then().assertThat().statusCode(401);
        captureScreenshotlessStep();

        loginPage.assertFailedLogin(user).
                seleniumAdminLogin().
                loadAdminUi().
                setMaxSessions(getDefaults().getDefaultMaxSessions()).
                logLogout();

        new SSHConnection().restartTomcat();
        captureScreenshotlessStep();
        maxSessionResetNeeded = false;

        validateSuccessfulConnections(user, maxSessions + 1);

        loginPage.login(user).logLogout();
    }

    @Test
    @TestRequires(data = TestData.SAMPLE_1)
    @JiraKey(simpleKey = "QA-335")
    public void testGuestAccess() {
        final Project privateProject = new Project().accessibility(Accessibility.PRIVATE);
        final Project publicProject = testSpecificProject.accessibility(Accessibility.PUBLIC);
        final String subject = "Sample_Patient"; // created by compressed upload

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setLoginRequired(false).
                createProject(publicProject).
                loadCompressedUploader().
                uploadToArchive(testSpecificProject, TestData.SAMPLE_1).
                createProject(privateProject).
                logLogout().
                home().
                assertLogin(User.GUEST, true).
                verifyNoSearchResultsFound(privateProject.getId()).
                searchForProject(testSpecificProject).
                capture().
                clickSubjectLink(subject).
                clickMrSessionLink().
                assertFilesRepresentSample1().
                getScanTable().
                capture().
                getUnderlyingPage().
                assertAutoRunCompletion().
                assertSnapshotGeneration().
                loadLoginPage().
                seleniumAdminLogin().
                loadAdminUi().
                setLoginRequired(true).
                logLogout();
    }

    @Test
    @TestRequires(db = true, users = 1)
    @JiraKey(simpleKey = "QA-299", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-296")
    })
    public void testPasswordExpirationDate() {
        testPasswordExpiration(PasswordExpirationType.DATE, getDefaults().getStandardExpirationDate());
    }

    @Test
    @TestRequires(db = true, users = 1)
    @JiraKey(simpleKey = "QA-300", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-297")
    })
    public void testPasswordExpirationInterval() {
        testPasswordExpiration(PasswordExpirationType.INTERVAL, getDefaults().getStandardExpirationInterval());
    }

    @Test
    @TestRequires(db = true, users = 1)
    @JiraKey(simpleKey = "QA-301", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-298")
    })
    public void testPasswordExpirationDisabled() {
        testPasswordExpiration(PasswordExpirationType.DISABLED, null);
    }

    @AfterMethod(alwaysRun = true)
    public void fixSettings(ITestResult result) {
        if (maxSessionResetNeeded) {
            constructXnatDriver();
            loginPage.
                    seleniumAdminLogin().
                    loadAdminUi().
                    setMaxSessions(getDefaults().getDefaultMaxSessions()).
                    logout();
            new SSHConnection().restartTomcat();
            maxSessionResetNeeded = false;
        }
    }

    private void validateSuccessfulConnections(User user, int numberOfConnections) {
        for (int i = 0; i < numberOfConnections; i++) {
            restDriver.getJson(
                    Credentials.build(user), mainInterface().userSessionsRestUrl(user)
            ).then().assertThat().statusCode(200).and().body(user.getUsername(), Matchers.equalTo(i + 1));
        }
        captureScreenshotlessStep();
    }

    private void testPasswordExpiration(PasswordExpirationType expirationType, String expirationData) {
        final long minWaitTime = 300000;
        final String newPass = RandomHelper.randomID(18);
        final User user = getGenericUser();
        final String expirePassQuery = String.format("UPDATE xhbm_xdat_user_auth SET password_updated = '2000-12-12 12:12:12.121' WHERE xdat_username = '%s'", user.getUsername());

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setPasswordExpiration(expirationType, expirationData).
                logLogout();
        new XnatDatabaseCommand(expirePassQuery).execute();
        captureScreenshotlessStep();

        final long waitedTime = cacheTimer.getTime();
        if (waitedTime < minWaitTime) {
            LOGGER.info(String.format("Waiting an additional %d milliseconds for the user cache to clear...", minWaitTime - waitedTime));
            TimeUtils.sleep(minWaitTime - waitedTime);
        }

        if (expirationType == PasswordExpirationType.DISABLED) {
            loginPage.login(user).assertLogin(user);
        } else {
            loginPage.
                    loginToExpiredPasswordAccount(user).
                    updatePasswordFor(user, newPass).
                    logLogout().
                    login(user).
                    assertLogin(user);
        }
        home().
                logLogout().
                seleniumAdminLogin(). // Make sure valid accounts still A-OK!
                logLogout();
    }

}
