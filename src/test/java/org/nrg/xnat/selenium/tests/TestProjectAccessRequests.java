package org.nrg.xnat.selenium.tests;

import org.apache.log4j.Logger;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.SyntheticPageObject;
import org.nrg.selenium.xnat.page_model.XnatLoginPage;
import org.nrg.selenium.xnat.page_model.XnatLogoutPage;
import org.nrg.selenium.xnat.page_model.project.report.ManageProjectAccessRequestPage;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.selenium.xnat.page_model.users.AcceptParPage;
import org.nrg.selenium.xnat.page_model.users.PostregistrationPage;
import org.nrg.selenium.xnat.page_model.users.RegistrationPage;
import org.nrg.testing.TestNgUtils;
import org.nrg.testing.annotations.DisallowXnatVersion;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.email.EmailQuery;
import org.nrg.testing.email.XnatEmailCatalog;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.Users;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.enums.SiteDataRole;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.pogo.users.UserGroup;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_7_4;
import org.nrg.xnat.versions.Xnat_1_7_5;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.nrg.xnat.pogo.users.UserGroups.*;
import static org.testng.AssertJUnit.fail;

@TestRequires(email = true)
public class TestProjectAccessRequests extends BaseSeleniumTest {

    private boolean accountsCreated = true;
    private User nonAccessUser, semiAccessUser, fullAccessUser;
    private User parUser;
    private Project parProject;
    // These will all be initialized randomly in the @BeforeMethod method. Some subset of these is required for each test
    private static final Logger LOGGER = Logger.getLogger(TestProjectAccessRequests.class);

    @BeforeClass
    public void setupPARAccounts() {
        // Initialize users here since calling AssortedUtils.permuteSeleniumEmail() fails if XNAT hasn't been initialized
        try {
            nonAccessUser = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10));
            semiAccessUser = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10)).dataRole(SiteDataRole.ALL_DATA_ACCESS);
            fullAccessUser = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10)).dataRole(SiteDataRole.ALL_DATA_ADMIN);

            constructRestDriver();
            mainAdminInterface().createUser(nonAccessUser);
            mainAdminInterface().createUser(semiAccessUser);
            mainAdminInterface().createUser(fullAccessUser);
        } catch (Exception | Error e) {
            LOGGER.warn("PAR account setup failed. Tests requiring previously set up accounts will be skipped. Setup failed due to:", e);
            accountsCreated = false;
        }
    }

    @BeforeMethod
    public void randomizeVariables() {
        parUser = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10));
        parProject = new Project().runningTitle(RandomHelper.randomID()).title(RandomHelper.randomID());
    }

    @AfterClass
    public void disableAutoenabling() {
        constructXnatDriver();
        new SyntheticPageObject(getXnatDriver()).
                loadLoginPage().
                seleniumAdminLogin().
                loadAdminUi().
                setAutoEnable(false).
                logout().
                quit();
    }

    @Test
    @JiraKey(simpleKey = "QA-430", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-338")
    })
    public void testLoggedInPAR() {
        performExistingUserPARTest(true, OWNER);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-339")
    public void testLoggedOutPAR() {
        performExistingUserPARTest(false, MEMBER);
    }

    @Test
    @JiraKey(simpleKey = "QA-340")
    public void testAutoenableAutoParEnable() {
        performNewUserPARTest(true, true, false, COLLABORATOR);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-341")
    public void testAutoenableNoAutoParEnable() {
        performNewUserPARTest(true, false, false, MEMBER);
    }

    @Test
    @JiraKey(simpleKey = "QA-342")
    public void testNoAutoenableAutoParEnable() {
        performNewUserPARTest(false, true, false, OWNER);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class) // 1.6.* always autoenables
    @JiraKey(simpleKey = "QA-343")
    public void testNoAutoenableNoAutoParEnable() {
        performNewUserPARTest(false, false, false, COLLABORATOR);
    }

    @Test
    @JiraKey(simpleKey = "QA-140")
    public void testNondirectPAR() {
        performNewUserPARTest(true, true, true, OWNER);
    }

    @Test
    @JiraKey(simpleKey = "QA-349")
    public void testProtectedOwnerRequest() {
        performRequestProjectAccessTest(Accessibility.PROTECTED, OWNER, true, nonAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-350")
    public void testPublicOwnerRequest() {
        performRequestProjectAccessTest(Accessibility.PUBLIC, OWNER, true, nonAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-351")
    public void testProtectedMemberRequest() {
        performRequestProjectAccessTest(Accessibility.PROTECTED, MEMBER, true, nonAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-352")
    public void testPublicMemberRequest() {
        performRequestProjectAccessTest(Accessibility.PUBLIC, MEMBER, true, nonAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-353")
    public void testProtectedCollaboratorRequest() {
        performRequestProjectAccessTest(Accessibility.PROTECTED, COLLABORATOR, true, nonAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-354")
    public void testPublicCollaboratorRequest() {
        performRequestProjectAccessTest(Accessibility.PUBLIC, COLLABORATOR, true, nonAccessUser);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class) // bug fixed only in 1.7 line
    @JiraKey(simpleKey = "QA-355")
    public void testReadAllUserRequest() {
        performRequestProjectAccessTest(Accessibility.PROTECTED, OWNER, true, semiAccessUser);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class) // bug fixed only in 1.7 line
    @JiraKey(simpleKey = "QA-356")
    public void testModifyAllUserRequest() {
        performRequestProjectAccessTest(Accessibility.PROTECTED, OWNER, true, fullAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-357")
    public void testProtectedMemberRequestDeny() {
        performRequestProjectAccessTest(Accessibility.PROTECTED, MEMBER, false, nonAccessUser);
    }

    @Test
    @JiraKey(simpleKey = "QA-358")
    public void testPublicOwnerRequestDeny() {
        performRequestProjectAccessTest(Accessibility.PUBLIC, OWNER, false, nonAccessUser);
    }

    private void performExistingUserPARTest(boolean loggedIn, UserGroup role) {
        final XnatLogoutPage logoutPage = loginPage.seleniumAdminLogin().
                loadAdminUsersPage().
                createUser(parUser).
                logLogout().
                seleniumLogin().
                createProject(parProject).
                loadAccessTab().
                sendPAR(Settings.permuteSeleniumEmail(), role).
                getProjectPage().
                logLogout();
        acceptParFromEmail(logoutPage, loggedIn).
                assertPageRepresents(parProject).
                assertPageRepresentsGroup(role).
                logLogout().
                login(parUser).
                searchForProject(parProject).
                assertPageRepresentsGroup(role).
                logLogout();
    }

    private ProjectReportPage acceptParFromEmail(XnatLogoutPage logoutPage, boolean preemptiveLogin) {
        if (preemptiveLogin) {
            logoutPage.login(parUser);
            goToPAR();
            return loadPage(ProjectReportPage.class);
        } else {
            goToPAR();
            return loadPage(AcceptParPage.class).
                    login(parUser).
                    searchForProject(parProject);
        }
    }

    private void performNewUserPARTest(boolean autoEnable, boolean parAutoEnables, boolean isNondirect, UserGroup role) {
        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setPARSettings(autoEnable, parAutoEnables).
                logLogout().
                seleniumLogin().
                createProject(parProject).
                loadAccessTab().
                sendPAR(parUser.getEmail(), role).
                getProjectPage().
                logLogout();
        final RegistrationPage registrationPageObject;
        if (isNondirect) {
            registrationPageObject = loginPage.clickRegisterLink();
        } else {
            goToPAR();
            registrationPageObject = loadPage(AcceptParPage.class).
                    clickRegisterButton().
                    getRegistrationPageSubcomponent();
        }
        registrationPageObject.fillCommonFields(parUser).
                capture().
                clickRegisterButton();
        if (isNondirect) {
            // Assumption: isNondirect => autoEnable && parAutoEnables && requireVerify
            loadPage(PostregistrationPage.class).
                    assertVerificationRequired().
                    capture();
            new EmailQuery()
                    .fromCatalog(XnatEmailCatalog.verificationEmail(parUser))
                    .queryUntilSingleResult()
                    .validateEach(message -> navigate().to(message.readVerificationEmailLink()));

            if (XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_7_5.class)) {
                loginPage.loadPage(XnatLoginPage.class, false, true).
                        assertVerificationMessageFor(parUser).
                        capture().
                        login(parUser);
            }
            loadPage(IndexPage.class).acceptSingleNondirectPAR(parProject);
        } else if (!autoEnable && !parAutoEnables) {
            registrationPageObject.assertRegistrationDoesNotRequireEmailVerification().capture();
            new EmailQuery()
                    .containing("New User Request")
                    .containing(parUser.getUsername())
                    .queryUntilSingleResult();
            captureScreenshotlessStep();
            loginPage.seleniumAdminLogin().
                    loadAdminUsersPage().
                    enableUser(parUser).
                    logLogout().
                    login(parUser);
        }

        home().
                searchForProject(parProject).
                assertPageRepresents(parProject).
                assertPageRepresentsGroup(role).
                logLogout();
    }

    private void performRequestProjectAccessTest(Accessibility accessibility, UserGroup desiredAccess, boolean acceptRequest, User user) {
        TestNgUtils.assumeTrue(accountsCreated, "Cannot perform test requesting project access because accounts were not initialized...");
        final String description = "Test project to be used in testing project access requests.";
        final String comment = "PAR comment: " + RandomHelper.randomID(30);

        loginPage.seleniumLogin().
                createProject(parProject.description(description).accessibility(accessibility)).
                logLogout().
                login(user).
                requestProjectAccess(parProject, desiredAccess, comment).
                logLogout().
                seleniumLogin();
        navigate().to(extractAccessRequestLink());
        final IndexPage indexPage = loadPage(ManageProjectAccessRequestPage.class).
                processRequestedAccess(acceptRequest, user, parProject, desiredAccess).
                capture().
                getProjectPage().
                logLogout().
                login(user);
        if (acceptRequest) {
            indexPage.assertDisplayedProjectGroup(parProject, desiredAccess);
        } else {
            indexPage.assertProjectAccessRequestLinkPresent(parProject);
        }
        final ProjectReportPage projectReportPage = indexPage.captureProjectListing().clickProjectListing(parProject);
        if (acceptRequest) {
            projectReportPage.assertPageRepresentsGroup(desiredAccess);
        } else {
            switch (accessibility) {
                case PROTECTED :
                    projectReportPage.assertProtectedProjectWarning(parProject);
                    break;
                case PUBLIC :
                    projectReportPage.assertPageRepresentsGroup(COLLABORATOR);
                    break;
                default :
                    fail("Other project accessibility levels don't make sense here.");
            }
        }
        projectReportPage.logLogout();
        checkPARActionEmail(acceptRequest);
        captureScreenshotlessStep();
    }

    private void checkPARActionEmail(boolean approved) {
        new EmailQuery()
                .fromCatalog(XnatEmailCatalog.projectAccessEmail(parProject, approved))
                .queryUntilSingleResult();
    }

    private String extractPARLink() {
        if (XnatTestingVersionManager.testedVersionFollows(Xnat_1_7_4.class)) {
            return new EmailQuery()
                    .containing(String.format("You have been invited to join the %s project", parProject.getTitle()))
                    .queryUntilSingleResult()
                    .getEmail()
                    .extractFirstLink();
        } else {
            return new EmailQuery()
                    .containing(parProject.getTitle() + " access invitation")
                    .queryUntilSingleResult()
                    .getEmail()
                    .extractLinkByText("Proceed to the site to get started reviewing/using the data.");
        }
    }

    private String extractAccessRequestLink() {
        if (XnatTestingVersionManager.testedVersionFollows(Xnat_1_7_4.class)) {
            return new EmailQuery()
                    .containing(String.format("We received a request to access the %s project", parProject.getTitle()))
                    .queryUntilSingleResult()
                    .getEmail()
                    .extractFirstLink();
        } else {
            return new EmailQuery()
                    .containing(parProject.getTitle() + " access request")
                    .queryUntilSingleResult()
                    .getEmail()
                    .extractLinkByText("Proceed to the site to approve or deny the user's request.");
        }
    }

    private void goToPAR() {
        navigate().to(extractPARLink());
    }

}
