package org.nrg.xnat.selenium.tests;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.*;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.io.FileUtils;
import org.nrg.jira.components.zephyr.TestStatus;
import org.nrg.selenium.*;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.selenium.xnat.page_model.site_search.AdvancedSearchPage;
import org.nrg.selenium.xnat.page_model.site_search.AdvancedSearchResult;
import org.nrg.selenium.xnat.page_model.site_search.DataTypeListing;
import org.nrg.selenium.xnat.page_model.site_search.StoredSearchReportPage;
import org.nrg.selenium.xnat.page_model.ui_element.XmlDialog;
import org.nrg.selenium.xnat.page_model.ui_element.YuiTable;
import org.nrg.testing.TestNgUtils;
import org.nrg.testing.annotations.Basic;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.email.EmailQuery;
import org.nrg.testing.email.MessageTemplate;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.enums.Gender;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.sessions.CTSession;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.experiments.sessions.PETSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestSearching extends BaseSeleniumTest {

    private final Project project = new Project();
    private final Subject subject = new Subject(project);

    @BeforeClass
    public void setupProject() {
        constructRestDriver();
        mainInterface().createProject(project);
    }

    @Test
    @JiraKey(simpleKey = "QA-274", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-159")
    })
    public void testAdvancedSearchAlphabetization() {
        final AdvancedSearchPage advancedSearchPage = loginPage.seleniumAdminLogin().loadAdvancedSearchPage();
        TestNgUtils.assertCaseInsensitiveAlphabeticalOrder(advancedSearchPage.readAdditionalDataTypes());
        advancedSearchPage.captureAdditionalDataTypeTable().logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-160")
    public void testPETTabGeneration() {
        // Tests a bug in generating the PET Session tab for a project

        final String petId = RandomHelper.randomID(18);

        final ProjectReportPage projectReportPage = loginPage.seleniumLogin().
                createSession(new PETSession(project, subject, petId)).
                logLogout().
                seleniumLogin().
                searchForProject(project);
        projectReportPage.loadProjectDataTable().
                switchToTab(DataType.PET_SESSION.getPluralName()).
                assertColumnContains(0, petId).
                capture().
                launchFilteringOnColumn("XNAT_PETSESSIONDATA ID").
                capture().
                addLikeFilter("~!@#$%^&*(){}|[]\\").
                submitFilters().
                assertSearchResultsFailed().
                capture();
        projectReportPage.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-275", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-161")
    })
    public void testShowXML() {
        final DataTypeListing dataTypeListing = loginPage.seleniumLogin().
                createSession(new CTSession(project, subject)).
                logLogout().
                seleniumLogin().
                loadDataTypeListing(DataType.CT_SESSION);
        final XmlDialog xmlDialog = dataTypeListing.getDataTable().
                optionsShowXml();
        xmlDialog.getxModal().
                assertTextContains("<?xml version=\"1.0\" encoding=\"UTF-8\"").
                assertTextContains("<xdat:bundle ID=\"@xnat:ctSessionData\"").
                capture();
        captureStep(stepCounter, TestStatus.UNEXECUTED, "TODO", false); // TODO: Assert that the XML is well-formed too.
        xmlDialog.returnToUnderlyingPage();
        dataTypeListing.logLogout();
    }

    @Test
    @Basic
    @TestRequires(email = true)
    @JiraKey(simpleKey = "QA-279", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-162")
    })
    public void testAdvancedSearch() throws IOException {
        final String filterString = "case";
        final String sessionLabelHeader = "Label";
        final String dateHeader = "Date";
        final String mrTypeField = "Type (MR Session)";
        final String typeColumn = "Type";

        final IndexPage indexPage = loginPage.seleniumLogin();

        for (int i = 0; i < 5; i++) {
            // Create some appropriate MRs to filter on
            final String label = (i != 0) ? permuteFilterString(filterString) : "zzz1234512345";
            final MRSession session = new MRSession(project, subject, label).date(LocalDate.of(2010 + i, i + 1, 9 - i));
            indexPage.createSession(session);
            stepCounter.decrement();
        }
        stepCounter.increment();

        final YuiTable dataTable = indexPage.logLogout().
                seleniumLogin().
                loadAdvancedSearchPage().
                selectPivotDataType(DataType.MR_SESSION).
                selectAdditionalDataType(DataType.SUBJECT, true).
                screenshotPivotDataTypeSection().
                screenshotAdditionalDataTypeTable().
                nextSearchPage().
                screenshotCriteriaSection().
                submitSearch().
                capture().
                launchFilteringOnColumn(sessionLabelHeader).
                capture().
                addLikeFilter(filterString).
                uploadScreenshot().
                submitFilters();
        for (String sessionLabel : dataTable.readEntriesForColumn(sessionLabelHeader)) {
            assertTrue(sessionLabel.contains(filterString));
        }
        final List<String> sortedDates = dataTable.capture().
                sortDownOnColumn(dateHeader).
                readSortedColumn();
        sortedDates.removeIf(date -> date.contains(" "));
        assertTrue(sortedDates.size() >= 4);

        for (int i = 0; i < sortedDates.size() - 1; i++) {
            String laterDate = sortedDates.get(i);
            String earlierDate = sortedDates.get(i + 1);
            final LocalDate laterParsed = LocalDate.parse(laterDate);
            final LocalDate earlierParsed = LocalDate.parse(earlierDate);
            assertTrue(laterParsed.isAfter(earlierParsed) || laterParsed.isEqual(earlierParsed));
        }

        final List<String> headers = dataTable.capture().
                optionsEditColumns().
                capture().
                addField(mrTypeField).
                assertFieldCurrent(mrTypeField).
                capture().
                submitModifications().
                readHeaders();
        assertEquals(typeColumn, headers.get(headers.size() - 1));
        dataTable.capture();

        final MessageTemplate email = new MessageTemplate("searchEmailTemplate.txt");
        final String currentTime = email.usedTime();

        dataTable.optionsEmail().
                capture().
                fillRecipient(Settings.EMAIL).
                fillSubject("Automated Test of script XNAT-2756").
                fillBody(email.read()).
                uploadScreenshot().
                submitEmail();
        captureScreenshotlessStep();

        final File downloadedSpreadsheet = dataTable.optionsSpreadsheet();
        captureScreenshotlessStep();
        logLogout();

        final CSVReader csvReader = new CSVReader(new FileReader(downloadedSpreadsheet));
        assertEquals(headers, Arrays.asList(csvReader.readNext()));
        for (String[] dataRow : csvReader.readAll()) {
            assertTrue(dataRow[0].contains(filterString));
        }
        captureScreenshotlessStep();

        new EmailQuery()
                .containing(currentTime)
                .queryUntilSingleResult()
                .validateEach(message -> {
                    assertTrue(message.getEmailContent().contains(String.format("%s %s thought you might be interested in a data set", mainUser.getFirstName(), mainUser.getLastName())));
                });
        captureScreenshotlessStep();
    }

    @Test
    @JiraKey(simpleKey = "QA-278", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-163")
    })
    public void testAdvancedSearchProjectFilter() {
        // Tests that selecting a project in advanced search performs "=" filter and not a "LIKE"

        final Project project2 = new Project(project + "1");
        final Subject subject2 = new Subject(project2);
        final MRSession session = new MRSession(project, subject);
        final MRSession session2 = new MRSession(project2, subject2);

        final AdvancedSearchResult advancedSearchResult = loginPage.seleniumLogin().
                createSession(session).
                createProject(project2).
                createSubject(subject2).
                createSession(session2).
                loadAdvancedSearchPage().
                selectPivotDataType(DataType.MR_SESSION).
                screenshotPivotDataTypeSection().
                nextSearchPage().
                captureCriteriaSection().
                selectProject(project).
                screenshotCriteriaSection().
                submitSearch().
                capture();
        final YuiTable dataTable = advancedSearchResult.getDataTable();
        final Map<String, Integer> headerIndices = dataTable.readHeaderIndices();
        final int projectIndex = headerIndices.get("Project");
        final int labelIndex = headerIndices.get("Label");
        dataTable.assertColumnContains(labelIndex, session.getLabel()).
                assertColumnDoesNotContain(labelIndex, session2.getLabel()).
                assertColumnContains(projectIndex, project.getId()).
                assertColumnDoesNotContain(projectIndex, project2.getId());
        captureScreenshotlessStep();
        advancedSearchResult.logLogout();
    }

    @Test
    @Basic
    @JiraKey(simpleKey = "QA-277", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-164")
    })
    public void testCreateStoredSearch() {
        final String storedSearch = RandomHelper.randomString(12);

        final AdvancedSearchResult searchResult = loginPage.seleniumLogin().
                createSession(new PETSession(project, subject)).
                logLogout().
                seleniumLogin().
                loadAdvancedSearchPage().
                selectPivotDataType(DataType.PET_SESSION).
                selectAdditionalDataType(DataType.SUBJECT, true).
                screenshotPivotDataTypeSection().
                screenshotAdditionalDataTypeTable().
                nextSearchPage().
                captureCriteriaSection().
                specifyTracer("PIB").
                selectCriterion("Gender", "female").
                screenshotCriteriaSection().
                submitSearch();
        searchResult.capture().
                getDataTable().
                optionsSaveAsNewSearch().
                capture().
                fillBriefDescription(storedSearch).
                uploadScreenshot().
                saveSearch().
                capture();
        searchResult.logLogout().
                seleniumLogin().
                loadStoredSearch(storedSearch).
                getDataTable().
                assertCurrentTab(storedSearch).
                capture();
        searchResult.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-276", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-165")
    })
    public void testGUIStyleCSV() throws IOException {
        // Tests that the GUI's results match the downloaded spreadsheet when "guiStyle=true" is specified.

        final File xmlFile = Files.createTempFile("seleniumXML", ".xml").toFile();
        final File xmlTemplate = getDataFile("seleniumXML_sample.xml");
        final String bundleName = RandomHelper.randomString(15);

        final List<String> outputLines = new ArrayList<>();
        for (String line : FileUtils.readLines(xmlTemplate, StandardCharsets.UTF_8)) {
            outputLines.add(
                    line.replace("BUNDLE_NAME", bundleName).replace("MY_PROJECT", project.getId()).replace("selenium", mainUser.getUsername())
            );
        }
        FileUtils.writeLines(xmlFile, outputLines);
        captureScreenshotlessStep();

        final IndexPage indexPage = loginPage.seleniumAdminLogin().
                loadXmlUploadPage().
                attachFile(xmlFile).
                uploadScreenshot().
                upload(StoredSearchReportPage.class).
                assertStoredSearchId(bundleName).
                capture().
                logLogout().
                seleniumLogin();

        for (int i = 0; i < 3; i++) {
            indexPage.createSubject(new Subject(project).gender(Gender.get(RandomHelper.randomGender())));
            stepCounter.decrement();
        }
        stepCounter.increment();

        final List<List<String>> fullTable = indexPage.loadStoredSearch(bundleName).
                getDataTable().
                capture().
                readEntireTable();

        final InputStream restCsvStream = restDriver.mainQueryBase().queryParam("guiStyle", true).queryParam("format", "csv").get(formatRestUrl("/search/saved", bundleName, "/results")).asInputStream();
        captureScreenshotlessStep();

        final List<List<String>> fullDownloadedCsv = new ArrayList<>();
        for (String[] row : new CSVReader(new InputStreamReader(restCsvStream)).readAll()) {
            fullDownloadedCsv.add(Arrays.asList(row));
        }

        assertEquals(fullTable, fullDownloadedCsv);
        captureScreenshotlessStep();
        indexPage.logLogout();
    }

    private String permuteFilterString(String filterString) {
        String junkChars = RandomHelper.randomString(14);
        if (new Random().nextBoolean()) {
            return junkChars + filterString;
        }
        else {
            return filterString + junkChars;
        }
    }

}

