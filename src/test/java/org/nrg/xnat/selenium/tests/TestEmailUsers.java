package org.nrg.xnat.selenium.tests;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.exceptions.CredentialEmailLimitException;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.XnatLogoutPage;
import org.nrg.selenium.xnat.page_model.admin.AdminEmailPage;
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatLoginMessage;
import org.nrg.selenium.xnat.page_model.users.PasswordResetPage;
import org.nrg.selenium.xnat.page_model.users.PostregistrationPage;
import org.nrg.testing.TimeUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.annotations.XnatVersionLink;
import org.nrg.testing.email.EmailQuery;
import org.nrg.testing.email.MessageTemplate;
import org.nrg.testing.email.ParsedEmail;
import org.nrg.testing.email.XnatEmailCatalog;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.Users;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.xnat.versions.XnatTestingVersionManager;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_7_6;
import org.nrg.xnat.versions.Xnat_1_8_4;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.fail;


@TestRequires(email = true)
public class TestEmailUsers extends BaseSeleniumTest {

    @Test(groups = TestGroups.CLIENT_DEPLOYMENT)
    @JiraKey(simpleKey = "QA-267", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-110")
    })
    public void testForgotUsernameAndPass() throws CredentialEmailLimitException {
        final User newUserA = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10)).password(RandomHelper.randomID());
        final User newUserB = Users.genericAccount().firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10)).password(RandomHelper.randomID());

        loginPage.seleniumAdminLogin().
                loadAdminUsersPage().
                createUser(newUserA).
                createUser(newUserB).
                logLogout();

        requestAndCheckCredentials(newUserA);
        requestAndCheckCredentials(newUserB);
    }

    @Test
    @JiraKey(simpleKey = "QA-111")
    public void testResendVerifyLimit() {
        // assumes users must verify email and auto-enable is disabled
        final String spamUser = RandomHelper.randomID(12);
        final String spamPass = RandomHelper.randomString(12);
        final String spamEmail = Settings.permuteSeleniumEmail();
        final String spamFirstName = RandomHelper.randomLetters(10);
        final String spamLastName = RandomHelper.randomLetters(10);
        final int fullNumEmails = 6;

        final PostregistrationPage postregistrationPage = loginPage.clickRegisterLink().
                capture().
                fillUsername(spamUser).
                fillPassword(spamPass).
                fillPasswordCheck(spamPass).
                fillFirstName(spamFirstName).
                fillLastName(spamLastName).
                fillEmail(spamEmail).
                capture().
                clickRegisterButton().
                assertRegistrationRequiresEmailVerification();
        for (int i = 0; i < fullNumEmails; i++) {
            postregistrationPage.clickResendEmailButtion();
        }
        postregistrationPage.loadPage(XnatLoginMessage.class)
                .assertTextEquals("You have exceeded the allowed number of email requests. Please try again later.")
                .capture();

        final EmailQuery verificationLimitEmailQuery = new EmailQuery()
                .fromCatalog(XnatEmailCatalog.verificationEmail(
                        new User(spamUser).firstName(spamFirstName).lastName(spamLastName))
                );
        verificationLimitEmailQuery.queryUntilMatchingExactNumberOfResults(fullNumEmails);
        // Should be fullNumEmails in inbox. Once this is confirmed, search again to make sure there are still the same amount.
        TimeUtils.sleep(10000); // Valid use of sleep()
        verificationLimitEmailQuery.queryUntilMatchingExactNumberOfResults(fullNumEmails);
        captureScreenshotlessStep();
    }

    @Test
    @JiraKey(simpleKey = "QA-112")
    public void testForgotCredentialsEmailLimit() {
        // assumes users must verify email
        final User forgotUser = Users.genericAccount().password(RandomHelper.randomString(14)).firstName("Jens").lastName("Aasgaard");
        final User forgotPass = Users.genericAccount().password(RandomHelper.randomString(14)).firstName(RandomHelper.randomLetters(12)).lastName(RandomHelper.randomLetters(12));
        final int maxUsernameRequests = 5;
        final int maxPasswordResets = 5;

        final XnatLogoutPage logoutPage = loginPage.seleniumAdminLogin().
                loadAdminUsersPage().
                createUser(forgotUser).
                createUser(forgotPass).
                logLogout().
                waitForLogout();

        for (int i = 0; i < maxUsernameRequests + 1; i++) {
            try {
                logoutPage.loadLoginPage().
                        clickForgotCredentialsLink().
                        requestUsername(forgotUser.getEmail());
                if (i < maxUsernameRequests) {
                    stepCounter.decrement();
                } else {
                    fail(maxUsernameRequests + 1 + "th request for username email should have been rejected.");
                }
            } catch (CredentialEmailLimitException e) {
                if (i < maxUsernameRequests) {
                    fail("Email limit was reached too quickly.");
                }
            }
        }

        for (int i = 0; i < maxPasswordResets + 1; i++) {
            try {
                logoutPage.loadLoginPage().
                        clickForgotCredentialsLink().
                        requestPassword(forgotPass.getUsername());
                if (i < maxPasswordResets) {
                    stepCounter.decrement();
                } else {
                    fail(maxPasswordResets + 1 + "th request for password reset email should have been rejected.");
                }
            } catch (CredentialEmailLimitException e) {
                if (i < maxPasswordResets) {
                    fail("Email limit was reached too quickly.");
                }
            }
        }

        final EmailQuery usernameEmailQuery = new EmailQuery()
                .containing(forgotUsernameSearchTerm(forgotUser));
        final EmailQuery passwordEmailQuery = new EmailQuery()
                .containing(forgotPasswordSearchTerm(forgotPass));

        usernameEmailQuery.queryUntilMatchingExactNumberOfResults(maxUsernameRequests);
        passwordEmailQuery.queryUntilMatchingExactNumberOfResults(maxPasswordResets);

        // Wait 10 seconds and check again to make sure another email did not show up (valid use of sleep())
        TimeUtils.sleep(10000);
        usernameEmailQuery.queryUntilMatchingExactNumberOfResults(maxUsernameRequests);
        captureScreenshotlessStep();
        passwordEmailQuery.queryUntilMatchingExactNumberOfResults(maxPasswordResets);
        captureScreenshotlessStep();
    }

    @Test
    @JiraKey(simpleKey = "QA-266", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-113")
    })
    public void testNewUser() {
        // assumes users must verify email
        final User newUser = Users.genericAccount().password(RandomHelper.randomString(14)).firstName(RandomHelper.randomLetters(10)).lastName(RandomHelper.randomLetters(10));
        final String newPhone = "314-867-5309";
        final String newLab = RandomHelper.randomString(6);
        final String newComment = RandomHelper.randomString(20);

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setAutoEnable(false).
                logLogout().
                loadLoginPage().
                clickRegisterLink().
                capture().
                fillCommonFields(newUser).
                fillSpecificFields(newComment, newPhone, newLab).
                capture().
                clickRegisterButton().
                assertRegistrationRequiresEmailVerification().
                capture();

        new EmailQuery()
                .fromCatalog(XnatEmailCatalog.verificationEmail(newUser))
                .queryUntilSingleResult()
                .validateEach(message -> navigate().to(message.readVerificationEmailLink()));
        loginPage.assertLoginMessage("Thank you for your interest in our site. Your user account will be reviewed and enabled by the site administrator. When this is complete, you will receive an email inviting you to login to the site.").
                capture();

        final Document document = new EmailQuery()
                .containing(newUser.getUsername())
                .notContaining("MESSAGE") // In current 1.6dev code, 2 emails will be received, so we can filter out one of them
                .queryUntilSingleResult()
                .getEmail()
                .getDocument();
        document.outputSettings(new Document.OutputSettings().prettyPrint(false));

        if (XnatTestingVersionManager.testedVersionPrecedes(Xnat_1_8_4.class)) {
            assertEquals(newUser.getUsername(), document.select("th:contains(Username) ~ td").first().text());
            assertEquals(newUser.getFirstName(), document.select("th:contains(First) ~ td").first().text());
            assertEquals(newUser.getLastName(), document.select("th:contains(Last) ~ td").first().text());
            assertEquals(newPhone, document.select("th:contains(Phone) ~ td").first().text());
            assertEquals(newLab, document.select("th:contains(Lab) ~ td").first().text());
            assertEquals(newUser.getEmail(), document.select("th:contains(Email) ~ td").first().text());
            assertEquals(" " + newComment, document.select("b:matches(User Comments:)").first().nextSibling().toString());
        } else {
            assertEquals("Username: " + newUser.getUsername(), document.select("li:contains(Username)").first().text());
            assertEquals("First: " + newUser.getFirstName(), document.select("li:contains(First)").first().text());
            assertEquals("Last: " + newUser.getLastName(), document.select("li:contains(Last)").first().text());
            assertEquals("Phone: " + newPhone, document.select("li:contains(Phone)").first().text());
            assertEquals("Lab: " + newLab, document.select("li:contains(Lab)").first().text());
            assertEquals("Email: " + newUser.getEmail(), document.select("li:contains(Email)").first().text());
            assertEquals("User Comments: " + newComment, document.select("p:matches(User Comments)").first().text());
        }

        captureScreenshotlessStep();

        final IndexPage indexPage = loginPage.loadLoginPage().
                seleniumAdminLogin().
                loadAdminUsersPage().
                enableUser(newUser).
                logLogout().
                login(newUser);
        stepCounter.decrement();
        indexPage.assertLogin(newUser).logLogout();
    }

    @Test(groups = TestGroups.CLIENT_DEPLOYMENT)
    @JiraKey(simpleKey = "QA-114")
    @TestRequires(admin = true)
    public void testAdminEmail() {
        final MessageTemplate email = new MessageTemplate("adminEmailTemplate.txt");
        final String currentTime = email.usedTime();
        final String emailSubject = "Test of XNAT Admin email features";
        final String emailText = email.read();
        final User destinationUser = new User().username(RandomHelper.randomID()).password(RandomHelper.randomID()).email(Settings.EMAIL).firstName("SeleniumTest").lastName("UserEmail");

        loginPage.seleniumAdminLogin().
                loadAdminUsersPage().
                createUser(destinationUser).
                loadPage(AdminEmailPage.class).
                capture().
                addRecipient(destinationUser).
                capture().
                fillSubject(emailSubject).
                fillMessageBody(emailText).
                uploadScreenshot().
                clickSendEmailButton().
                confirmSendSuccess().
                logLogout();
        new EmailQuery()
                .containing(currentTime)
                .queryUntilSingleResult()
                .validateEach(message -> {
                    message.assertSubjectEquals(emailSubject);
                    message.assertEmailEquals(emailText.replace("\n\n", " "));
                });
        captureScreenshotlessStep();
    }

    private void requestAndCheckCredentials(User user) throws CredentialEmailLimitException {
        navigateToLoginPage().
                clickForgotCredentialsLink().
                capture().
                requestUsername(user.getEmail()).
                clickForgotCredentialsLink().
                capture().
                requestPassword(user.getUsername());

        new EmailQuery()
                .containing(forgotUsernameSearchTerm(user))
                .queryUntilSingleResult();
        final ParsedEmail passwordEmail = new EmailQuery()
                .containing(forgotPasswordSearchTerm(user))
                .queryUntilSingleResult()
                .getEmail();

        captureScreenshotlessStep();

        navigate().to(extractResetLink(passwordEmail));
        final String newPass = RandomHelper.randomString(14);
        final IndexPage indexPage = loadPage(PasswordResetPage.class).
                capture().
                resetPassword(newPass).
                login(user.password(newPass));
        stepCounter.decrement();
        indexPage.assertLogin(user).
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                toggleLoginRequired().
                logLogout().
                waitForLogout();
    }

    private String forgotUsernameSearchTerm(User user) {
        return "You requested your username, which is " + user.getUsername(); // ":" not searchable in gmail API
    }

    private List<String> forgotPasswordSearchTerm(User user) {
        return Arrays.asList(
                String.format("Dear %s %s", user.getFirstName(), user.getLastName()),
                "Please click this link to reset your password"
        );
    }

    private String extractResetLink(ParsedEmail passwordEmail) {
        if (XnatTestingVersionManager.testedVersionFollows(Xnat_1_7_6.class)) {
            return passwordEmail.extractSingleLink();
        } else {
            for (String word : StringUtils.split(passwordEmail.getEmailContent())) {
                if (word.startsWith("http://") || word.startsWith("https://")) {
                    return ParsedEmail.removeAmpersandEncoding(word);
                }
            }
            throw new AssertionError("Reset password link not found in email");
        }
    }

}
