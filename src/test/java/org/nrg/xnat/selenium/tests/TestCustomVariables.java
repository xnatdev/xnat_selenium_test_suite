package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.xnat.page_model.session.edit.ImagingSessionEditPage;
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage;
import org.nrg.selenium.xnat.page_model.subject.edit.SubjectEditPage;
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage;
import org.nrg.testing.TimeUtils;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.util.RandomHelper;
import org.nrg.xnat.enums.VariableType;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.custom_variable.CustomVariable;
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

public class TestCustomVariables extends BaseSeleniumTest {
    
    @BeforeClass
    public void addDataInstances() {
        mainInterface().createProject(new Project().addSubject(new Subject().addExperiment(new MRSession())));
    }

    @Test
    @JiraKey(simpleKey = "QA-154")
    public void testCustomSubjectVars() {
        // JIRA ticket: CNDA-175
        final Project project = new Project();
        final Subject subject = new Subject();
        final CustomVariableSet customVariableSet = new CustomVariableSet().dataType(DataType.SUBJECT).name(RandomHelper.randomString(8) + " " + RandomHelper.randomString(7)).description("Selenium automated subject custom variable").projectSpecific(true);
        compileFullCustomVariableList(customVariableSet);
        final Map<CustomVariable<?>, Object> variableValues = constructValidValues(customVariableSet);

        final SubjectReportPage subjectPage = loginPage.seleniumLogin().createProject(project).loadSubjectCreationPage().create(project, subject).searchForProject(project).loadDetailsTab().loadCustomVariableManagement().
                capture().createCustomVariableSet(customVariableSet).returnToProjectPage().clickSubjectLink(subject.getLabel());
        subjectPage.readCustomVariableSection().assertSetTitle(customVariableSet).captureCustomVariableSetTitle();
        final SubjectEditPage editPage = subjectPage.loadEditPage();
        editPage.readCustomVariableSection().populateCustomVariables(variableValues);
        editPage.submitForm();
        subjectPage.readCustomVariableSection().assertCustomVariableValues(variableValues).captureCustomVariableReportTable();
        subjectPage.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-155")
    public void testRequiredCustomVariables() {
        // tests that toggling the required flag on custom variables works as expected

        final Project project = new Project();
        final Subject subject = new Subject();
        final CustomVariableSet customVariableSet = new CustomVariableSet().dataType(DataType.SUBJECT).name(RandomHelper.randomString(8) + " " + RandomHelper.randomString(7)).description("Tests required flag on custom variables").projectSpecific(true);
        compilePartialCustomVariableList(customVariableSet, VariableType.values(), new boolean[]{true}, new boolean[]{true, false});
        // Custom variable list used is all types, only required, with and without expected values
        final Map<CustomVariable<?>, Object> variableValues = constructValidValues(customVariableSet);

        final SubjectReportPage subjectPage = loginPage.seleniumLogin().createProject(project).loadSubjectCreationPage().create(project, subject).searchForProject(project).loadDetailsTab().loadCustomVariableManagement().capture().
                createCustomVariableSet(customVariableSet).returnToProjectPage().clickSubjectLink(subject.getLabel());
        subjectPage.readCustomVariableSection().assertSetTitle(customVariableSet).captureCustomVariableSetTitle();
        final SubjectEditPage editPage = subjectPage.loadEditPage();
        editPage.submitForm();
        editPage.readCustomVariableSection().readRequiredVariableDialog().assertAllVariablesRequired(customVariableSet).closeAndReturn().populateCustomVariables(variableValues).submitForm();
        subjectPage.readCustomVariableSection().assertCustomVariableValues(variableValues).captureCustomVariableReportTable();
        subjectPage.logLogout();
    }

    @Test
    @JiraKey(simpleKey = "QA-156")
    public void testCustomMRVars() {
        final Project project = new Project();
        final Subject subject = new Subject();
        final CustomVariableSet customVariableSet = new CustomVariableSet().dataType(DataType.MR_SESSION).name(RandomHelper.randomID(16)).description("Uses automation").projectSpecific(true);
        compileFullCustomVariableList(customVariableSet);
        final Map<CustomVariable<?>, Object> variableValues = constructValidValues(customVariableSet);

        final ImagingSessionReportPage sessionPage = loginPage.seleniumLogin().createProject(project).loadSubjectCreationPage().create(project, subject).loadSessionCreationPage().create(project, subject, new MRSession()).logLogout().seleniumLogin().
                searchForProject(project).loadDetailsTab().loadCustomVariableManagement().capture().createCustomVariableSet(customVariableSet).returnToProjectPage().
                clickSubjectLink(subject.getLabel()).clickMrSessionLink();
        sessionPage.readCustomVariableSection().assertSetTitle(customVariableSet).captureCustomVariableSetTitle();
        final ImagingSessionEditPage editPage = sessionPage.loadEditPage();
        editPage.readCustomVariableSection().populateCustomVariables(variableValues);
        editPage.submitForm();
        sessionPage.readCustomVariableSection().assertCustomVariableValues(variableValues).captureCustomVariableReportTable();
        sessionPage.logLogout();
    }

    private void compileFullCustomVariableList(CustomVariableSet customVariableSet) {
        // Create a list of CustomVariable objects which represent a list of all possible custom variables
        compilePartialCustomVariableList(customVariableSet, VariableType.values(), new boolean[]{true, false}, new boolean[]{true, false});
    }

    private void compilePartialCustomVariableList(CustomVariableSet customVariableSet, VariableType[] types, boolean[] requiredArray, boolean[] hasRequiredValuesArray) {
        // Create a list of CustomVariable objects which represent a list of certain types of custom variables

        for (VariableType type : types) {
            for (boolean required : requiredArray) {
                for (boolean hasRequiredValues : hasRequiredValuesArray) {
                    CustomVariable<?> customVariable = new CustomVariable<>();

                    // dates and booleans cannot have required values
                    if (hasRequiredValues) {
                        switch (type) {
                            case INTEGER:
                                customVariable = new CustomVariable<Integer>().possibleValues(RandomHelper.randomIntegers(3, 0, 100, false));
                                break;
                            case FLOAT:
                                customVariable = new CustomVariable<Double>().possibleValues(Arrays.asList(RandomHelper.randomDouble(6), RandomHelper.randomDouble(6)));
                                break;
                            case STRING:
                                customVariable = new CustomVariable<String>().possibleValues(Arrays.asList(RandomHelper.randomString(10), RandomHelper.randomString(10)));
                                break;
                            default:
                        }
                    }
                    customVariableSet.getCustomVariables().add(customVariable.name(RandomHelper.randomString(10)).type(type).required(required));
                }
            }
        }
    }

    private Map<CustomVariable<?>, Object> constructValidValues(CustomVariableSet variableSet) {
        final Map<CustomVariable<?>, Object> values = new HashMap<>();
        for (CustomVariable<?> variable : variableSet.getCustomVariables()) {
            if (variable.getPossibleValues().isEmpty()) {
                switch (variable.getType()) {
                    case INTEGER:
                        values.put(variable, RandomHelper.randomInteger(-50, 50));
                        break;
                    case FLOAT:
                        values.put(variable, RandomHelper.randomDouble(5));
                        break;
                    case BOOLEAN:
                        values.put(variable, new Random().nextBoolean());
                        break;
                    case DATE:
                        values.put(variable, TimeUtils.date(RandomHelper.randomInteger(2010, 2014), RandomHelper.randomInteger(10, 12), RandomHelper.randomInteger(10, 28)));
                        break;
                    case STRING:
                        values.put(variable, RandomHelper.randomString(8));
                        break;
                }
            } else {
                values.put(variable, RandomHelper.randomListEntry(new ArrayList<Object>(variable.getPossibleValues())));
            }
        }
        return values;
    }

}
