package org.nrg.xnat.selenium.tests;

import org.apache.commons.io.FileUtils;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.xnat.page_model.session.edit.ImagingSessionEditPage;
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage;
import org.nrg.testing.annotations.Basic;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestOtherUploads extends BaseSeleniumTest {

    private final Project project = new Project();
    private final Subject subject = new Subject(project);

    @BeforeClass
    public void setupProject() {
        mainInterface().createProject(project);
    }

    @Test
    @Basic
    @JiraKey(simpleKey = "QA-166")
    public void testUploadXML() {
        final int xmlVisitReplacementIndex = 1;
        final String visit = "v00";
        final String sessionXml = Settings.TEMP_SUBDIR + File.separator + "session.XML";
        final File xmlFile = new File(sessionXml);

        final ImagingSessionReportPage sessionReportPage = loginPage.seleniumLogin().
                loadPage(ImagingSessionEditPage.class).
                create(project, subject, new MRSession());
        final File downloadedXml = sessionReportPage.downloadXml();
        captureScreenshotlessStep();

        try {
            final List<String> xmlAsLines = FileUtils.readLines(downloadedXml, "utf-8");
            final String lineWithVisit = xmlAsLines.get(xmlVisitReplacementIndex).replace("label", String.format("visit_id=\"%s\" label", visit));
            xmlAsLines.remove(xmlVisitReplacementIndex);
            xmlAsLines.add(xmlVisitReplacementIndex, lineWithVisit);
            FileUtils.writeLines(new File(sessionXml), "utf-8", xmlAsLines);
        } catch (IOException ioe) {
            throw new RuntimeException("Error in handling XML", ioe);
        }
        captureScreenshotlessStep();

        sessionReportPage.loadXmlUploadPage().
                markAllowDataDeletionAs(false).
                attachFile(xmlFile).
                uploadScreenshot().
                uploadAsSession().
                assertVisit(visit).
                capture().
                logLogout();
    }

}
