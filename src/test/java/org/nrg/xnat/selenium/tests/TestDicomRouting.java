package org.nrg.xnat.selenium.tests;

import org.dcm4che3.data.Tag;
import org.nrg.jira.components.zephyr.TestStatus;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.testing.DicomUtils;
import org.nrg.testing.annotations.*;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.nrg.xnat.versions.Xnat_1_8_0;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

@TestRequires(dicomScp = true, data = TestData.SAMPLE_1)
@AddedIn(Xnat_1_8_0.class)
public class TestDicomRouting extends BaseSeleniumTest {

    private static final TestData TEST_DATA = TestData.SAMPLE_1;
    private static final Project RULES_PROJECT = new Project("rules_" + RandomHelper.randomInteger(100000, 999999));
    private static final Project FIRST_PASS_PROJECT = new Project("pass1_" + RandomHelper.randomID(6));
    private static final Project SECOND_PASS_PROJECT = new Project("pass2_" + RandomHelper.randomID(6));
    private static final Project THIRD_PASS_PROJECT = new Project("pass3_" + RandomHelper.randomID(6));
    private static final Project FOURTH_PASS_PROJECT = new Project("pass4_" + RandomHelper.randomID(6));

    private static final String RULES_SUBJECT = "rules_" + RandomHelper.randomID(6);
    private static final String RULES_SESSION = "rules_" + RandomHelper.randomID(6);
    private static final String FIRST_PASS_SUBJECT = "pass1_" + RandomHelper.randomID(6);
    private static final String FIRST_PASS_SESSION = "pass1_" + RandomHelper.randomID(6);
    private static final String SECOND_PASS_SUBJECT = "pass2_" + RandomHelper.randomID(6);
    private static final String SECOND_PASS_SESSION = "pass2_" + RandomHelper.randomID(6);
    private static final String THIRD_PASS_SUBJECT = "pass3_" + RandomHelper.randomID(6);
    private static final String THIRD_PASS_SESSION = "pass3_" + RandomHelper.randomID(6);
    private static final String FOURTH_PASS_SUBJECT = "pass4_" + RandomHelper.randomID(6);
    private static final String FOURTH_PASS_SESSION = "pass4_" + RandomHelper.randomID(6);

    private boolean routingModified = false;

    private void shipDicom(Map<Integer, String> overwrittenHeaders) {
        new XnatCStore(Settings.DEFAULT_RECEIVER).data(TEST_DATA).overwrittenHeaders(overwrittenHeaders).sendDICOM();
        captureStep(stepCounter, TestStatus.PASS, "Overwritten headers: " + overwrittenHeaders, false);
    }

    @Test
    @TestRequires(ssh = true)
    @JiraKey(simpleKey = "QA-311")
    public void testSetupDicomRouting() {
        final int customProjectRoutingTag = Tag.StationName;
        final int customSubjectRoutingTag = Tag.MilitaryRank;
        final int customSessionRoutingTag = Tag.BranchOfService;
        final String fullContentRegex = "(.*)";

        loginPage.seleniumLogin().
                createProject(RULES_PROJECT).
                createProject(FIRST_PASS_PROJECT).
                createProject(SECOND_PASS_PROJECT).
                createProject(THIRD_PASS_PROJECT).
                createProject(FOURTH_PASS_PROJECT).
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                disableSiteAnonScript().
                logLogout();
        routingModified = true;
        mainAdminInterface().setProjectDicomRoutingConfig(formRoutingString(customProjectRoutingTag, "(?:[^\\^]+)\\^((?:[a-zA-Z\\d]+)[_\\-](?:\\d+))(.*?)"));
        mainAdminInterface().setSubjectDicomRoutingConfig(formRoutingString(customSubjectRoutingTag, fullContentRegex));
        mainAdminInterface().setSessionDicomRoutingConfig(formRoutingString(customSessionRoutingTag, fullContentRegex));
        captureScreenshotlessStep();

        // Send data 5 times with different headers to check all 5 passes. Do this all at once, and allow autoarchiving.

        final Map<Integer, String> overwrittenHeaders = new HashMap<>();

        overwrittenHeaders.put(Tag.AccessionNumber, FOURTH_PASS_PROJECT.getId());
        overwrittenHeaders.put(Tag.PatientName, FOURTH_PASS_SUBJECT);
        overwrittenHeaders.put(Tag.PatientID, FOURTH_PASS_SESSION);
        shipDicom(overwrittenHeaders);

        overwrittenHeaders.put(Tag.StudyDescription, THIRD_PASS_PROJECT.getId());
        overwrittenHeaders.put(Tag.PatientName, THIRD_PASS_SUBJECT);
        overwrittenHeaders.put(Tag.PatientID, THIRD_PASS_SESSION);
        shipDicom(overwrittenHeaders);

        overwrittenHeaders.put(Tag.StudyComments, String.format("Project:%s Subject:%s Session:%s", SECOND_PASS_PROJECT, SECOND_PASS_SUBJECT, SECOND_PASS_SESSION));
        shipDicom(overwrittenHeaders);

        overwrittenHeaders.put(Tag.PatientComments, String.format("Project:%s Subject:%s Session:%s", FIRST_PASS_PROJECT, FIRST_PASS_SUBJECT, FIRST_PASS_SESSION));
        shipDicom(overwrittenHeaders);

        overwrittenHeaders.put(customProjectRoutingTag, "XNAT^" + RULES_PROJECT);
        overwrittenHeaders.put(customSubjectRoutingTag, RULES_SUBJECT);
        overwrittenHeaders.put(customSessionRoutingTag, RULES_SESSION);
        shipDicom(overwrittenHeaders);

        waitForSessionRebuilder();
    }

    @Test
    @HardDependency("testSetupDicomRouting")
    @JiraKey(simpleKey = "QA-288")
    public void testFirstPass() {
        testDicomRouting(FIRST_PASS_PROJECT, FIRST_PASS_SUBJECT, FIRST_PASS_SESSION);
    }

    @Test
    @HardDependency("testSetupDicomRouting")
    @JiraKey(simpleKey = "QA-289")
    public void testSecondPass() {
        testDicomRouting(SECOND_PASS_PROJECT, SECOND_PASS_SUBJECT, SECOND_PASS_SESSION);
    }

    @Test
    @HardDependency("testSetupDicomRouting")
    @JiraKey(simpleKey = "QA-290")
    public void testThirdPass() {
        testDicomRouting(THIRD_PASS_PROJECT, THIRD_PASS_SUBJECT, THIRD_PASS_SESSION);
    }

    @Test
    @HardDependency("testSetupDicomRouting")
    @JiraKey(simpleKey = "QA-291")
    public void testFourthPass() {
        testDicomRouting(FOURTH_PASS_PROJECT, FOURTH_PASS_SUBJECT, FOURTH_PASS_SESSION);
    }

    @Test
    @HardDependency("testSetupDicomRouting")
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-312")
    public void testProjectRules() {
        testDicomRouting(RULES_PROJECT, RULES_SUBJECT, RULES_SESSION);
    }

    @AfterClass(alwaysRun = true)
    public void disableCustomRouting() {
        if (routingModified) {
            mainAdminInterface().disableProjectDicomRoutingConfig();
            mainAdminInterface().disableSubjectDicomRoutingConfig();
            mainAdminInterface().disableSessionDicomRoutingConfig();
        }
    }

    private void testDicomRouting(Project expectedProject, String expectedSubject, String expectedSession) {
        loginPage.seleniumLogin().
                captureRecentSession(expectedProject, 300).
                searchForSession(expectedSession).
                getScanTable().
                assertFilesRepresentSample1().
                capture().
                getUnderlyingPage().
                assertAutoRunCompletion().
                assertSnapshotGeneration().
                readBreadcrumbs().
                clickSubjectBreadcrumb().
                assertPageRepresents(new Subject(expectedProject, expectedSubject)).
                readBreadcrumbs().
                clickProjectBreadcrumb().
                assertPageRepresents(expectedProject).
                logLogout();
    }

    private String formRoutingString(int header, String regex) {
        return String.format("%s:%s", DicomUtils.intToFullHexString(header), regex);
    }

}
