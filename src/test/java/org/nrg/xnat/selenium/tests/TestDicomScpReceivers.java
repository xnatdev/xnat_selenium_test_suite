package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.xnat.page_model.SyntheticPageObject;
import org.nrg.selenium.xnat.page_model.admin.ScpReceiverManagementPage;
import org.nrg.testing.TimeUtils;
import org.nrg.testing.annotations.DisallowXnatVersion;
import org.nrg.testing.annotations.HardDependency;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.dicom.DicomScpReceiver;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.fail;

@TestRequires(data = TestData.SAMPLE_1)
@DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
public class TestDicomScpReceivers extends BaseSeleniumTest {

    private final TestData TEST_DATA = TestData.SAMPLE_1;
    private final Project SCP_PROJECT = new Project("SCPTEST_" + RandomHelper.randomID(5));
    private final int TEST_PORT = 8105;
    private final int TEST_PORT_2 = 8106;
    private final String INITIAL_AE_TITLE = RandomHelper.randomID(5);
    private final String NEW_AE_TITLE = RandomHelper.randomID(5);
    private final DicomScpReceiver RECEIVER = receiver().aeTitle(INITIAL_AE_TITLE).port(TEST_PORT);

    @BeforeClass
    public void initTests() {
        constructXnatDriver();
        mainAdminQueryBase().get(formatXapiUrl("dicomscp")).then().assertThat().statusCode(200);
        mainAdminInterface().createProject(SCP_PROJECT);
    }

    @Test
    @JiraKey(simpleKey = "QA-313")
    public void testAddDicomScpReceiver() {
        loginPage.seleniumAdminLogin().loadScpReceiverManagementPage().capture().deleteAllReceiversOnPort(TEST_PORT).deleteAllReceiversOnPort(TEST_PORT_2).capture().createScpReceiver(RECEIVER);

        TimeUtils.sleep(1000);
        shipDicom(RECEIVER);
        captureScreenshotlessStep();
        handleData();
    }

    @Test
    @HardDependency("testAddDicomScpReceiver")
    @JiraKey(simpleKey = "QA-314")
    public void testEditDicomScpReceiver() {
        final ScpReceiverManagementPage receiverTable = loginPage.seleniumAdminLogin().loadScpReceiverManagementPage();
        assertNotNull(receiverTable.findReceiverOnPort(TEST_PORT));
        receiverTable.capture().launchEditDialog(RECEIVER).assertAeTitle(INITIAL_AE_TITLE).assertPort(TEST_PORT).fillAeTitle(NEW_AE_TITLE).fillPort(TEST_PORT_2).submitDialog();
        receiverTable.validateAndCaptureReceiver(RECEIVER.aeTitle(NEW_AE_TITLE).port(TEST_PORT_2)).assertAeTitleNotPresent(INITIAL_AE_TITLE);
        captureScreenshotlessStep();
        TimeUtils.sleep(1000);
        try {
            testSendToBadReceiver(receiver().aeTitle(INITIAL_AE_TITLE).port(TEST_PORT));
        } catch (Error e) {
            testController.failBecause("DICOM SCP Receiver not deactivated on previous port after edit", "XNAT-5141");
        }
        captureScreenshotlessStep();
        receiverTable.searchForProject(SCP_PROJECT).loadProjectPrearchive().assertPrearchiveEmpty().capture();
        shipDicom(RECEIVER);
        captureScreenshotlessStep();
        handleData();
    }

    @Test
    @HardDependency("testEditDicomScpReceiver")
    @JiraKey(simpleKey = "QA-315")
    public void testEnablingDicomScpReceiver() {
        final ScpReceiverManagementPage receiverTable = loginPage.seleniumAdminLogin().loadScpReceiverManagementPage().assertReceiverFields(RECEIVER).capture().disableReceiver(RECEIVER).capture();
        TimeUtils.sleep(1000);
        testSendToBadReceiver(RECEIVER);
        captureScreenshotlessStep();
        receiverTable.enableReceiver(RECEIVER).capture();
        TimeUtils.sleep(1000);
        shipDicom(RECEIVER);
        captureScreenshotlessStep();
        handleData();
    }

    @Test
    @HardDependency("testEnablingDicomScpReceiver")
    @JiraKey(simpleKey = "QA-316")
    public void testDeletingDicomScpReceiver() {
        final ScpReceiverManagementPage receiverTable = loginPage.seleniumAdminLogin().loadScpReceiverManagementPage().assertReceiverFields(RECEIVER).capture().deleteReceiver(RECEIVER).capture();
        TimeUtils.sleep(1000);
        testSendToBadReceiver(RECEIVER);
        captureScreenshotlessStep();
        receiverTable.searchForProject(SCP_PROJECT).loadProjectPrearchive().assertPrearchiveEmpty().capture().logLogout();
    }

    private DicomScpReceiver receiver() {
        return new DicomScpReceiver().host(Settings.DICOM_HOST);
    }

    private void handleData() {
        new SyntheticPageObject(getXnatDriver()).searchForProject(SCP_PROJECT).loadProjectPrearchive().findOnlySessionInProject(SCP_PROJECT).rebuildAndArchive().capture();
        home().captureRecentSession(SCP_PROJECT).clickRecentSession(SCP_PROJECT).capture().assertAutoRunCompletion().assertSnapshotGeneration().searchForProject(SCP_PROJECT).loadDeleteComponent().clearProjectArchive().logLogout();
    }

    private void shipDicom(DicomScpReceiver receiver) {
        new XnatCStore(receiver).data(TEST_DATA).sendDICOMToProject(SCP_PROJECT);
    }

    private void testSendToBadReceiver(DicomScpReceiver receiver) {
        final String failureString = String.format("DICOM send to receiver %s reported no issues, but it should have been invalid.", receiver);
        try {
            shipDicom(receiver);
        } catch (Exception | AssertionError ignored) {
            return; // The send was unhappy, as it should have been
        }
        fail(failureString); // If we got this far, no exception was caught earlier
    }

}
