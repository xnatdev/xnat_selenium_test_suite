package org.nrg.xnat.selenium.tests;

import au.com.bytecode.opencsv.CSVWriter;
import org.nrg.jira.components.zephyr.TestStatus;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.DataUnit;
import org.nrg.selenium.util.ResourceValidation;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.project.edit.ProjectEditPage;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.selenium.xnat.page_model.session.edit.ReassignSessionToSubjectDialog;
import org.nrg.selenium.xnat.page_model.subject.edit.SubjectEditPage;
import org.nrg.selenium.xnat.page_model.subject.report.SubjectReportPage;
import org.nrg.selenium.xnat.page_model.ui_element.YuiTable;
import org.nrg.selenium.xnat.page_model.upload.SpreadsheetUploadPreview;
import org.nrg.selenium.xnat.page_model.upload.SpreadsheetUploadResultPage;
import org.nrg.selenium.xnat.page_model.upload.SpreadsheetUploadReviewPage;
import org.nrg.testing.TestNgUtils;
import org.nrg.testing.annotations.*;
import org.nrg.testing.util.RandomHelper;
import org.nrg.xnat.enums.Accessibility;
import org.nrg.xnat.enums.Gender;
import org.nrg.xnat.enums.Handedness;
import org.nrg.xnat.pogo.DataType;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

@Basic
public class TestDataInteraction extends BaseSeleniumTest {

    private final String projectTitle = RandomHelper.randomID(11).toLowerCase();
    private final String projectRunningTitle = RandomHelper.randomID(11).toLowerCase();
    private final String projectId = RandomHelper.randomID(11).toLowerCase();
    // Lowercase is until XNAT-3075 is fixed.
    private final String uniqueString = projectId + "00";
    private final List<String> projectDetails = new ArrayList<>(Arrays.asList(projectTitle, projectRunningTitle, projectId));
    private final Project project = new Project(projectId).title(projectTitle).runningTitle(projectRunningTitle);
    private final Subject subject = new Subject(project);
    private final int waitTolerance = 30;

    @Test
    @JiraKey(simpleKey = "QA-94")
    public void testCreateProject() {
        final String description = "Automated testing";
        final String keywords = "automation";
        project.description(description).keywords(Collections.singletonList(keywords)).accessibility(Accessibility.PRIVATE);

        final ProjectEditPage createPage = loginPage.seleniumLogin().loadPage(ProjectEditPage.class).create(project, true).capture().loadPage(ProjectEditPage.class).captureDetailsTable();
        for (String projectTerm : projectDetails) {
            createPage.createProjectWithExpectedFailure(new Project(uniqueString).title(projectTerm)).assertTextEquals("Invalid Project Title: '" + projectTerm + "' is already being used.").uploadScreenshot();
            createPage.createProjectWithExpectedFailure(new Project(uniqueString).runningTitle(projectTerm)).assertTextEquals("Invalid Running Title: '" + projectTerm + "' is already being used.").uploadScreenshot();
            createPage.createProjectWithExpectedFailure(new Project(projectTerm).runningTitle(uniqueString).title(uniqueString)).assertTextEquals(
                    projectTerm.equals(projectId) ?
                            "Project '" + projectTerm + "' already exists." :
                            "Invalid Project Id: '" + projectTerm + "' is already being used."
            ).capture();
        }
        createPage.logLogout();
    }

    @Test
    @HardDependency("testCreateProject")
    @JiraKey(simpleKey = "QA-95")
    public void testAddSubject() {
        final String group = "Abelian";
        final int yob = 1950;
        final Gender gender = Gender.MALE;
        final Handedness handedness = Handedness.RIGHT;
        final int education = 12;
        final String race = "silicon";
        final String ethnicity = "robot";
        final int height = 45;
        final int weight = 850;
        final String source = "Internet";
        subject.group(group).yob(yob).gender(gender).handedness(handedness).education(education).race(race).ethnicity(ethnicity).height(height).weight(weight).src(source);

        final SubjectReportPage subjectPage = loginPage.seleniumLogin().loadPage(SubjectEditPage.class).create(subject);
        assertSubjectFields(subjectPage, subject).logLogout();
    }

    @Test
    @HardDependency("testCreateProject")
    @JiraKey(simpleKey = "QA-96")
    public void testUploadSubjectSpreadsheet() throws IOException {
        final File csvFile = Files.createTempFile("subjectcsv", "csv").toFile();
        final DataType dataType = DataType.SUBJECT;
        final int numSubjects = 3;
        final String fieldExtension = "demographics";
        final String extensionDataType = "xnat:demographicData";
        final List<String> fields = Arrays.asList("age", "gender", "handedness", "education");
        final List<List<String>> csvContents = new ArrayList<>();
        final List<String> headers = new ArrayList<>(fields);
        final List<List<String>> csvDataContent = new ArrayList<>();
        headers.add(0, "ID");
        csvContents.add(headers);
        for (int i = 0; i < numSubjects; i++) {
            final List<String> dataRow = Arrays.asList(RandomHelper.randomID(10), Integer.toString(RandomHelper.randomInteger(20, 80)), RandomHelper.randomGender(), RandomHelper.randomHandedness(), Integer.toString(RandomHelper.randomInteger(12, 20)));
            csvContents.add(dataRow);
            csvDataContent.add(dataRow);
        }
        final CSVWriter writer = new CSVWriter(new FileWriter(csvFile), ',', CSVWriter.NO_QUOTE_CHARACTER);
        for (List<String> line : csvContents) {
            writer.writeNext(line.toArray(new String[]{}));
        }
        writer.close();
        captureScreenshotlessStep();

        final SpreadsheetUploadPreview preview = loginPage.seleniumLogin().loadSpreadsheetUploader().capture().fillTitle("Test Spreadsheet Upload").selectDataType(dataType).capture().submit().
                capture().selectRelativeFieldExtension(fieldExtension, extensionDataType).capture().checkRelativeFieldExtensionBoxes(fieldExtension, fields).capture().submit().
                capture().uploadCsvFile(csvFile).readPreviewTable().assertNumberOfDataRows(numSubjects).capture();
        for (int i = 0; i < csvDataContent.size(); i++) {
            final List<String> csvRow = csvDataContent.get(i);
            for (int j = 0; j < csvRow.size(); j++) {
                preview.assertSpecificDataCell(j + 1, i, csvRow.get(j));
            }
        }
        final SpreadsheetUploadReviewPage review = preview.capture().returnToUnderlyingPage().selectProject(project).captureProject().submit().capture().assertNumberOfDataRows(numSubjects);
        for (int i = 0; i < csvDataContent.size(); i++) {
            final List<String> dataRow = new ArrayList<>(csvDataContent.get(i));
            dataRow.add("NEW");
            review.assertDataRowEquals(i, dataRow);
        }
        final SpreadsheetUploadResultPage results = review.capture().submit().capture();
        for (int i = 0; i < csvDataContent.size(); i++) {
            final List<String> dataRow = new ArrayList<>(csvDataContent.get(i));
            dataRow.add("Successful");
            results.assertDataRowEquals(i, dataRow);
        }
        results.capture();
        for (List<String> subjectRow : csvDataContent) {
            final SubjectReportPage subjectPage = results.searchForSubject(subjectRow.get(0));
            assertEquals("Subject Details: " + subjectRow.get(0), subjectPage.readSubjectDetails());
            assertEquals(subjectRow.get(2), subjectPage.readGenderDisplay());
            assertEquals(subjectRow.get(3), subjectPage.readHandednessDisplay());
            assertEquals(subjectRow.get(4), String.valueOf(subjectPage.readEducation()));
            subjectPage.uploadScreenshot();
        }
        captureScreenshotlessStep();
        results.logLogout();
    }

    @Test
    @HardDependency("testCreateProject")
    @JiraKey(simpleKey = "QA-391", versionMap = {
            @XnatVersionLink(xnatVersions = Xnat_1_6dev.class, mappedValue = "QA-97")
    })
    public void testCompressedUpload() {
        // This test will also set up notifications, but not check them, just to make sure XNAT-2985 is still fixed
        final int maximumAllowedUploadTime = 120;

        loginPage.seleniumLogin().searchForProject(projectId).capture().loadManageTab().capture().getProjectPage().loadCompressedUploader().
                uploadToArchive(project, getDataFile("seleniumSampleA.zip"), maximumAllowedUploadTime, true).
                assertScanResourcesRepresent(4, new ResourceValidation(187.5, DataUnit.KILOBYTES, 1)).
                assertScanResourcesRepresent(5, new ResourceValidation(186.9, DataUnit.KILOBYTES, 1)).
                assertScanResourcesRepresent(6, new ResourceValidation(188.5, DataUnit.KILOBYTES, 1)).
                assertSummedScanResourcesRepresent(new ResourceValidation(562.8, DataUnit.KILOBYTES, 3)).
                capture().assertAutoRunCompletion(true).assertSnapshotGeneration().logLogout();
    }

    @Test
    @HardDependency("testAddSubject")
    @JiraKey(simpleKey = "QA-98")
    public void testEditSession() {
        final int numSubjects = 5;
        final String originalSessionName = RandomHelper.randomID(16);
        final String newSessionName = RandomHelper.randomID(15);

        final IndexPage navigablePageStub = loginPage.seleniumLogin();
        for (int i = 0; i < numSubjects; i++) {
            navigablePageStub.loadSubjectCreationPage().create(new Subject(project));
            stepCounter.decrement();
        }
        stepCounter.increment();
        final ReassignSessionToSubjectDialog dialog = navigablePageStub.loadSessionCreationPage().create(new MRSession(project, subject, originalSessionName)).searchForProject(projectId).
                clickSubjectLink(subject.getLabel()).clickSessionLink(originalSessionName).capture().loadEditPage().captureSimpleSummary().loadAssignToSubjectDialog().capture();
        final List<String> subjectOptions = dialog.readSubjectOptions();
        TestNgUtils.assertCaseInsensitiveAlphabeticalOrder(subjectOptions);
        captureStep(stepCounter, TestStatus.PASS, subjectOptions.size() + " subjects asserted to be in alphabetical order.", false);
        dialog.returnToUnderlyingPage().captureSimpleSummary().modifySessionLabel(newSessionName, true).clickBackButton().capture().assertPageRepresents(new MRSession(newSessionName)).logLogout();
    }

    @Test
    @HardDependency("testEditSession")
    @JiraKey(simpleKey = "QA-99")
    public void testDeleteSession() {
        loginPage.seleniumLogin().searchForProject(projectId).clickSubjectLink(subject.getLabel()).clickMrSessionLink().capture().
                loadDeleteComponent().delete(waitTolerance, true).clickSubjectLink(subject.getLabel()).
                readExperimentTable().assertNoExperiments().returnToUnderlyingPage().capture().logLogout();
    }

    @Test
    @HardDependency("testAddSubject")
    @JiraKey(simpleKey = "QA-100")
    public void testEditSubject() {
        final int yob = 1964;
        final Gender gender = Gender.FEMALE;
        final Handedness handedness = Handedness.AMBIDEXTROUS;
        final int education = 20;
        final String race = "Zerg";
        final String ethnicity = "Not Latino";
        final int height = 65;
        final int weight = 130;
        final String group = "group";
        final String newLabel = RandomHelper.randomString(10);
        subject.group(group).yob(yob).gender(gender).handedness(handedness).education(education).race(race).ethnicity(ethnicity).height(height).weight(weight);

        final SubjectEditPage editPage = loginPage.seleniumLogin().searchForProject(projectId).clickSubjectLink(subject.getLabel()).capture().loadEditPage().capture().
                modifySubjectLabel(newLabel);
        subject.setLabel(newLabel);
        editPage.fillWeight("thin").submitForm();
        editPage.readError().assertTextEquals("Invalid parameters:\nxnat:subjectData/demographics/weight : Must Be A Valid Float").capture();
        editPage.fillMetaFields(subject).capture().submitForm();
        assertSubjectFields(
                editPage.loadPage(SubjectReportPage.class).assertSubjectDetailsRepresentSubject(newLabel),
                subject
        ).logLogout();
    }

    @Test
    @HardDependency({"testEditSession", "testEditSubject"})
    @SoftDependency("testDeleteSession")
    @JiraKey(simpleKey = "QA-101")
    public void testDeleteSubject() {
        final ProjectReportPage projectPage = loginPage.seleniumLogin().searchForProject(projectId).clickSubjectLink(subject.getLabel()).capture().loadDeleteComponent().delete(waitTolerance, true);
        final YuiTable projectDataTable = projectPage.loadProjectDataTable();
        assertFalse(projectDataTable.readEntriesForColumn("Subject").contains(subject.getLabel()));
        projectDataTable.capture();
        projectPage.logLogout();
    }

    @Test
    @HardDependency("testCreateProject")
    @JiraKey(simpleKey = "QA-102")
    public void testEditProject() {
        final String editProjectTitle = RandomHelper.randomID(16).toLowerCase(); // LowerCase is until XNAT-3075 is fixed.
        final String projectDescription = RandomHelper.randomID(50);
        final List<String> projectKeywords = Arrays.asList(RandomHelper.randomString(10), RandomHelper.randomString(12));
        final String alias0 = RandomHelper.randomID(9);
        final String alias1 = RandomHelper.randomID(9);
        final String alias2 = RandomHelper.randomID(9);
        final List<String> aliases = new ArrayList<>(Arrays.asList(alias0, alias1, alias2));
        final Accessibility accessibility = Accessibility.PROTECTED;

        final ProjectEditPage originalProjectEditPage = loginPage.seleniumLogin().searchForProject(projectId).capture().loadEditPage().capture().
                fillProjectTitle(editProjectTitle).fillProjectDescription(projectDescription).fillKeywords(projectKeywords).fillAliases(aliases).setProjectAccessibility(accessibility);
        commentStep(String.format("title = %s, description = %s, alias1 = %s, alias2 = %s, alias3 = %s, keywords = %s", editProjectTitle, projectDescription, alias0, alias1, alias2, projectKeywords));
        originalProjectEditPage.capture().submitForm();
        project.title(editProjectTitle).description(projectDescription).keywords(projectKeywords).aliases(aliases);
        final ProjectEditPage throwawayProjectEditPage = originalProjectEditPage.loadPage(ProjectReportPage.class).assertPageRepresents(project).loadAccessTab().assertAccessibility(accessibility).capture().
                getProjectPage().createProject(new Project()).loadEditPage().capture();
        for (String projectTerm : Arrays.asList(editProjectTitle, projectRunningTitle, projectId)) {
            throwawayProjectEditPage.fillProjectTitle(projectTerm).fillRunningTitle(uniqueString).submitForm();
            throwawayProjectEditPage.readAlert().assertTextEquals("Invalid Project Title: '" + projectTerm + "' is already being used.").uploadScreenshot();
            throwawayProjectEditPage.fillProjectTitle(uniqueString).fillRunningTitle(projectTerm).submitForm();
            throwawayProjectEditPage.readAlert().assertTextEquals("Invalid Running Title: '" + projectTerm + "' is already being used.").capture();
        }
        throwawayProjectEditPage.logLogout();
    }

    @Test
    @HardDependency("testCreateProject")
    @SoftDependency({"testUploadSubjectSpreadsheet", "testCompressedUpload", "testDeleteSubject", "testEditProject"})
    @JiraKey(simpleKey = "QA-103")
    public void testDeleteProject() {
        loginPage.seleniumLogin().searchForProject(projectId).capture().loadDeleteComponent().delete(2 * waitTolerance, true).verifyNoSearchResultsFound(projectId).logLogout();
    }

    private SubjectReportPage assertSubjectFields(SubjectReportPage subjectPage, Subject subject) {
        assertEquals(subject.getYob(), subjectPage.readBirthYear());
        assertEquals(subject.getGender().capitalize(), subjectPage.readGenderDisplay());
        assertEquals(subject.getHandedness().capitalize(), subjectPage.readHandednessDisplay());
        assertEquals(subject.getEducation(), subjectPage.readEducation());
        assertEquals(subject.getRace(), subjectPage.readRace());
        assertEquals(subject.getEthnicity(), subjectPage.readEthnicity());
        assertEquals(subject.getHeight() + ".0", subjectPage.readHeight());
        assertEquals(subject.getWeight() + ".0", subjectPage.readWeight());
        assertEquals(subject.getGroup(), subjectPage.readGroup());
        assertEquals(subject.getSrc(), subjectPage.readSource());
        return subjectPage.capture();
    }

}