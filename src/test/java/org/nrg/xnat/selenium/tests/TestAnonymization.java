package org.nrg.xnat.selenium.tests;

import org.apache.log4j.Logger;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.enums.PrearchiveStatus;
import org.nrg.selenium.xnat.page_model.dicom.DicomDumpPage;
import org.nrg.selenium.xnat.page_model.prearchive.PrearchiveDetails;
import org.nrg.selenium.xnat.page_model.project.report.ProjectReportPage;
import org.nrg.selenium.xnat.page_model.session.report.ImagingSessionReportPage;
import org.nrg.testing.TestNgUtils;
import org.nrg.testing.annotations.DisallowXnatVersion;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.dicom.RootDicomObject;
import org.nrg.testing.dicom.XnatCStore;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.XnatObjectUtils;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.xnat.enums.DicomEditVersion;
import org.nrg.xnat.pogo.AnonScript;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension;
import org.nrg.xnat.versions.Xnat_1_6dev;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.dcm4che3.data.Tag.*;

@TestRequires(data = TestData.ANON_SESSION)
public class TestAnonymization extends BaseSeleniumTest {

    private final TestData ANON_TEST_DATA = TestData.ANON_SESSION;
    private final String ANON_SESSION_NAME = "anonSession";
    private final String SITE_SCRIPT_NAME = "siteAnonScript.das";
    private final String PROJECT_SCRIPT_NAME = "projAnonScript.das";
    private final String BONUS_SCRIPT_NAME = "bonusScript.das";

    private final AnonScript siteScriptDE4 = XnatObjectUtils.anonScriptFromFile(DicomEditVersion.DE_4, SITE_SCRIPT_NAME);
    private final AnonScript siteScriptDE6 = XnatObjectUtils.anonScriptFromFile(DicomEditVersion.DE_6, SITE_SCRIPT_NAME);
    private final AnonScript projectScriptDE4 = XnatObjectUtils.anonScriptFromFile(DicomEditVersion.DE_4, PROJECT_SCRIPT_NAME);
    private final AnonScript projectScriptDE6 = XnatObjectUtils.anonScriptFromFile(DicomEditVersion.DE_6, PROJECT_SCRIPT_NAME);
    private final AnonScript bonusScriptDE4 = XnatObjectUtils.anonScriptFromFile(DicomEditVersion.DE_4, BONUS_SCRIPT_NAME);
    private final AnonScript bonusScriptDE6 = XnatObjectUtils.anonScriptFromFile(DicomEditVersion.DE_6, BONUS_SCRIPT_NAME);
    private AnonScript defaultSiteScript;

    private final RootDicomObject dicomWithoutAnon = addProjectAnonCheck(false, addSiteAnonCheck(false, new RootDicomObject()));
    private final RootDicomObject projectAnonymizedDicom = addProjectAnonCheck(true, addSiteAnonCheck(false, new RootDicomObject()));
    private final Logger logger = Logger.getLogger(TestAnonymization.class);

    @BeforeClass
    public void initAnonTests() {
        checkDicomReceiverInfo();
        constructXnatDriver(true, Settings.DEFAULT_XNAT_CONFIG);
        defaultSiteScript = restDriver.getDefaultXnatAnonScript();
        mainAdminInterface().enablePostArchiveAnon();
    }

    private void checkDicomReceiverInfo() {
        TestNgUtils.assumeTrue(Settings.HAS_DICOM_RECEIVER_INFO, "No DICOM Receiver information available for any of the tests in class: " + this.getClass().getSimpleName());
    }

    @AfterClass(alwaysRun = true)
    public void disableScript() {
        restDriver.interfaceFor(mainAdminUser).disableSiteAnonScript();
        logger.info("Site anonymization script has been disabled.");
    }

    @Test
    @JiraKey(simpleKey = "QA-465")
    public void testProjectChangeAnon_DE4() {
        runProjectChangeTest(projectScriptDE4, siteScriptDE4);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-475")
    public void testProjectChangeAnon_DE6() {
        runProjectChangeTest(projectScriptDE6, siteScriptDE6);
    }

    @Test
    @JiraKey(simpleKey = "QA-466")
    public void testSubjectReassignAnon_DE4() {
        runSubjectReassignAnonTest(projectScriptDE4, siteScriptDE4);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-476")
    public void testSubjectReassignAnon_DE6() {
        runSubjectReassignAnonTest(projectScriptDE6, siteScriptDE6);
    }

    @Test
    @JiraKey(simpleKey = "QA-467")
    public void testSessionLabelChangeAnon_DE4() {
        runSessionLabelChangeAnonTest(projectScriptDE4, siteScriptDE4);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-477")
    public void testSessionLabelChangeAnon_DE6() {
        runSessionLabelChangeAnonTest(projectScriptDE6, siteScriptDE6);
    }

    @Test
    @JiraKey(simpleKey = "QA-468")
    public void testUploadAndArchiveAnon_DE4() {
        runUploadAndArchiveAnonTest(projectScriptDE4, siteScriptDE4, bonusScriptDE4);
    }

    @Test
    @DisallowXnatVersion(disallowedVersions = Xnat_1_6dev.class)
    @JiraKey(simpleKey = "QA-478")
    public void testUploadAndArchiveAnon_DE6() {
        runUploadAndArchiveAnonTest(projectScriptDE6, siteScriptDE6, bonusScriptDE6);
    }

    @Test
    @JiraKey(simpleKey = "QA-479")
    public void testDefaultSiteAnon() {
        final String sessionLabel = "defaultAnonTest";
        final ImagingSession anonSession = new ImagingSession().label(sessionLabel);
        final Project anonProject = new Project().runningTitle(RandomHelper.randomID()).title(RandomHelper.randomID());
        final RootDicomObject expectedDicomObject = new RootDicomObject();
        expectedDicomObject.putValueEqualCheck(StudyDescription, anonProject.getId());
        expectedDicomObject.putValueEqualCheck(PatientName, testSpecificSubject.getLabel());
        expectedDicomObject.putValueEqualCheck(PatientID, sessionLabel);

        final ProjectReportPage projectReportPage = loginPage.seleniumAdminLogin().
                loadAdminUi().
                setSiteAnonScript(true, defaultSiteScript).
                logLogout().
                seleniumLogin().
                createProject(anonProject);

        restDriver.mainInterface().logout(); // cached permissions in REST API
        restDriver.mainInterface().reauthenticate();
        mainInterface().createSubjectAssessor(anonProject, testSpecificSubject, anonSession.extension(new SessionImportExtension(anonSession, ANON_TEST_DATA.toFile())));
        captureScreenshotlessStep();

        projectReportPage.searchForSubject(testSpecificSubject.getLabel()).
                clickMrSessionLink().
                viewFirstScan().
                readDicomDump().
                validate(expectedDicomObject).
                returnToUnderlyingPage().
                returnToUnderlyingPage().
                logLogout();
    }

    private void runProjectChangeTest(AnonScript projectScript, AnonScript siteScript) {
        final Project originalProject = new Project();
        final Project destinationProject = new Project();
        final MRSession session = new MRSession(originalProject, ANON_SESSION_NAME);

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                disableSiteAnonScript().
                logLogout().
                seleniumLogin().
                createProject(originalProject).
                loadManageTab().
                setAnonScript(true, projectScript).
                disableAnonScript().
                getProjectPage().
                createProject(destinationProject).
                loadManageTab().
                setAnonScript(true, projectScript).
                getProjectPage().
                loadCompressedUploader().
                uploadToArchive(originalProject, ANON_TEST_DATA).
                viewFirstScan().
                readDicomDump().
                validate(dicomWithoutAnon);
        captureScreenshotlessStep();
        home().
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                setSiteAnonScript(true, siteScript).
                logLogout().
                seleniumLogin().
                clickRecentSession(session).
                loadEditPage().
                captureSimpleSummary().
                moveSessionToProject(destinationProject.getId()).
                home().
                clickRecentSession(session.project(destinationProject)).
                viewFirstScan().
                readDicomDump().
                validate(projectAnonymizedDicom);
        captureScreenshotlessStep();
        home().logLogout();
    }

    private void runSubjectReassignAnonTest(AnonScript projectScript, AnonScript siteScript) {
        final Project anonProject = new Project();
        final Subject destinationSubject = new Subject(anonProject);
        final MRSession session = new MRSession(anonProject, ANON_SESSION_NAME);

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                disableSiteAnonScript().
                logLogout().
                seleniumLogin().
                createProject(anonProject).
                createSubject(destinationSubject).
                loadCompressedUploader().
                uploadToArchive(anonProject, ANON_TEST_DATA).
                viewFirstScan().
                readDicomDump().
                validate(dicomWithoutAnon);
        captureScreenshotlessStep();
        home().
                searchForProject(anonProject).
                loadManageTab().
                setAnonScript(true, projectScript).
                getProjectPage().
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                setSiteAnonScript(true, siteScript).
                logLogout().
                seleniumLogin().
                clickRecentSession(session).
                loadEditPage().
                captureSimpleSummary().
                reassignSessionToSubject(destinationSubject.getLabel()).
                home().
                clickRecentSession(session).
                viewFirstScan().
                readDicomDump().
                validate(projectAnonymizedDicom);
        captureScreenshotlessStep();
        home().logLogout();
    }

    private void runSessionLabelChangeAnonTest(AnonScript projectScript, AnonScript siteScript) {
        final Project anonProject = new Project();

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                disableSiteAnonScript().
                logLogout().
                seleniumLogin().
                createProject(anonProject).
                loadCompressedUploader().
                uploadToArchive(anonProject, ANON_TEST_DATA).
                viewFirstScan().
                readDicomDump().
                validate(dicomWithoutAnon).
                captureScreenshotlessStep();
        home().
                searchForProject(anonProject).
                loadManageTab().
                setAnonScript(true, projectScript).
                getProjectPage().
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                setSiteAnonScript(true, siteScript).
                logLogout().
                seleniumLogin().
                clickRecentSession(new MRSession(anonProject, ANON_SESSION_NAME)).
                loadEditPage().
                captureSimpleSummary().
                modifySessionLabel("changedLabel").
                navigate().
                back();
        loadPage(ImagingSessionReportPage.class).
                viewFirstScan().
                readDicomDump().
                validate(projectAnonymizedDicom).
                captureScreenshotlessStep();
        home().logLogout();
    }

    private void runUploadAndArchiveAnonTest(AnonScript projectScript, AnonScript siteScript, AnonScript bonusScript) {
        final RootDicomObject dicomInPrearc = new RootDicomObject();
        addProjectAnonCheck(false, dicomInPrearc); // project anon doesn't run until archive
        addSiteAnonCheck(true, dicomInPrearc);
        addBonusScriptCheck(false, dicomInPrearc);

        final RootDicomObject dicomAfterArchive = new RootDicomObject();
        addProjectAnonCheck(true, dicomAfterArchive);
        addSiteAnonCheck(true, dicomAfterArchive);
        addBonusScriptCheck(false, dicomAfterArchive); // "site" (bonus) script should not run on archival!

        final Project anonProject = new Project();
        final MRSession session = new MRSession(anonProject, ANON_SESSION_NAME);

        final ProjectReportPage projectReportPage = loginPage.seleniumAdminLogin().
                loadAdminUi().
                setSiteAnonScript(true, siteScript).
                logLogout().
                seleniumLogin().
                createProject(anonProject).
                loadManageTab().
                setAnonScript(true, projectScript).
                getProjectPage();
        new XnatCStore().data(ANON_TEST_DATA).sendDICOMToProject(anonProject);
        captureScreenshotlessStep();
        final DicomDumpPage<PrearchiveDetails> dicomDump = projectReportPage.loadProjectPrearchive().
                findOnlySessionInProject(anonProject).
                capture().
                rebuild().
                findOnlySessionInProject(anonProject, PrearchiveStatus.READY).
                capture().
                checkAll().
                loadDetails().
                capture().
                readDicomDump().
                validate(dicomInPrearc);
        captureScreenshotlessStep();
        dicomDump.returnToUnderlyingPage().
                returnToPrearchive().
                logLogout().
                seleniumAdminLogin().
                loadAdminUi().
                setSiteAnonScript(true, bonusScript).
                logLogout().
                seleniumLogin().
                searchForProject(anonProject).
                loadProjectPrearchive().
                findOnlySessionInProject(anonProject).
                archive().
                capture().
                home().
                captureRecentSession(session).
                clickRecentSession(session).
                viewFirstScan().
                readDicomDump().
                validate(dicomAfterArchive).
                captureScreenshotlessStep();
        home().logLogout();
    }

    private RootDicomObject addProjectAnonCheck(boolean isEnabled, RootDicomObject expectedDicomObject) {
        expectedDicomObject.putValueEqualCheck(MagneticFieldStrength, "3");

        if (isEnabled) {
            expectedDicomObject.putNonexistenceChecks(0x00291018);
            expectedDicomObject.putValueEqualCheck(InstitutionalDepartmentName, "str_3D_University");
            expectedDicomObject.putValueEqualCheck(PerformingPhysicianName, "01204567");
            expectedDicomObject.putValueNotEqualCheck(ReferencedSOPInstanceUID, "");
            expectedDicomObject.putValueEqualCheck(BodyPartExamined, "");
            expectedDicomObject.putValueEqualCheck(PatientName, "Fox");
            expectedDicomObject.putValueEqualCheck(Allergies, "Rest");
            expectedDicomObject.putValueEqualCheck(DetectorID, "OP_Raven");
        } else {
            expectedDicomObject.putNonexistenceChecks(InstitutionalDepartmentName, ReferencedSOPInstanceUID, Allergies, DetectorID);
            expectedDicomObject.putExistenceChecks(0x00291018);
            expectedDicomObject.putValueEqualCheck(PerformingPhysicianName, "");
            expectedDicomObject.putValueEqualCheck(BodyPartExamined, "HEAD");
            expectedDicomObject.putValueEqualCheck(PatientName, "Sample Patient");
        }
        return expectedDicomObject;
    }

    private RootDicomObject addSiteAnonCheck(boolean isEnabled, RootDicomObject expectedDicomObject) {
        expectedDicomObject.putValueEqualCheck(ImagedNucleus, "1H");

        if (isEnabled) {
            expectedDicomObject.putNonexistenceChecks(0x00291019);
            expectedDicomObject.putValueEqualCheck(AccessionNumber, "Ac_MR_VAR_yelling");
            expectedDicomObject.putValueEqualCheck(ReferringPhysicianName, "Math is interesting");
            expectedDicomObject.putValueNotEqualCheck(StudyInstanceUID, ANON_TEST_DATA.getStudyInstanceUid());
            expectedDicomObject.putValueEqualCheck(ReferringPhysicianAddress, "");
            expectedDicomObject.putValueEqualCheck(CodeValue, "5");
            expectedDicomObject.putValueEqualCheck(StationName, "King's Crossing");
            expectedDicomObject.putValueEqualCheck(PatientComments, "Session 20XX");
            expectedDicomObject.putValueEqualCheck(CodeMeaning, "KK_DR");
        } else {
            expectedDicomObject.putNonexistenceChecks(ReferringPhysicianAddress, CodeValue, PatientComments, CodeMeaning);
            expectedDicomObject.putExistenceChecks(0x00291019);
            expectedDicomObject.putValueEqualCheck(AccessionNumber, ".");
            expectedDicomObject.putValueEqualCheck(ReferringPhysicianName, "");
            expectedDicomObject.putValueEqualCheck(StudyInstanceUID, ANON_TEST_DATA.getStudyInstanceUid());
            expectedDicomObject.putValueEqualCheck(StationName, "MEDPC");
        }
        return expectedDicomObject;
    }

    @SuppressWarnings("SameParameterValue")
    private void addBonusScriptCheck(boolean isEnabled, RootDicomObject expectedDicomObject) {
        if (isEnabled) {
            expectedDicomObject.putNonexistenceChecks(0x00291020);
            expectedDicomObject.putValueEqualCheck(Occupation, "Zergling Rusher");
            expectedDicomObject.putValueEqualCheck(Manufacturer, "Catscan Inc.");
        } else {
            expectedDicomObject.putNonexistenceChecks(Occupation);
            expectedDicomObject.putValueNotEqualCheck(0x00291020, "");
            expectedDicomObject.putValueEqualCheck(Manufacturer, "SIEMENS");
        }
    }

}
