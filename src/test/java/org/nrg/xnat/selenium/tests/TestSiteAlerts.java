package org.nrg.xnat.selenium.tests;

import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.selenium.CustomConditions;
import org.nrg.selenium.enums.SiteAlertEnablingOption;
import org.nrg.selenium.enums.SiteAlertType;
import org.nrg.selenium.xnat.page_model.IndexPage;
import org.nrg.selenium.xnat.page_model.SiteAlertContainer;
import org.nrg.selenium.xnat.page_model.ui_element.notification.XnatSiteAlert;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.util.RandomHelper;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

@TestRequires(closedXnat = true)
public class TestSiteAlerts extends BaseSeleniumTest {

    private final String wikipediaLinkText = "Wikipedia";
    
    @Test
    @JiraKey(simpleKey = "QA-381")
    public void testMessageEverywhere() {
        performSiteAlertTest(SiteAlertType.MESSAGE, SiteAlertEnablingOption.ON_EVERYWHERE);
    }
    
    @Test
    @JiraKey(simpleKey = "QA-382")
    public void testMessageLoginPage() {
        performSiteAlertTest(SiteAlertType.MESSAGE, SiteAlertEnablingOption.LOGIN_PAGE);
    }

    @Test
    @JiraKey(simpleKey = "QA-383")
    public void testMessageOff() {
        performSiteAlertTest(SiteAlertType.MESSAGE, SiteAlertEnablingOption.OFF);
    }

    @Test
    @JiraKey(simpleKey = "QA-384")
    public void testAlertEverywhere() {
        performSiteAlertTest(SiteAlertType.ALERT, SiteAlertEnablingOption.ON_EVERYWHERE);
    }

    @Test
    @JiraKey(simpleKey = "QA-385")
    public void testAlertLoginPage() {
        performSiteAlertTest(SiteAlertType.ALERT, SiteAlertEnablingOption.LOGIN_PAGE);
    }

    @Test
    @JiraKey(simpleKey = "QA-386")
    public void testAlertOff() {
        performSiteAlertTest(SiteAlertType.ALERT, SiteAlertEnablingOption.OFF);
    }

    @Test
    @JiraKey(simpleKey = "QA-387")
    public void testErrorEverywhere() {
        performSiteAlertTest(SiteAlertType.ERROR, SiteAlertEnablingOption.ON_EVERYWHERE);
    }

    @Test
    @JiraKey(simpleKey = "QA-388")
    public void testErrorLoginPage() {
        performSiteAlertTest(SiteAlertType.ERROR, SiteAlertEnablingOption.LOGIN_PAGE);
    }

    @Test
    @JiraKey(simpleKey = "QA-389")
    public void testErrorOff() {
        performSiteAlertTest(SiteAlertType.ERROR, SiteAlertEnablingOption.OFF);
    }

    private void performSiteAlertTest(SiteAlertType alertType, SiteAlertEnablingOption enablingOption) {
        final String wikipediaLink = String.format("<a href=\"http://wikipedia.org\">%s</a>", wikipediaLinkText);
        final String alertContents = RandomHelper.randomString(20) + " ";

        loginPage.seleniumAdminLogin().
                loadAdminUi().
                setSiteAlert(enablingOption, alertContents + wikipediaLink, alertType).
                logLogout();
        if (enablingOption == SiteAlertEnablingOption.OFF) {
            assertFalse(loginPage.siteAlertIsPresent());
            loginPage.capture();
        } else {
            checkAlertLink(loginPage, alertContents, alertType);
        }
        final IndexPage indexPage = loginPage.seleniumLogin();
        if (enablingOption == SiteAlertEnablingOption.ON_EVERYWHERE) {
            checkAlertLink(indexPage, alertContents, alertType);
        } else {
            assertFalse(indexPage.siteAlertIsPresent());
            captureScreenshotlessStep();
        }
        indexPage.logLogout();
    }

    private void checkAlertLink(SiteAlertContainer alertContainer, String baseAlertString, SiteAlertType alertType) {
        final XnatSiteAlert alert = alertContainer.readSiteAlert();
        alert.assertTextEquals(baseAlertString + wikipediaLinkText);
        assertEquals(alertType, alert.getAlertType());
        alert.uploadScreenshot();
        alert.clickLinkInAlert();
        wait.until(CustomConditions.urlContains("wikipedia"));
        captureScreenshotlessStep();
        navigate().back();
    }
}
