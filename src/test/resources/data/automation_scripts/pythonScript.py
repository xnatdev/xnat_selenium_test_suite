from org.nrg.xnat.turbine.utils import ArcSpecManager
import os.path

archiveResources = os.path.join(ArcSpecManager.GetInstance().getArchivePathForProject(externalId), 'resources', 'python_proj_folder')

with open(os.path.join(archiveResources, 'testFile.txt'), 'a+') as f:
    f.write('Here goes nothing...\n')
    f.write('User: ' + user.getLogin() + '\n')
    f.write('srcWorkflowId: ' + str(srcWorkflowId) + '\n')
    f.write('scriptWorkflowId: ' + str(scriptWorkflowId) + '\n')
    f.write('dataId: ' + dataId + '\n')
    f.write('externalId: ' + externalId + '\n')
    f.write('dataType: ' + dataType + '\n')
    f.write('workflow: ' + str(workflow.wrkWorkflowdataId) + '\n')
