try {
    load("nashorn:mozilla_compat.js");
} catch (e) {}
importClass(org.nrg.xnat.turbine.utils.ArcSpecManager);
importPackage(java.io);

var archiveResources = org.nrg.xnat.turbine.utils.ArcSpecManager.GetInstance().getArchivePathForProject(externalId) + "resources/javascript_proj_folder";
var file = new java.io.File(archiveResources, "testFile.txt");
var writer = new java.io.PrintWriter(new FileWriter(file));

writer.println("Here goes nothing...");
writer.println("User: " + user.getLogin());
writer.println("srcWorkflowId: " + srcWorkflowId);
writer.println("scriptWorkflowId: " + scriptWorkflowId);
writer.println("dataId: " + dataId);
writer.println("externalId: " + externalId);
writer.println("dataType: " + dataType);
writer.println("workflow: " + workflow.wrkWorkflowdataId);
writer.close();