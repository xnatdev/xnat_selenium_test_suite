# This project runs webapp tests on an XNAT-based test server.

# Usage/Installation

1.  Make sure you have installed **JDK 8 version** and you also have **Maven** installed as well(**3.6+ version**).
2.  Clone repo:  
    `git clone git@bitbucket.org:xnatdev/xnat_selenium_test_suite.git`
3.  Add ***geckodriver, credentials.json, local.properties*** files into `src/test/resources/config` directory.
4.  Run tests using IDE (i.e. IDEA's) Runner/Debugger or using terminal command:  
    `mvn clean install`

# Debugging

Please note, that most (actually, all) **Page Object classes for XNAT** application located in ***nrg_selenium*** repo.  
So, if you need to adjust/test/fix/debug some part - you most likely will require this repo.  
In order to do that pls perform:

1.  Make sure that you have Graddle installed, **compatible with JDK 8 version**
2.  Clone repo:  
    *`git clone git@bitbucket.org:xnatdev/nrg_selenium.git`*
3.  Make your changes
4.  Remove local copy of your Selenium snapshot (**not the whole selenium directory, only snapshot directory, i.e. '9.0-SNAPSHOT'**)  
    **LINUX/UNIX Path: *~/.m2/repository/org/nrg/selenium***
5.  Compile your tweaked local copy using this terminal command in nrg_selenium project directory  
    *`./gradlew clean jar publishToMavenLocal`*

# Properties

There are several properties required to be set. The meanings of the properties are defined in [nrg_test](http://www.bitbucket.org/xnatdev/nrg_test/). Note that if the default value is correct for the particular test run, the particular property can of course be omitted:

- xnat.main.user
- xnat.main.password
- xnat.mainAdmin.user
- xnat.mainAdmin.password
- xnat.admin.user
- xnat.admin.password
- xnat.version
- xnat.users.email
- xnat.users.email.credentials
- xnat.users.email.tokens
- xnat.baseurl
- xnat.dicom.host
- xnat.dicom.port
- xnat.dicom.aetitle
- firefox.path
- xnat.browser
- xnat.defaultTimeout

A few tests do require SSH access and/or direct database access to XNAT in order to run. They depend on the following properties:

- xnat.db.url
- xnat.db.user
- xnat.db.password
- xnat.ssh.user
- xnat.ssh.key
- xnat.ssh.skipHostKeyVerification
- tomcat.control

Properties that are used only if using the summary email feature are:

- xnat.notifiedEmails
- xnat.notificationTitle
- xnat.notifyOnSuccess
- mail.smtp.host
- mail.smtp.port

Properties that these tests support to change some of the behavior, but are not required are:

- xnat.dicom.callingaetitle
- xnat.testBehavior.expectedFailures
- xnat.basic
- xnat.temp
- xnat.dependencies
- xnat.timelogs
- xnat.gitLogs
- xnat.setupMrscan
- xnat.requireAdmin
- xnat.allowLogging
- xnat.jira
- xnat.producePdf
- selenium.headless
- xnat.captureDom
- xnat.init